/**
 * 
 */
package fr.cemaj.hp.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

/**
 * @author MathieuDegaine
 * 
 */
public class StackTest {
    @Test
    public void testConstructor (){
        Stack test = new Stack();
    }
    @Test( expected  = IllegalArgumentException.class )
    public void testInit() {
        Stack test = new Stack();
        test.init(1);
    }
    @Test
    public void testInitNbMin(){
        Stack test = new Stack();
        test.init(2);
        assertEquals(72, test.getStack().size());
    }
    @Test
    public void testInitMax() {
        Stack test = new Stack();
        test.init(4);
        assertEquals(72, test.getStack().size());
    }
    @Test
    public void testShuffle() {
        Stack test = new Stack();
        Stack test2 = new Stack();
        test.init(3);
        test.shuffle();
        assertFalse(test.equals(test2));
    }
}
