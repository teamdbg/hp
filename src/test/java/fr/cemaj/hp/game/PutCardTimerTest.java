/**
 * PutCardTimerTest.java
 *
 * Version 0.1
 *
 * Date 13 dec. 2013
 *
 * Copyright CEMAJ
 *
 * Developers: C.L.
 */
package fr.cemaj.hp.game;

public class PutCardTimerTest {

    /**
     * Test the timer.
     */
    public static void main(String[] args) {
        // init the timer
        PutCardTimer t = new PutCardTimer(new ServerEnvironment(true, null));
        t.initializeTimer();
        try {
            Thread.sleep(5*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // reset the timer (must suppress the previous countdown)
        t.resetTimer();
        try {
            Thread.sleep(15*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t.cancelTimer();
    }

}
