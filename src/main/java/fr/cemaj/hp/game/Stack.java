/**
 *
 */
package fr.cemaj.hp.game;

import java.util.ArrayList;
import java.util.Collections;

import fr.cemaj.hp.game.card.Abracadabra;
import fr.cemaj.hp.game.card.BlackCat;
import fr.cemaj.hp.game.card.Card;
import fr.cemaj.hp.game.card.CounterSpell;
import fr.cemaj.hp.game.card.CrystalBall;
import fr.cemaj.hp.game.card.Curse;
import fr.cemaj.hp.game.card.EnchantedMirror;
import fr.cemaj.hp.game.card.Hourglass;
import fr.cemaj.hp.game.card.Inspiration;
import fr.cemaj.hp.game.card.MagicStaff;
import fr.cemaj.hp.game.card.Necklace;
import fr.cemaj.hp.game.card.Owl;
import fr.cemaj.hp.game.card.Pumpkin;
import fr.cemaj.hp.game.card.Sacrifice;
import fr.cemaj.hp.game.card.Spell;
import fr.cemaj.hp.game.card.Thief;
import fr.cemaj.hp.game.card.ThunderBolt;
import fr.cemaj.hp.game.card.Whirlpool;


/**
 * @author MathieuDegaine
 *
 */
public class Stack {
    private ArrayList<Card> stack;

    /**
     * Stack Constructor.
     */
    protected Stack() {
        super();
        this.stack = new ArrayList<>();
    }

    /**
     * @param playerNumber : the id of the player to init
     *
     * IF the number of player is not allowed, \
     * the exception IllegalArgumentException is thrown
     */
    public void init(int playerNumber) {
        if(playerNumber < 2 || playerNumber > 6) {
            throw new IllegalArgumentException();
        }
        initVortex();
        initAbracadabra();
        initCrystalBall();
        initOwl();
        initSacrifice();
        initCurse();
        initInspiration();
        initThief();
        initSpell();
        initNecklace(playerNumber);
        initMagicStaff();
        initEnchantedMirror(playerNumber);
        initPumpkin();
        initCounterSpell();
        initHourglass();
        initBlackCat();
        initThunderBolt();
    }

    private void initVortex() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Whirlpool());
        }
    }

    private void initAbracadabra() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Abracadabra());
        }
    }

    private void initCrystalBall() {
        for (int i = 0; i < 3; i++) {
            this.stack.add(new CrystalBall());
        }
    }

    private void initOwl() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Owl(true, 1));
        }
        this.stack.add(new Owl(true, 2));
    }

    private void initSacrifice() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Sacrifice(2));
        }
    }

    private void initCurse() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Curse(1));
        }
        this.stack.add(new Curse(2));
    }

    private void initInspiration() {
        for (int i = 0; i < 3; i++) {
            this.stack.add(new Inspiration(2));
        }
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Inspiration(3));
        }
    }

    private void initThief() {
        for (int i = 0; i < 5; i++) {
            this.stack.add(new Thief(1));
        }
        for (int i = 0; i < 4; i++) {
            this.stack.add(new Thief(2));
        }
        for (int i = 0; i < 3; i++) {
            this.stack.add(new Thief(3));
        }
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Thief(4));
        }
        this.stack.add(new Thief(5));
    }

    private void initSpell() {
        for (int i = 0; i < 5; i++) {
            this.stack.add(new Spell(1));
        }
        for (int i = 0; i < 4; i++) {
            this.stack.add(new Spell(2));
        }
        for (int i = 0; i < 3; i++) {
            this.stack.add(new Spell(3));
        }
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Spell(4));
        }
        this.stack.add(new Spell(5));
    }

    private void initNecklace(int playerNumber) {
        if(playerNumber == 2) {
            for(int i = 0; i < 7; i++) {
                this.stack.add(new Necklace());
            }
        } else {
            for(int i = 0; i < 4; i++) {
                this.stack.add(new Necklace());
            }
        }
    }

    private void initMagicStaff() {
        for(int i = 0; i < 4; i++) {
            this.stack.add(new MagicStaff());
        }
    }

    private void initEnchantedMirror(int playerNumber) {
        if (playerNumber > 2) {
            for (int i = 0; i < 3; i++) {
                this.stack.add(new EnchantedMirror());
            }
        } else {
            // TODO Ce bloc else est vide les amis !
            System.out.println("TODO");
        }
    }

    private void initPumpkin() {
        for (int i = 0; i < 3; i++) {
            this.stack.add(new Pumpkin());
        }
    }

    private void initCounterSpell() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new CounterSpell());
        }
    }

    private void initHourglass() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new Hourglass());
        }
    }

    private void initBlackCat() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new BlackCat());
        }
    }

    private void initThunderBolt() {
        for (int i = 0; i < 2; i++) {
            this.stack.add(new ThunderBolt());
        }
    }

    /**
     *
     */
    public void shuffle() {
        Collections.shuffle(this.stack);
    }

    /**
     * Formating the object into a String.
     * @return a string representing the object
     */
    @Override
    public String toString() {
        return "Stack [stack=" + this.stack + "]";
    }

    /**
     *
     * @return stack : the current list of Card that composed the stack
     */
    public ArrayList<Card> getStack() {
        return this.stack;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO You !! MUST !! override hashCode() if you override equals()
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        final Stack other = (Stack) obj;
        if (this.stack == null) {
            if (other.stack != null) {
                return false;
            }
        } else if (!this.stack.equals(other.stack)) {
            return false;
        }
        return true;
    }
}
