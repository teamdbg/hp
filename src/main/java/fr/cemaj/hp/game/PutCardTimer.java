/**
 * PutCardTimer.java
 *
 * Version 0.1
 *
 * Date 13 dec. 2013
 *
 * Copyright CEMAJ
 *
 * Developers: C.L.
 */
package fr.cemaj.hp.game;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class is used to simulate a timer to end card playing sessions.
 */
public class PutCardTimer {

    private Timer timer;
    private static int putCardTimeout = 10;
    private ServerEnvironment environment;

    /**
     * Create and initialize a card timer.
     * @param anEnvironment (ServerEnvironment)
     */
    public PutCardTimer(ServerEnvironment anEnvironment) {
        this.environment = anEnvironment;
        this.initializeTimer();
    }

    /**
     * Reset timer.
     */
    public void resetTimer() {
        this.timer.cancel();
        System.out.println("Timer reseted.");
        this.initializeTimer();
    }

    /**
     * Initialize the timer.
     */
    public void initializeTimer() {
        this.timer = new Timer();
        this.timer.schedule(new TimeOut(this.environment),
                putCardTimeout * 1000);
        System.out.format("Timer initialized.");
    }

    /**
     * Cancel the timer.
     */
    public void cancelTimer() {
        this.timer.cancel();
        System.out.println("Timer canceled !");
    }

    /**
     * This class runs actions when timer is ending.
     */
    class TimeOut extends TimerTask {
        private ServerEnvironment environment;
        /**
         * Constructor: Initialize the link to the server environment.
         * @param anEnvironment to link
         */
        public TimeOut(ServerEnvironment anEnvironment) {
            this.environment = anEnvironment;
        }
        /**
         * Method run when time is out !
         */
        public void run() {
            System.out.format("Time's up!");
            this.environment.endSession();
            timer.cancel(); //Terminate the timer thread
        }
    }
}
