/**
 * package-info: fr.cemaj.hp.game.resolution
 *
 * Version 0.1
 *
 * Date 11.26.2013
 *
 * Copyright CEMAJ
 *
 * Developers: C.L.
 */

/**
 * This package contents the classes needed to resolve a game session.
 */
package fr.cemaj.hp.game.resolution;
