/**
 * ResolutionState.java
 *
 * Version 0.1
 *
 * Date 26 nov. 2013
 *
 * Copyright CEMAJ
 *
 * Developers: C.L.
 */
package fr.cemaj.hp.game.resolution;

import java.util.ArrayList;

import fr.cemaj.hp.game.Player;
import fr.cemaj.hp.game.card.BlackCat;
import fr.cemaj.hp.game.card.Card;
import fr.cemaj.hp.game.card.Card.ActionType;
import fr.cemaj.hp.game.card.Card.TargetType;
import fr.cemaj.hp.game.card.CardTargeting;
import fr.cemaj.hp.game.card.CounterSpell;
import fr.cemaj.hp.game.card.EnchantedMirror;
import fr.cemaj.hp.game.card.Hourglass;
import fr.cemaj.hp.game.card.MagicStaff;
import fr.cemaj.hp.game.card.Necklace;
import fr.cemaj.hp.game.card.PlayerTargeting;
import fr.cemaj.hp.game.card.Pumpkin;
import fr.cemaj.hp.game.card.Targeting;
import fr.cemaj.hp.game.card.ThunderBolt;

/**
 * This class represents a state of resolution.
 */
public class ResolutionState {

    private ActionType actionType;
    private ArrayList<Card> blockedCards;
    private boolean blockedInspiration;
    private boolean endHocusPocusSession;
    private boolean endPlayerTurn;
    private Card hocusCard;
    private Player newOwner;
    private int power;
    /**
     * Only used when the target is a specific grimoire card.
     */
    private ArrayList<Card> targetedCards;
    /**
     * Only used when the target is a specific player.
     */
    private Player targetedPlayer;
    private TargetType targetType;
    private boolean thundered;

    /**
     * Creates an empty state of resolution.
     */
    public ResolutionState() {
        this.actionType = ActionType.NONE;
        this.blockedCards = new ArrayList<Card>();
        this.endPlayerTurn = false;
        this.endHocusPocusSession = false;
        this.hocusCard = null;
        this.newOwner = null;
        this.power = 0;
        this.targetedCards = new ArrayList<Card>();
        this.targetedPlayer = null;
        this.targetType = TargetType.NONE;
        this.thundered = false;
    }

    /**
     * @return the actionType of the current ResolutionState
     */
    public ActionType getActionType() {
        return this.actionType;
    }

    /**
     * @return the blockedCards of the current player turn
     */
    public ArrayList<Card> getBlockedCards() {
        return this.blockedCards;
    }

    /**
     * @return the endPlayerTurn
     * true if the current player turn must be closed after cards resolution\
     * otherwise, false.
     */
    public boolean isEndPlayerTurn() {
        return this.endPlayerTurn;
    }

    /**
     * @return the hocusPocusSessionEnd
     * true if the hocus-pocus session must be ended immediately and the cards\
     * to be resolved, otherwise, false.
     */
    public boolean isEndHocusPocusSession() {
        return this.endHocusPocusSession;
    }

    /**
     * @return the power of the current resolution state\
     * (0 if there is no particular power level)
     */
    public int getPower() {
        return this.power;
    }

    /**
     * @return the targetedCard
     * Remark: the player must be targeted too (to identify whose card it is).
     */
    public ArrayList<Card> getTargetedCards() {
        return this.targetedCards;
    }

    /**
     * setTargetedCards.
     * @param cards : list of cards
     */
    public void setTargetedCards(ArrayList<Card> cards) {
        this.targetedCards = cards;
    }

    /**
     * @return the targetedPlayer
     */
    public Player getTargetedPlayer() {
        return this.targetedPlayer;
    }

    /**
     * @return the targetType
     */
    public TargetType getTargetType() {
        return this.targetType;
    }

    /**
     * getNewOwner.
     * @return newOwner (Player)
     */
    public Player getNewOwner() {
        return this.newOwner;
    }

    /**
     * Resolve a new played card.
     * @param aCard a card to resolve.
     */
    public void resolveCard(Card aCard) {
        if (aCard.isHocus()) {
            this.hocusCard = aCard;
            this.targetType = aCard.getTargetType();
            this.actionType = aCard.getActionType();
            this.power = aCard.getPower();

            if (aCard instanceof Targeting) {
                Targeting targetingCard = (Targeting) aCard;
                // Set the targets
                this.targetedPlayer = targetingCard.getTargetedPlayer();
                if (targetingCard instanceof CardTargeting) {
                    CardTargeting cardTargetingCard =
                            (CardTargeting) targetingCard;
                    this.targetedCards = cardTargetingCard.getTargetedCards();
                }
            }
        } else {
            // When a Pocus card is encountered...
            if (this.hocusCard == null) {
                //TODO throw exception
                System.err.println("ResolutionState log:");
                System.err.println("Cannot play a Pocus card when "
                        + "no hocus card was already played.");
            }
            if (aCard instanceof BlackCat) {
                this.actionType = aCard.getActionType();
                this.targetType = aCard.getTargetType();
                this.targetedPlayer = null;
                this.targetedCards = null;
                this.power = 0;
                this.endHocusPocusSession = true;
                this.newOwner = aCard.getCardOwner();
            }
            if (aCard instanceof CounterSpell) {
                this.actionType = aCard.getActionType();
                this.targetType = aCard.getTargetType();
                this.targetedPlayer = null;
                this.targetedCards = null;
                this.power = 0;
                this.endHocusPocusSession = true;
            }
            if (aCard instanceof EnchantedMirror) {
                PlayerTargeting enchantCard = (PlayerTargeting) aCard;
                if (this.targetType == TargetType.GRIMOIRE_CARD
                        || this.targetType == TargetType.PLAYER_GEMS
                        || this.targetType == TargetType.PLAYER_HAND) {
                    this.targetedPlayer = enchantCard.getTargetedPlayer();
                    if (this.targetType == TargetType.GRIMOIRE_CARD) {
                        this.targetedCards = new ArrayList<Card>();
                        this.targetedCards = ((CardTargeting) this.hocusCard)
                                .getTargetedCards();
                    }
                } else {
                    System.err.println("ResolutionState log:");
                    System.err.println("The EnchantedMirror "
                            + "is useless in this case");
                }
            }
            if (aCard instanceof Hourglass) {
                this.endPlayerTurn = true;
            }
            if (aCard instanceof MagicStaff) {
                this.power = this.hocusCard.getPower() * 2;
            }
            if (aCard instanceof Necklace) {
                if (this.targetedPlayer.getId() == aCard.getCardOwner()
                        .getId()) {
                    this.actionType = aCard.getActionType();
                    this.targetType = aCard.getTargetType();
                    this.endHocusPocusSession = true;
                    this.targetedCards = null;
                    this.targetedPlayer = null;
                    this.power = 0;
                } else {
                    System.err.println("ResolutionState log:");
                    System.err.println("The Necklace is useless in this case");
                }
            }
            if (aCard instanceof Pumpkin) {
                this.actionType = aCard.getActionType();
                this.targetType = aCard.getTargetType();
                this.power = 0;
                this.endHocusPocusSession = true;
                this.targetedCards = null;
                this.targetedPlayer = null;
                this.blockedCards.add(this.hocusCard);
                System.out.println("ResolutionState log:");
                System.out.println(this.hocusCard.getClass().getName());
            }
            if (aCard instanceof ThunderBolt) {
                this.thundered = true;
            }
        }
    }

    /**
     * Resolve playedCards after a thunderbolt.
     */
    public void resolveFromThunder() {
        this.actionType = this.hocusCard.getActionType();
        this.targetType = this.hocusCard.getTargetType();
        this.endHocusPocusSession = false;
        this.endPlayerTurn = false;
        this.newOwner = null;
        this.power = this.hocusCard.getPower();
        if (this.hocusCard instanceof Targeting) {
            Targeting targetingCard = (Targeting) this.hocusCard;
            this.targetedPlayer = targetingCard.getTargetedPlayer();
            if (targetingCard instanceof CardTargeting) {
                CardTargeting cardTargetingCard = (CardTargeting) targetingCard;
                this.targetedCards = cardTargetingCard.getTargetedCards();
            } else {
                this.targetedCards = null;
            }
        } else {
            this.targetedPlayer = null;
        }
    }

    /**
     * @return the hocusCard
     */
    public Card getHocusCard() {
        return this.hocusCard;
    }

    /**
     * Reset the resolution state when hocus-pocus session ends.
     */
    public void resetSessionState() {
        this.actionType = ActionType.NONE;
        this.endHocusPocusSession = false;
        this.endPlayerTurn = false;
        this.hocusCard = null;
        this.power = 0;
        this.targetedCards = new ArrayList<Card>();
        this.targetedPlayer = null;
    }

    /**
     * Reset the resolution state when player's turn ends.
     */
    public void resetTurnState() {
        if (this.endPlayerTurn) {
            this.resetSessionState();
            this.blockedInspiration = false;
            this.blockedCards = new ArrayList<Card>();
        }
    }

    /**
     * @return the thundered
     */
    public boolean isThundered() {
        return this.thundered;
    }

    /**
     * @return the blockedInspiration
     */
    public boolean isBlockedInspiration() {
        return this.blockedInspiration;
    }

    /**
     * @param blockedInspiration the blockedInspiration to set
     */
    public void setBlockedInspiration(boolean blockedInspiration) {
        this.blockedInspiration = blockedInspiration;
    }
}
