/**
 * CardsResolver.java
 *
 * Version 0.1
 *
 * Date 26 nov. 2013
 *
 * Copyright CEMAJ
 *
 * Developers: C.L.
 */
package fr.cemaj.hp.game.resolution;

import java.util.ArrayList;

import fr.cemaj.hp.game.ServerEnvironment;
import fr.cemaj.hp.game.card.Card;
import fr.cemaj.hp.game.card.Card.ActionType;
import fr.cemaj.hp.game.card.Inspiration;

/**
 * This class enables the resolution of cards.
 */
public class CardsResolver {

    private ResolutionState currentState;
    private ServerEnvironment serverEnvironment;

    /**
     * Create a card resolver.
     * @param aServerEnvironment the environment creating the CardsResolver
     */
    public CardsResolver(ServerEnvironment aServerEnvironment) {
        this.currentState = new ResolutionState();
        this.serverEnvironment = aServerEnvironment;
    }

    /**
     * Resolve the last played card according to the previous resolution state.
     * @param lastPlayedCard the last played card
     */
    public void resolveCard(Card lastPlayedCard) {
        this.currentState.resolveCard(lastPlayedCard);
        this.updateBlockedCards();
    }

    /**
     * updateTargetedCards.
     * @param cards : cards list
     */
    public void updateTargetedCards(ArrayList<Card> cards) {
        this.currentState.setTargetedCards(cards);
    }

    /**
     * Resolve the hocus-pocus session and make the serverEnvironment react.
     */
    public void sessionResolution() {
        if (this.currentState.isThundered()) {
            this.currentState.resolveFromThunder();
        }
        switch (this.currentState.getTargetType()) {
        case CAULDRON:
            if (this.currentState.getActionType() == ActionType.TAKE) {
                this.serverEnvironment.pickGems(this.currentState.getPower(),
                        this.currentState.getHocusCard()
                        .getCardOwner().getId());
            } else {
                    System.err.println("CardsResolver log:");
                    System.err.println("Invalid action tried on cauldron.");
                    System.err.println(
                            this.currentState.getActionType().toString());
            }
            break;
        case LIBRARY:
            if (this.currentState.getActionType() == ActionType.LOOK) {
                this.serverEnvironment.sendFourCardStack(
                        this.currentState
                        .getHocusCard().getCardOwner());
            } else {
                if (this.currentState.getActionType() == ActionType.TAKE) {
                    this.serverEnvironment.drawCards(
                            this.currentState.getPower(),
                            this.currentState.
                            getHocusCard().getCardOwner().getId()
                            );
                    // Inspiration success !
                    this.currentState.setBlockedInspiration(true);
                    ArrayList <Card> playerHand = this.serverEnvironment
                            .getPlayers()
                            .get(this.serverEnvironment.getCurrentPlayer())
                            .getHandCardsList();
                    for (Card handCard : playerHand) {
                        if (handCard instanceof Inspiration) {
                            handCard.setPlayable(false);
                        }
                    }
                    } else {
                    System.err.println("CardsResolver log:");
                    System.err.println("Invalid action tried on library.");
                    System.err.println(
                            this.currentState.getActionType().toString());
                }
            }
            break;
        case PLAYER_GEMS:
            if (this.currentState.getActionType() == ActionType.TAKE) {
                this.serverEnvironment.pickGems(this.currentState.getPower(),
                        this.currentState.getTargetedPlayer().getId(),
                        this.currentState.getHocusCard()
                        .getCardOwner().getId());
            } else {
                if (this.currentState.getActionType() == ActionType.PUT) {
                    this.serverEnvironment.putGems(
                            this.currentState.getTargetedPlayer().getId(),
                            this.currentState.getPower());
                } else {
                    System.err.println("CardsResolver log:");
                    System.err.println("Invalid action tried on cauldron.");
                    System.err.println(
                            this.currentState.getActionType().toString());
                }
            }
            break;
        case PLAYER_HAND:
            if (this.currentState.getActionType() == ActionType.EXCHANGE) {
                this.serverEnvironment.exchangePlayerHands(
                        this.currentState.getHocusCard().getCardOwner().getId(),
                        this.currentState.getTargetedPlayer().getId());
            } else {
                System.err.println("CardsResolver log:");
                System.err.println("Invalid action tried on player hand.");
                System.err.println(
                        this.currentState.getActionType().toString());
            }
            break;
        case GRIMOIRE_FULL:
            if (this.currentState.getActionType() == ActionType.TURN_GRIMOIRE) {
                this.serverEnvironment.turnGrimoires();
            } else {
                System.err.println("CardsResolver log:");
                System.err.println("Invalid action tried on full grimoires.");
                System.err.println(
                        this.currentState.getActionType().toString());
            }
            break;
        case GRIMOIRE_CARD:
            if (this.currentState.getActionType() == ActionType.DESTROY) {
                this.serverEnvironment.destroyCardsFromGrimoire(
                        this.currentState.getTargetedPlayer().getId(),
                        this.currentState.getTargetedCards());
            } else {
                if (this.currentState.getActionType() == ActionType.TAKE) {
                    this.serverEnvironment.stealCardsFromGrimoire(
                            this.currentState.getTargetedPlayer().getId(),
                            this.currentState
                            .getHocusCard().getCardOwner().getId(),
                            this.currentState.getTargetedCards());

                } else {
                    System.err.println("CardsResolver log:");
                    System.err.println("Invalid action tried on "
                            + "grimoire cards.");
                    System.err.println(
                            this.currentState.getActionType().toString());
                }
            }
            break;
        case HOCUS_CARD:
            switch (this.currentState.getActionType()) {
            case DESTROY:
                this.serverEnvironment.discardSession();
                break;
            case PUT:
                this.serverEnvironment.throwInOwnerHand(
                        this.currentState.getHocusCard());
                break;
            case TAKE:
                this.serverEnvironment.takeHocusCard(this.currentState
                        .getHocusCard(), this.currentState.getNewOwner());
                break;
            default:
                System.err.println("CardsResolver log:");
                System.err.println("Invalid action tried on "
                        + "hocus card.");
                System.err.println(
                        this.currentState.getActionType().toString());
                break;
            }
            break;
        default: // equals the TargetType.NONE
            System.err.println("CardsResolver log:");
            System.err.println("Invalid target.");
            System.err.println(
                    this.currentState.getTargetType().toString());
        }
    }

    /**
     * Update the server environment according to the resolution state\
     * blocked card moves.
     */
    private void updateBlockedCards() {
        ArrayList <Card> blockedCards = this.currentState.getBlockedCards();
        for (Card blockedCard : blockedCards) {
            ArrayList <Card> playerHand = this.serverEnvironment.getPlayers()
                    .get(this.serverEnvironment.getCurrentPlayer())
                    .getHandCardsList();
            for (Card handCard : playerHand) {
                if (handCard.equals(blockedCard)) {
                    handCard.setPlayable(false);
                }
            }
        }
    }

    /**
     * Triggered when the server environment finish the turn.
     */
    public void endTurn() {
        this.currentState.resetTurnState();
    }
}
