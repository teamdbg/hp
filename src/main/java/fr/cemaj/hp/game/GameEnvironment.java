/**
 *
 */
package fr.cemaj.hp.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.cemaj.hp.game.card.Card;

/**
 * Abstract class representing a game environment.
 * @author MathieuDegaine
 */
public abstract class GameEnvironment {
/** ------------------------- Attributes -------------------------- **/
    /**
     * A map of identifiers and their corresponding players.
     */
    protected HashMap <Integer, Player> players;
    /**
     * The list of played cards.
     */
    protected ArrayList<Card> playedCards;
    /**
     * The number of gems in  the cauldron.
     */
    protected int cauldron;
    /**
     * The player who it is the turn.
     */
    protected int currentPlayer;
    /**
     * Tells if the game has started or if it is still in lobby.
     */
    protected boolean inGame;
    /**
     * Contains the list of cards in the discard.
     */
    protected ArrayList<Card> discard;
    /**
     * Defines if it is a hocus phase or pocus phase.
     */
    protected boolean pocusPhase;

    /**
     * Defines if the current game is in quick ort normal mode.
     */
    protected boolean quickGame;


    /**
     * Class constructor.
     */
    protected GameEnvironment() {
        super();
        this.discard = new ArrayList<Card>();
        this.currentPlayer = 0;
        this.inGame = false;
        this.players = new HashMap <Integer, Player>();
        this.playedCards = new ArrayList<Card>();
    }

    /**
     * Returns the player list.
     *
     * @return players : list of players
     */
    public HashMap<Integer, Player> getPlayers() {
        return this.players;
    }

    /**
     * Sets the player list.
     *
     * @param plyrs
     *            in the game environment
     */
    public void setPlayers(HashMap<Integer, Player> plyrs) {
        this.players = plyrs;
    }

    /**
     * Returns the list of played cards.
     *
     * @return playedCards
     */
    public List<Card> getPlayedCards() {
        return this.playedCards;
    }

    /**
     * Returns the number of gems.
     *
     * @return cauldron : number of gems in the cauldron
     */
    public int getCauldron() {
        return this.cauldron;
    }

    /**
     * Sets the number of gems in cauldron.
     *
     * @param cldrn : number of gems
     */
    public void setCauldron(int cldrn) {
        this.cauldron = cldrn;
    }

    /**
     * Init the number of gems in the cauldron according to the players \
     * number and the game mode.
     * @param nbPlayer : number of players
     * @throws IllegalArgumentException
     */
    public void initCauldron(int nbPlayer) {
        if (nbPlayer == 1 || nbPlayer < 0 || nbPlayer > 6) {
            throw new IllegalArgumentException();
        }
        switch (nbPlayer) {
        case 0:
            this.cauldron = 15;
            break;
        case 2:
            this.cauldron = 20;
            break;
        case 3:
            this.cauldron = 25;
            break;
        case 4:
            this.cauldron = 30;
            break;
        default:
            this.cauldron = 35;
            break;
        }
    }

    /**
     * Returns the currently playing player.
     *
     * @return currentPlayer : the player that plays its turn
     */
    public int getCurrentPlayer() {
        return this.currentPlayer;
    }

    /**
     * Sets the player turn.
     *
     * @param currPlayer : current player
     */
    public void setCurrentPlayer(int currPlayer) {
        this.currentPlayer = currPlayer;
    }

    /**
     * Adds a played card.
     *
     * @param played Card
     */
    public void addPlayedCard(Card played) {
        this.playedCards.add(played);
    }

    /**
     * Remove all the played cards.
     */
    public void removeAll() {
        this.playedCards.clear();
    }

    /**
     * Remove the card at the selected index.
     *
     * @param index : the index of the card to remove from the played cards
     */
    public void removeCard(int index) {
        this.playedCards.remove(index);
    }

    /**
     * Get the card at the specified index.
     *
     * @param index : the index of the card to get
     * @return Card : the card needed
     */
    public Card getCard(int index) {
        return this.playedCards.get(index);
    }

    /**
     * Return the game status.
     *
     * @return inGame : the game status (true : inGame, false : waiting...)
     */
    public boolean isInGame() {
        return this.inGame;
    }

    /**
     * Add a player to the game.
     *
     * @param player : the Player to add
     */
    public void addPlayer(Player player) {
        this.players.put(player.getId(), player);
    }

    /**
     * Remove a player from the game.
     *
     * @param player : the Player to remove
     * @param isWantedAction (boolean)
     */
    public abstract void removePlayer(Player player, boolean isWantedAction);

    /**
     * Set the ready statement of a player.
     * @param p : the player that needs to be updated
     * @param state : true : player is ready, false : player is not ready
     */
    public abstract void setReadyStatement(Player p, boolean state);

    /**
     * Ending the game.
     */
    public abstract void endGame();

    /**
     * Get the discard.
     * @return discard : return the cards list composing the discard
     */
    public ArrayList<Card> getDiscard() {
        return this.discard;
    }

    /**
     * Set the discard.
     * @param aDiscard to set
     */
    public abstract void setDiscard(ArrayList<Card> aDiscard);
}
