package fr.cemaj.hp.game;

/**
 * Exception thrown by an invalid move.
 * @author Mathieu Degaine,
 * Adrien Faure,
 * Éric Gillet,
 * Jérémy Longin,
 * Clémence Lop
 */
public class InvalidMoveException extends Exception {

}
