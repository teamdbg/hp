/**
 * Curse
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Curse card,
 * it enables to destroy some ennemi's grimoire cards.
 * @author M.D. & C.L.
 */
public class Curse extends CardTargeting {

    private static final long serialVersionUID = 1L;

    /**
     * Curse Constructor.
     * @param power (int) : the power of the Curse card
     */
    public Curse(int power) {
        super(true, power);
        this.setActionType(ActionType.DESTROY);
        this.setTargetType(TargetType.GRIMOIRE_CARD);
    }
}
