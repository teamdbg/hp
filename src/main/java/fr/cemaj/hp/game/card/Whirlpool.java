/**
 * Whirlpool
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Whirlpool card, it makes each player's hand move to
 * the right.
 * @author M.D. & C.L.
 */
public class Whirlpool extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * Whirlpool Constructor.
     */
    public Whirlpool() {
        super(true, 0);
        this.setActionType(ActionType.TURN_GRIMOIRE);
        this.setTargetType(TargetType.GRIMOIRE_FULL);
    }
}
