/**
 * CounterSpell
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the CounterSpell card
 * which enables to suppress a Hocus card.
 * @author M.D. & C.L.
 */
public class CounterSpell extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * CounterSpell Constructor.
     */
    public CounterSpell() {
        super(false, 0);
        this.setActionType(ActionType.DESTROY);
        this.setTargetType(TargetType.HOCUS_CARD);
    }

}
