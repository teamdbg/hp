/**
 * Thief
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Thief card, it enables to steal
 * a specific number of gems from another player's.
 * @author M.D. & C.L.
 */
public class Thief extends PlayerTargeting {

    private static final long serialVersionUID = 1L;

    /**
     * Thief Constructor.
     * @param power (int) : the Thief card power
     */
    public Thief(int power) {
        super(true, power);
        this.setActionType(ActionType.TAKE);
        this.setTargetType(TargetType.PLAYER_GEMS);
    }
}
