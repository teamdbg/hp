/**
 * Hourglass
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Hourglass card
 * which enables to stop a player's game after the cards' resolution.
 * @author M.D. & C.L.
 */
public class Hourglass extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * Hourglass Constructor.
     */
    public Hourglass() {
        super(false, 0);
        this.setActionType(ActionType.END_TURN);
        this.setTargetType(TargetType.NONE);
    }
}
