/**
 * BlackCat
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the BlackCat card,
 * it enables to take a played Hocus card.
 * @author M.D. & C.L.
 */
public class BlackCat extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * BlackCat Constructor.
     */
    public BlackCat() {
        super(false, 0);
        this.setActionType(ActionType.TAKE);
        this.setTargetType(TargetType.HOCUS_CARD);
    }
}
