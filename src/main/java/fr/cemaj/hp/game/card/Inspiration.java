/**
 * Inspiration
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Inspiration card,
 * it enables to draw cards from the library.
 * @author M.D. & C.L.
 */
public class Inspiration extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * Inspiration Constructor.
     * @param power (int) : the Inspiration card power
     */
    public Inspiration(int power) {
        super(true, power);
        this.setActionType(ActionType.TAKE);
        this.setTargetType(TargetType.LIBRARY);
    }
}
