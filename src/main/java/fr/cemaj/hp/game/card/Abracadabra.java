/**
 * Abracadabra
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Abracadabra card,
 * it enables to exchange two player hands.
 * @author M.D. & C.L.
 */
public class Abracadabra extends PlayerTargeting {

    private static final long serialVersionUID = 1L;

    /**
     * Abracadabra Constructor.
     */
    public Abracadabra() {
        super(true, 0);
        this.setActionType(ActionType.EXCHANGE);
        this.setTargetType(TargetType.PLAYER_HAND);
    }
}
