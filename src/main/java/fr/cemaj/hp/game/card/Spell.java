/**
 * Spell
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Spell card, it enables take gems into the cauldron.
 * @author M.D. & C.L.
 */
public class Spell extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * Spell Constructor.
     * @param power (int) : the Spell card power
     */
    public Spell(int power) {
        super(true, power);
        this.setActionType(ActionType.TAKE);
        this.setTargetType(TargetType.CAULDRON);
    }
}
