/**
 * Owl
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Owl card, it enables to steal
 * a specific number of cards from another player's grimoire.
 * @author M.D. & C.L.
 */
public class Owl extends CardTargeting {

    private static final long serialVersionUID = 1L;

    /**
     * Owl Constructor.
     * @param isHocus (boolean) : the Owl card type
     * @param power (int) : the Owl card power
     */
    public Owl(boolean isHocus, int power) {
        super(isHocus, power);
        this.setActionType(ActionType.TAKE);
        this.setTargetType(TargetType.GRIMOIRE_CARD);
    }
}
