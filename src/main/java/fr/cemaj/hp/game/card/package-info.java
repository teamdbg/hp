/**
 * package-info: fr.cemaj.hp.game.card
 *
 * Version 0.1
 *
 * Date 11.26.2013
 *
 * Copyright CEMAJ
 *
 * Developers: J.L.
 */

/**
 * This package contents the classes representing the game cards.
 */
package fr.cemaj.hp.game.card;
