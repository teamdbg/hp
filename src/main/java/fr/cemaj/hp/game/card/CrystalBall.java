/**
 * CristalBall
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the CristalBall card,
 * which enables to order the 4 firsts cards of the library.
 * @author M.D. & C.L.
 */
public class CrystalBall extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * CrystallBall Constructor.
     */
    public CrystalBall() {
        super(true, 0);
        this.setActionType(ActionType.LOOK);
        this.setTargetType(TargetType.LIBRARY);
    }

}
