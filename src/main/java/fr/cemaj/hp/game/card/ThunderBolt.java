/**
 * ThunderBolt
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the ThunderBolt card, it enables to protect a Hocus
 * card from every Pocus cards (except the MagicStaff).
 * @author M.D. & C.L.
 */
public class ThunderBolt extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * Thunderbolt Constructor.
     */
    public ThunderBolt() {
        super(false, 0);
        this.setActionType(ActionType.THUNDER);
        this.setTargetType(TargetType.HOCUS_CARD);
    }
}
