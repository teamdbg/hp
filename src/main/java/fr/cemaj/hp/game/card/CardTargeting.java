/**
 * CardTargeting
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

import java.util.ArrayList;

import fr.cemaj.hp.game.Player;

/**
 * This class describes the generic kind of card
 * that will aim to targeting an ennemi's card.
 * @author M.D. & C.L.
 */
public abstract class CardTargeting extends Targeting {

    private static final long serialVersionUID = 1L;
    private ArrayList<Card> targetedCards;

    /**
     * CardTargeting Constructor.
     * @param isHocus (boolean) : the type of the card to create
     * @param power (int) : the power of the card to create
     */
    public CardTargeting(boolean isHocus, int power) {
        super(isHocus, power);
        this.targetedCards = new ArrayList<Card>();
    }

    /**
     * @return the targetedPlayer
     */
    public Player getTargetedPlayer() {
        return targetedPlayer;
    }

    /**
     * @param targetedPlayer the targetedPlayer to set
     */
    public void setTargetedPlayer(Player targetedPlayer) {
        this.targetedPlayer = targetedPlayer;
    }

    /**
     * @return the targetedCards
     */
    public ArrayList<Card> getTargetedCards() {
        return this.targetedCards;
    }

    /**
     * @param targetedCards the targetedCards to set
     */
    public void setTargetedCards(ArrayList<Card> targetedCards) {
        this.targetedCards = targetedCards;
    }

}
