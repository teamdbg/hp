/**
 * EnchantedMirror
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the EnchantedMirror card which enables to deviate the \
 * focus of a card onto another player (and specify new cards targeted).
 *
 * @author M.D. & C.L.
 */
public class EnchantedMirror extends PlayerTargeting {

    private static final long serialVersionUID = 1L;

    /**
     * EnchantedMirror Constructor.
     */
    public EnchantedMirror() {
        super(false, 0);
        //Comment: no ActionType or TargetType
    }

}
