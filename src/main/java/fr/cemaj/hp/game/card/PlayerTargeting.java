/**
 * PlayerTargeting
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the generic kind of card
 * that will aim to targeting a specific player.
 * @author M.D. & C.L.
 */
public abstract class PlayerTargeting extends Targeting {

    private static final long serialVersionUID = 1L;

    /**
     * PlayerTargeting Constructor.
     * @param isHocus (boolean) : the PlayerTargeting card type
     * @param power (int) : the PlayerTargeting card power
     */
    public PlayerTargeting(boolean isHocus, int power) {
        super(isHocus, power);
    }
}
