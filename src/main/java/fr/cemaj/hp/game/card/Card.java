/**
 * Card
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

import java.io.Serializable;

import fr.cemaj.hp.game.Player;

/**
 * This class describes the generic card.
 * @author M.D. & C.L.
 */
public abstract class Card implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * enum of target type.
     * @author Clémence Lop
     */
    public enum TargetType {
        CAULDRON,
        GRIMOIRE_FULL,
        GRIMOIRE_CARD,
        HOCUS_CARD,
        LIBRARY,
        PLAYER_GEMS,
        PLAYER_HAND,
        NONE
    };

    /**
     * enum of action type.
     * @author Clémence Lop
     */
    public enum ActionType {
        THUNDER,
        DESTROY,
        EXCHANGE,
        LOOK,
        PUT,
        TAKE,
        TURN_GRIMOIRE,
        END_TURN,
        POWER_UP,
        NONE
    }

    private ActionType actionType;
    private Player cardOwner;
    private boolean isHocus;
    private boolean isPlayable;
    private int power;
    private TargetType targetType;

    /**
     * Card class constructor.
     * @param isHoc : indicates if the card is an hocus card
     * @param pow : indicates the power of the card (0 if there is no power)
     */
    public Card(boolean isHoc, int pow) {
        super();
        this.isHocus = isHoc;
        this.power = pow;
        this.isPlayable = true;
        this.targetType = TargetType.NONE;
        this.actionType = ActionType.NONE;
    }

    /**
     * Get the type of the card.
     * @return true if Hocus, false if Pocus
     */
    public boolean isHocus() {
        return this.isHocus;
    }

    /**
     * Get the power of the card.
     * @return power (0 if there is no power)
     */
    public int getPower() {
        return this.power;
    }

    /**
     * Check if the card is playable.
     * @return true if the card is playable
     */
    public boolean isPlayable() {
        return this.isPlayable;
    }

    /**
     * Set if the card is playable.
     * @param isPlybl : true : is playable, false : is not playable
     */
    public void setPlayable(boolean isPlybl) {
        this.isPlayable = isPlybl;
    }

    /**
     * @return String that represents the Card
     */
    @Override
    public String toString() {
        return "Card " + this.getClass() + "[isHocus="
                + this.isHocus + ", power=" + this.power + "]";
    }

    /**
     * @return the actionType
     */
    public ActionType getActionType() {
        return this.actionType;
    }

    /**
     * @param anActionType the actionType to set
     */
    protected void setActionType(ActionType anActionType) {
        this.actionType = anActionType;
    }

    /**
     * @return the targetType
     */
    public TargetType getTargetType() {
        return this.targetType;
    }

    /**
     * @param aTargetType the targetType to set
     */
    protected void setTargetType(TargetType aTargetType) {
        this.targetType = aTargetType;
    }

    /**
     * @return the cardOwner
     */
    public Player getCardOwner() {
        return this.cardOwner;
    }

    /**
     * @param cardOwner the cardOwner to set
     */
    public void setCardOwner(Player cardOwner) {
        this.cardOwner = cardOwner;
    }

    /**
     * Implementation of equals method to compare two cards.
     * Used in player to verify if he already possess the card.
     * @param o : object
     * @return true -> equals | false -> not equals
     */
    public boolean equals(Object o) {
        if(this.getClass() != o.getClass()) {
            return false;
        }
        Card oCard = (Card) o;
        if(this.power != oCard.getPower()
                && this.cardOwner != oCard.getCardOwner()) {
            return false;
        }
        return true;
    }
}
