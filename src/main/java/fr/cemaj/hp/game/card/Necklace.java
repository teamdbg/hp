/**
 * Necklace
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Necklace card
 * it can destroy a card aiming at the user.
 * @author M.D. & C.L.
 */
public class Necklace extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * Necklace Constructor.
     */
    public Necklace() {
        super(false, 0);
        this.setActionType(ActionType.DESTROY);
        this.setTargetType(TargetType.HOCUS_CARD);
    }
}
