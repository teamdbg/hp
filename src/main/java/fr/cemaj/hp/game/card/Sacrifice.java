/**
 * Sacrifice
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Sacrifice card, it forces a player to put some
 * gems into the game cauldron.
 * @author M.D. & C.L.
 */
public class Sacrifice extends PlayerTargeting {

    private static final long serialVersionUID = 1L;

    /**
     * Sacrifice Constructor.
     * @param power (int) : the Sacrifice card power
     */
    public Sacrifice(int power) {
        super(true, power);
        this.setActionType(ActionType.PUT);
        this.setTargetType(TargetType.PLAYER_GEMS);
    }
}
