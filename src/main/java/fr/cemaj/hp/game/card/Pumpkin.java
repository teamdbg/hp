/**
 * Pumpkin
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the Pumpkin card, it enables send a hocus card back
 * into it's player hand.
 * @author M.D. & C.L.
 */
public class Pumpkin extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * Pumpkin Constructor.
     */
    public Pumpkin() {
        super(false, 0);
        this.setActionType(ActionType.PUT);
        this.setTargetType(TargetType.HOCUS_CARD);
    }
}
