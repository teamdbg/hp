/**
 * Targeting
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

import fr.cemaj.hp.game.Player;

/**
 * This class describes a generic card that will need
 * the target's specification.
 * @author M.D. & C.L.
 */
public abstract class Targeting extends Card {

    private static final long serialVersionUID = 1L;
    protected Player targetedPlayer;

    /**
     * Targeting Constructor.
     * @param isHocus (boolean) : the Targeting card type
     * @param power (int) : the Targeting card power
     */
    public Targeting(boolean isHocus, int power) {
        super(isHocus, power);
        this.targetedPlayer = null;
    }

    /**
     * @return the targetedPlayer
     */
    public Player getTargetedPlayer() {
        return this.targetedPlayer;
    }

    /**
     * @param targetedPlayer the targetedPlayer to set
     */
    public void setTargetedPlayer(Player targetedPlayer) {
        this.targetedPlayer = targetedPlayer;
    }
}
