/**
 * MagicStaff
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game.card;

/**
 * This class describes the MagicStaff card
 * which enables to double the power of a Hocus card.
 * @author M.D. & C.L.
 */
public class MagicStaff extends Card {

    private static final long serialVersionUID = 1L;

    /**
     * MagicStaff Constructor.
     */
    public MagicStaff() {
        super(false, 0);
        this.setActionType(ActionType.POWER_UP);
        this.setTargetType(TargetType.HOCUS_CARD);
    }
}
