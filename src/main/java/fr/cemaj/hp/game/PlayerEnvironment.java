/**
 * PlayerEnvironment.java
 *
 * Version 0.1
 *
 * Date 21 dec. 2013
 *
 * Copyright CEMAJ
 *
 * Developers: C.L.
 */
package fr.cemaj.hp.game;

/**
 * Java imports
 */
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Application imports
 */
import fr.cemaj.hp.Client;
import fr.cemaj.hp.game.card.Card;
import fr.cemaj.hp.net.ServerProxy;

/**
 * This class describes the gaming environment of the player (the user).
 * @author Clémence Lop
 */
public class PlayerEnvironment extends GameEnvironment {

    /** -------------------- Attributes --------------------. **/
    private Player player;
    private ServerProxy proxy;
    private Client client;

    /**
     * Class constructor.
     * @param c : the associated Client
     * @param p : the associated Player
     */
    public PlayerEnvironment(Client c, Player p) {
        super();
        this.client = c;
        this.player = p;
    }

    /** -------------------- Methods -------------------- **/

    /** ------ Reactions to server's messages ------ **/

    /**
     * Initialize the game using server's information.
     * @param thePlayers : players in game
     * @param aCauldron : cauldron's number of gems
     */
    public void launchGame(HashMap<Integer, Player> thePlayers, int aCauldron) {
        this.cauldron = aCauldron;
        this.players = thePlayers;
        this.inGame = true;
        this.client.launchGame(this.players.values().toArray(),
                this.cauldron,
                this.player.getId());
    }

    /**
     * Set the current player turn.
     * @param playerId the id of the player able to put hocus cards, etc.
     */
    public void changePlayerTurn(int playerId) {
        if(!this.inGame) {
            //TODO Throw exception !
            System.err.println("PlayerEnvironment log");
            System.err.println("Cannot set the turn of a player if the game "
                    + "is not launched !");
        }
        this.currentPlayer = playerId;
        this.client.nextPlayer(playerId);
    }

    /**
     * Set the ready statement of a player.
     * @param p : the player that needs to be updated
     * @param state : true : player is ready, false : player is not ready
     */
    @Override
    public void setReadyStatement(Player p, boolean state) {
        if(this.inGame) {
            //TODO Throw exception !
            System.err.println("PlayerEnvironment log");
            System.err.println("Cannot set the player ready statement "
                    + "during the game !");
        }
        this.players.get(p.getId()).setReady(state);
        this.client.setReady(p.getId(), state);
    }

    /**
     * After connection: Set the player/user id.
     * @param id to set
     */
    public void setPlayerId(int id) {
        // TODO Actually set player ID, and call appropriate callback in UI
        this.player.setId(id);
    }

    /**
     * getPlayerId : getting the current player id.
     * @return the player id
     */
    public int getPlayerId() {
        return this.player.getId();
    }

    /**
     * InGame: Updates the sent player info.
     * @param aPlayer : the received player
     */
    public void receivePlayer(Player aPlayer) {
        if(this.inGame) {
            // InGame !
            int playerId = aPlayer.getId();
            if(this.players.get(playerId) == null) {
                // The player does not exist in the hashmap ! Error !
                // TODO throw exception !
                System.err.println("PlayerEnvironment log:");
                System.err.println("The received player does not exist. "
                        + "Cannot update its data.");
                System.err.println(playerId);
            } else {
                this.players.put(playerId, aPlayer);
                this.client.updatePlayerBoard(playerId, aPlayer);
            }
        } else {
            // The received player is ignored.
            System.err.println("PlayerEnvironment log:");
            System.err.println("The received player already exist "
                    + "in the lobby. Ignored message.");
            System.err.println("Player ID:");
            System.err.println(aPlayer.getId());
        }
    }

    @Override
    /**
     * This method is used when a player is removed from the lobby.
     */
    public void removePlayer(Player player, boolean isWantedAction) {
        if(!isWantedAction) {
            this.client.connectionLost(player);
        }
        if(!this.inGame) {
            this.players.remove(player.getId());
        }
        this.client.removePlayer(player);
    }
    /**
     * Updates the played cards.
     * @param playedCards the list of played cards
     */
    public void receivePlayedCards(ArrayList<Card> playedCards) {
        // When receiving a card played, obviously the pocus phase activates
        this.pocusPhase = true;
        if(playedCards.size() > 0) {
            Card lastPlayedCard = playedCards.get(playedCards.size() - 1);
            int ownerId = lastPlayedCard.getCardOwner().getId();
            Player owner = this.players.get(ownerId);
            // Add the card to played cards list
            this.playedCards.add(lastPlayedCard);
            this.client.cardPlayed(owner, lastPlayedCard);
        }
    }

    /**
     * Updates the cauldron.
     * @param cauldron number of gems
     */
    public void receiveCauldron(int cauldron) {
        this.cauldron = cauldron;
        this.client.updateCauldron(cauldron);
    }

    @Override
    public void setDiscard(ArrayList<Card> discard) {
        this.discard.clear();
        this.discard = discard;
        this.client.setDiscard(discard);
    }

    /**
     * Prepare for the reorder of the 4 first cards of the stack.
     * Behavior off crystal ball.
     * @param stack containing the four cards to reorder
     */
    public void receiveStackToReorder(ArrayList<Card> stack) {
        Card[] cards = new Card[stack.size()];
        for(int i = 0; i < stack.size(); i++) {
            cards[i] = stack.get(i);
        }
        this.client.displayCristalBallPopup(cards);
    }

    /**
     * End the playing session (hocus and pocus cards).
     */
    public void endSession() {
        this.setDiscard(this.playedCards);
        this.client.resetTargetStates();
        this.playedCards = new ArrayList<Card>();
        this.pocusPhase = false;
    }

    /**
     * Finish game and shows results!
     */
    @Override
    public void endGame() {
        this.inGame = false;
        this.cauldron = 0;
        this.discard = null;
        this.playedCards = null;
        this.client.displayEndGame(this.players.values().toArray());
    }

    /** ------ Actions to send to the server ------ **/

    /**
     * Lobby state: the player set/unset his ready statement.
     * @param state : the player ready statement (true = ready)
     */
    public void sendReadyStatement(boolean state) {
        this.proxy.setReadyStatement(state);
    }

    /**
     * Play a card.
     * @param aCard to play
     * @return true : the card is successfully played | false otherwise
     */
    public boolean playCard(Card aCard) {
        if(!this.pocusPhase && this.currentPlayer == this.player.getId()) {
            //The played card must be a playable hocus card
            if(aCard.isHocus() && aCard.isPlayable()) {
                this.proxy.playCard(aCard);
                if(aCard.getCardOwner().searchInGrimoire(aCard) != null
                        && aCard.getCardOwner().getHandCardsNumber() > 0) {
                    // The card is in the player grimoire
                    // Wait a milisecond to send the completeGrimoire action
                    try {
                        Thread.sleep(1);
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return true;
            } else {
                System.out.println("PlayerEnvironment log:");
                System.out.println("Please put a (playable) hocus card.");
                return false;
            }
        } else {
            if(this.pocusPhase) {
                // The player can put pocus cards
                if(!aCard.isHocus()) {
                    this.proxy.playCard(aCard);
                    if(aCard.getCardOwner().searchInGrimoire(aCard) != null) {
                        // The card is in the player grimoire
                        // Wait a milisecond to send the completeGrimoire action
                        try {
                            Thread.sleep(1);
                        } catch(InterruptedException e) {
                            e.printStackTrace();
                        }
                        // TODO Complétion du grimoire : il faut que ça soit
                        // asynchrone, l'interface peut pas le gérer au moment
                        // du playCard
                    }
                    return true;
                }
            } else {
                // The player must wait
                System.out.println("PlayerEnvironment log:");
                System.out.println("Cannot play a pocus card now.");
                return false;
            }
        }
        return false;
    }

    /**
     * Exchange a hand card with a grimoire card.
     * Can only be used during the user turn.
     * Can only be used when no card has been played.
     * @param handCard : hand card
     * @param grimoireCard : grimoir crad
     * @return true : exchange succeed | false otherwise
     */
    public boolean exchangeHandCardsWithGrimoire(Card handCard
            , Card grimoireCard) {
        // Can only be used during the user turn
        if(this.currentPlayer == this.player.getId()) {
            // Can only be used when no card has been played
            //if(this.playedCards.isEmpty() || this.playedCards == null) {
                this.proxy.exchangeGrimoireHand(handCard, grimoireCard);
                return true;
            //} else {
            //    return false;
            //}
        } else {
            return false;
        }
    }

    /**
     * Complete the grimoire with a hand card.
     * @param aHandCard : a hand card
     */
    public void completeGrimoire(Card aHandCard) {
        //TODO selon interface : retirer le if
        if(aHandCard.getCardOwner().searchInHand(aHandCard) != null) {
            // the card is truly a hand card
            this.proxy.completeGrimoire(aHandCard);
        } else {
            //TODO interface Jeremy :
            // relancer l'interface? dépend de comment est fait l'interface
            System.out.println("OhOh, fail");
        }
    }

    /**
     * Finish the turn of the user.
     * @param pickGem true => pick gems ; false => draw cards from the library
     */
    public void endTurn(boolean pickGem) {
        this.proxy.sendEndTurn(pickGem);
    }

    /**
     * When the player has reordered the 4 first cards of the stack (crystal).
     * @param aStack to send
     */
    public void sendFourCardStack(ArrayList<Card> aStack) {
        if(aStack.size() == 4) {
            this.proxy.sendReorderedStack(aStack);
        } else {
            System.err.println("PlayerEnvironment log:");
            System.err.println("The reordered stack cards "
                    + "are not in a correct number");
            System.err.println(aStack.size());
        }
    }

    /**
     * setProxy.
     * @param sp : ServerProxy
     */
    public void setProxy(ServerProxy sp) {
        this.proxy = sp;
    }

    @Override
    public void addPlayer(Player player) {
        super.addPlayer(player);
        this.client.addPlayer(player);
    }

    /**
     * Leave the game by the player.
     */
    public void leaveGame() {
        this.proxy.playerLeft(this.player);
    }

    /**
     * setGameMode.
     * @param quick (boolean)
     */
    public void setGameMode(boolean quick) {
        this.client.updateGameMode(quick);
    }

    /**
     * connectionLost.
     * @param player (Player)
     */
    public void connectionLost(Player player) {
        this.client.connectionLost(player);
    }

    /**
     * connectionLost.
     */
    public void connectionLost() {
        this.client.connectionLost();
    }

    /**
     * conNotPickLastGem.
     */
    public void canNotPickLastGem() {
        this.client.displayMessage("Impossible de piocher la dernière gemme\n"
                + "du chaudron s'il en résulte une inégalité.");
    }

    /**
     * getHocusPlayed.
     * @return the played hocus card
     */
    public Card getHocusPlayed() {
        for(int i = 0; i < this.playedCards.size(); i++) {
            if(this.playedCards.get(i).isHocus()) {
                return this.playedCards.get(i);
            }
        }
        return null;
    }
}
