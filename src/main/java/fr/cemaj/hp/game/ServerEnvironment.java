/**
 *
 */
package fr.cemaj.hp.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fr.cemaj.hp.Server;
import fr.cemaj.hp.game.card.Card;
import fr.cemaj.hp.game.resolution.CardsResolver;
import fr.cemaj.hp.net.Action;
import fr.cemaj.hp.net.ClientProxy;
import fr.cemaj.hp.net.Order;

/**
 * @author MathieuDegaine
 *
 */
public class ServerEnvironment extends GameEnvironment {

    /** -------------------------Attributes-------------------------. **/

    private Stack stack;
    private HashMap<Integer, ClientProxy> proxies;
    // Playing Timer
    private PutCardTimer putCardTimer;
    private StartGameTimer startGameTimer;
    // Resolution tool
    private CardsResolver resolver;
    private Server server;

    /**
     * Constructor.
     *
     * @param gameMode : define the game mode (true : quick mode, \
     * false : normal mode)
     * @param server (Server)
     */
    public ServerEnvironment(boolean gameMode, Server server) {
        super();
        this.stack = new Stack();
        this.proxies = new HashMap<Integer, ClientProxy>();
        this.quickGame = gameMode;
        this.resolver = new CardsResolver(this);
        this.startGameTimer = new StartGameTimer(this);
        this.server = server;
        // do not initialize the timers or they will be immediately run
    }

    /**
     * @return stack : a Stack containing the played cards
     */
    public Stack getStack() {
        return this.stack;
    }

    /**
     * @param stk
     *            : a Stack
     */
    public void setStack(Stack stk) {
        this.stack = stk;
    }

    /**
     * @return discard : return the cards list composing the discard
     */
    public ArrayList<Card> getDiscard() {
        return this.discard;
    }

    /**
     * @param discd
     *            : setting the discard with a list of Card
     */
    public void setDiscard(ArrayList<Card> discd) {
        this.discard = discd;
        for (Player pl : this.players.values()) {
            this.proxies.get(pl.getId()).sendDiscard(this.getDiscard());
        }
    }

    /**
     * updateDiscard.
     */
    public void updateDiscard() {
        for (Player pl : this.players.values()) {
            this.proxies.get(pl.getId()).sendDiscard(this.getDiscard());
        }
    }

    /**
     * Launching the game when each player is ready.
     */
    public void launchGame() {
        this.inGame = true;
        this.server.stopAccepting();
        this.stack.init(this.getPlayers().size());
        this.stack.shuffle();
        this.currentPlayer = 0;
        if (this.quickGame) {
            this.initCauldron(0);
        } else {
            this.initCauldron(this.getPlayers().size());
        }
        // Give cards to players !
        for (Player player : this.players.values()) {
            int i = 0;
            for (i = 0; i < 5; i++) {
                player.addHandCard(this.stack.getStack().get(0));
                this.stack.getStack().remove(0);
            }
            for (i = 5; i < 8; i++) {
                player.addGrimoireCard(this.stack.getStack().get(0));
                this.stack.getStack().remove(0);
            }
        }
        for (ClientProxy cp : this.proxies.values()) {
            cp.initGame(this.getPlayers(), this.getCauldron());
            cp.changePlayerTurn(0);
        }
    }

    /**
     * Shuffle the GAME POPO.
     *
     * @param array : a list of the cards
     */
    public void shuffle(ArrayList<Card> array) {
        Collections.shuffle(array);
    }

    /**
     * Add a player to the game.
     *
     * @param p : player to add
     * @param cp : corresponding client proxy
     */
    public void addPlayer(Player p, ClientProxy cp) {
        this.players.put(p.getId(), p);
        this.proxies.put(p.getId(), cp);
    }

    /**
     * @param p
     *            : The player that need to be updated
     * @param state
     *            : the state to set for the Player p
     */
    public void setReadyStatement(Player p, boolean state) {
        // Don't do anything if the game is already started
        if (this.isInGame()) {
            return;
        }
        boolean ready = true;
        p.setReady(state);
        for (Player pl : this.players.values()) {
            if (!pl.getReady()) {
                ready = false;
            }
            this.proxies.get(pl.getId()).playerReady(p, state);
        }
        // Is everyone ready ?
        if (ready && this.players.size() >= 2) {
            this.startGameTimer.initializeTimer();
        } else {
            this.startGameTimer.cancelTimer();
        }
    }

    /**
     * endGame.
     */
    public void endGame() {
        for (Player p : players.values()) {
            System.out.println(p);
        }
    }

    /**
     * Pick gems from the cauldron to a player.
     *
     * @param number : number of gems to pick
     * @param id : player id who do the action
     * @return true if action succeed | false otherwise
     */
    public boolean pickGems(int number, int id) {
        boolean canPickLastGem = false;
        if (getCauldron() > number) {
            setCauldron(getCauldron() - number);
            players.get(id).setGems(players.get(id).getGems() + number);
        } else {
            for (Player pl : this.players.values()) {
                if ((pl.getId() != id)
                        && (pl.getGems() != players.get(id).getGems()
                                + getCauldron())) {
                    // A player picked the last gems, end the game.
                    players.get(id).setGems(players.get(id).getGems() + number);
                    canPickLastGem = true;
                }
            }
            if (!canPickLastGem) {
                this.canNotPickLastGem(id);
                return false;
            }
        }
        for (Player pl : this.players.values()) {
            // Update the player's gem count
            this.proxies.get(pl.getId()).sendPlayer(players.get(id));
            // Update the cauldron's gem count
            this.proxies.get(pl.getId()).sendCauldron(this.cauldron);
        }
        if (canPickLastGem) {
            this.finishGame();
        }
        return true;
    }

    private void canNotPickLastGem(int id) {
        for (Player pl : this.players.values()) {
            if (pl.getId() == id) {
                this.proxies.get(pl.getId()).send(
                        new Action(Order.CANNOTPICKLASTGEM));
                break;
            }
        }
    }

    /**
     * finishGame.
     */
    private void finishGame() {
        for (ClientProxy prox : this.proxies.values()) {
            prox.send(new Action(Order.ENDGAME));
        }
    }

    /**
     * Pick gems from a player to another player.
     *
     * @param number : number of gems to pick
     * @param sourceId : source player id
     * @param destinationId : destination player id
     */
    public void pickGems(int number, int sourceId, int destinationId) {
        int i;
        if (players.get(sourceId).getGems() > number) {
            players.get(sourceId)
                .setGems(players.get(sourceId).getGems() - number);
            players.get(destinationId).setGems(
                    players.get(destinationId).getGems() + number);
            for (Player pl : this.players.values()) {
                this.proxies.get(pl.getId())
                    .sendPlayer(this.players.get(sourceId));
                this.proxies.get(pl.getId()).sendPlayer(
                        this.players.get(destinationId));
            }
        } else {
            i = players.get(sourceId).getGems();
            players.get(sourceId).setGems(0);
            players.get(destinationId).setGems(
                    players.get(destinationId).getGems() + i);
        }
        for (Player pl : this.players.values()) {
            this.proxies.get(pl.getId()).sendPlayer(this.players.get(sourceId));
            this.proxies.get(pl.getId()).sendPlayer(
                    this.players.get(destinationId));
        }
    }

    /**
     * Allows a player to draw card from the stack.
     *
     * @param number : number of cards to draw
     * @param id : id of the player who do the action
     */
    public void drawCards(int number, int id) {
        int totalNumberOfCards = number
                + players.get(id).getGrimoireCardsList().size()
                + players.get(id).getHandCardsList().size();
        if (totalNumberOfCards > 8) {
            number = number + (8 - totalNumberOfCards);
        }
        if (this.stack.getStack().size() < number) {
            this.shuffle(discard);
            this.stack.getStack().addAll(discard);
            discard.clear();
        }
        for (int i = 0; i < number; i++) {
            this.stack.getStack().get(0).setCardOwner(players.get(id));
            if (players.get(id).getGrimoireCardsList().size() >= 3) {
                players.get(id).addHandCard(this.stack.getStack().get(0));
            } else {
                players.get(id).addGrimoireCard(this.stack.getStack().get(0));
            }
            this.stack.getStack().remove(0);
        }
        this.updatePlayer(id);
    }

    /**
     * Take gems from a player and put them in the cauldron.
     *
     * @param id : the player who must put gems back to the cauldron
     * @param power : the number of gems to put
     */
    public void putGems(int id, int power) {
        if (power < players.get(id).getGems()) {
            players.get(id).setGems(players.get(id).getGems() - power);
            cauldron = cauldron + power;
        } else {
            cauldron = cauldron + players.get(id).getGems();
            players.get(id).setGems(0);
        }
        this.updateCauldron();
        this.updatePlayer(id);
    }

    /**
     * Exchange the hands of player 1 and 2.
     *
     * @param id1 : the id of the player 1
     * @param id2 : the id of the player 2
     */
    public void exchangePlayerHands(int id1, int id2) {
        ArrayList<Card> tmp;
        tmp = players.get(id1).getHandCardsList();
        players.get(id1).setHandCardsList(players.get(id2).getHandCardsList());
        players.get(id2).setHandCardsList(tmp);
        players.get(id1).changeHandOwner();
        players.get(id2).changeHandOwner();
        updatePlayer(id1);
        updatePlayer(id2);
    }

    /**
     * Let the grimoires turn between players ! (turn left).
     */
    public void turnGrimoires() {
        ArrayList<Card> tmp = new ArrayList<Card>();
        tmp.addAll(players.get(0).getGrimoireCardsList());
        for (int i = 0; i < players.size(); i++) {
            if (i != (players.size() - 1)) {
                players.get(i).setGrimoireCardsList(
                        players.get(i + 1).getGrimoireCardsList());
            } else {
                players.get(i).setGrimoireCardsList(tmp);
            }
            players.get(i).changeGrimoireOwner();
            this.updatePlayer(i);
        }
    }

    /**
     * Destroy the cards given from the targeted player's grimoire.
     *
     * @param playerId : player id
     * @param targetedCards : cards targeted
     */
    public void destroyCardsFromGrimoire(int playerId,
            ArrayList<Card> targetedCards) {
        /*
         * Note: pour chaque carte visée chercher la carte égale dans le
         * grimoire du joueur. Si toutes les cartes visées n'ont pas été
         * trouvées : erreur
         */
        for (Card card : targetedCards) {
            if (!players.get(playerId).getGrimoireCardsList().remove(card)) {
                System.err.println("Invalid card.");
            } else {
                this.discard.add(card);
            }
        }
        this.updatePlayer(playerId);
    }

    /**
     * Steal cards from another player.
     *
     * @param targetID : the player who will be stolen
     * @param destinationId : the player stealing cards
     * @param targetedCards : the targeted cards
     */
    public void stealCardsFromGrimoire(int targetID, int destinationId,
            ArrayList<Card> targetedCards) {
        /*
         * Note: pour chaque carte visée chercher la carte égale dans le
         * grimoire du joueur. Si toutes les cartes visées n'ont pas été
         * trouvées : erreur
         */
        boolean test = false;
        for (Card card : targetedCards) {
            test = players.get(targetID).getGrimoireCardsList().contains(card);
            //System.out.println("player : "+players.get(targetID).getName()+"
            // has card : "+card.getClass().getSimpleName()+" ?");
            if (!test) {
                System.err.println("Invalid card.");
                break;
            }
        }
        if (test) {
            for (Card card2 : targetedCards) {
                players.get(targetID).removeGrimoireCard(card2);
                card2.setCardOwner(players.get(destinationId));
                if (players.get(destinationId)
                        .getGrimoireCardsList().size() < 3) {
                    players.get(destinationId).addGrimoireCard(card2);
                } else if (players.get(destinationId)
                        .getHandCardsList().size() < 5) {
                    players.get(destinationId).addHandCard(card2);
                } else {
                    discard.add(card2);
                }
            }
        }
        this.updatePlayer(targetID);
        this.updatePlayer(destinationId);
    }

    /**
     * End a hocus-pocus session.
     */
    public void endSession() {
        this.resolver.sessionResolution();
        for (Player pl : this.players.values()) {
            this.proxies.get(pl.getId()).endSession();
        }
        this.putCardTimer.cancelTimer();
    }

    /**
     * Discard the cards of a hocus-pocus session at the end of a session.
     */
    public void discardSession() {
        discard.addAll(playedCards);
        playedCards.clear();
        this.updateDiscard();
        this.updatePlayedCards();
    }

    /**
     * Throw a card into his owner card.
     *
     * @param hocusCard (Card)
     */
    public void throwInOwnerHand(Card hocusCard) {
        int id;
        id = hocusCard.getCardOwner().getId();
        hocusCard.setPlayable(false);
        players.get(hocusCard.getCardOwner().getId()).addHandCard(hocusCard);
        playedCards.remove(hocusCard);
        discard.addAll(playedCards);
        playedCards.clear();
        this.updateDiscard();
        this.updatePlayer(id);
        this.updatePlayedCards();
    }

    /**
     * Take the played hocus card (black cat action).
     *
     * @param hocusCard (Card)
     * @param player (Player)
     */
    public void takeHocusCard(Card hocusCard, Player player) {
        hocusCard.setCardOwner(player);
        if (players.get(player.getId()).getGrimoireCardsList().size() < 3){
            players.get(player.getId()).addGrimoireCard(hocusCard);
        }
        else if (players.get(player.getId()).getHandCardsList().size() < 5) {
            players.get(player.getId()).addHandCard(hocusCard);
        }
        playedCards.remove(hocusCard);
        discard.addAll(playedCards);
        playedCards.clear();
        this.updateDiscard();
        this.updatePlayer(player.getId());
        this.updatePlayedCards();
    }

    /**
     * Signal the change of target after a enchanted mirror. Asks for new
     * targeted cards to the client if it is needed.
     *
     * @param targetId : the new targeted player id
     * @param cardsNumber : the number of cards to target
     */
    public void changeTarget(int targetId, int cardsNumber) {
        // TODO il faut cancel le timer si cardsnumber >0 (on attend que le
        // joueur ait mis à jour les cartes ciblées
        // TODO envoyer le message correspondant
        System.err.println("ServerEnvironment log:");
        System.err.println("Not developped yet feature.");
    }

    /**
     * Receive the changed target cards after an enchanted mirror.
     *
     * @param targetedCards : a list of cards
     */
    public void updateTarget(ArrayList<Card> targetedCards) {
        // TODO relancer le timer !
        // TODO mettre à jour le resolution state avec les nouvelles cibles
        System.err.println("ServerEnvironment log:");
        System.err.println("Not developped yet feature.");
    }

    @Override
    public void removePlayer(Player player, boolean isWantedAction) {
        if (!players.containsKey(player.getId())) {
            return;
        }
        if (this.inGame) {
            this.inGame = false;
            // For the moment, a player leaving stop the game.
            this.endGame();
        } else {
            // In the lobby, simply update the players lobbies
            this.players.remove(player.getId());
            this.proxies.get(player.getId()).dropPlayer();
            this.proxies.remove(player.getId());
            for (ClientProxy cp : this.proxies.values()) {
                if (isWantedAction) {
                    cp.playerLeft(player);
                } else {
                    cp.playerLost(player);
                }
            }
        }
    }

    /**
     * updatePlayer.
     * @param id : id
     */
    public void updatePlayer(int id) {
        for (Player pl : this.players.values()) {
            this.proxies.get(pl.getId()).sendPlayer(this.players.get(id));
        }
    }

    /**
     * updatePlayedCards.
     */
    public void updatePlayedCards() {
        for (Player pl : this.players.values()) {
            this.proxies.get(pl.getId()).sendPlayedCards(this.playedCards);
        }
    }

    /**
     * updateCurrentPlayer.
     */
    public void updateCurrentPlayer() {
        for (Player pl : this.players.values()) {
            this.proxies.get(pl.getId()).changePlayerTurn(currentPlayer);
        }
    }

    /**
     * updateCauldron.
     */
    public void updateCauldron() {
        for (Player pl : this.players.values()) {
            this.proxies.get(pl.getId()).sendCauldron(cauldron);
        }
    }

    // --------------------ACTIONS DECLENCHED BY THE
    // PROXIES---------------------

    /**
     * Process a played card (received from a clientProxy).
     *
     * @param receivedCard : from a clientProxy
     */
    public void playCard(Card receivedCard) {
        // Get the card owner ID
        int playerId = receivedCard.getCardOwner().getId();
        Player cardPlayer = null;
        if (this.players.containsKey(playerId)) {
            // Get a reference on the player who played the card
            cardPlayer = this.players.get(playerId);
            if (cardPlayer.getHandCardsList().contains(receivedCard)
                    || cardPlayer.getGrimoireCardsList()
                        .contains(receivedCard)) {
                // Case: the card do exist in the player hand: process!
                if (receivedCard.isHocus()) {
                    this.putCardTimer = new PutCardTimer(this);
                } else {
                    this.putCardTimer.resetTimer();
                }
                if (cardPlayer.getHandCardsList().contains(receivedCard)) {
                    cardPlayer.getHandCardsList().remove(receivedCard);
                } else if (cardPlayer.getGrimoireCardsList().contains(
                        receivedCard)) {
                    cardPlayer.getGrimoireCardsList().remove(receivedCard);
                }
                this.updatePlayer(playerId);
                this.getPlayedCards().add(receivedCard);
                this.updatePlayedCards();
                this.resolver.resolveCard(receivedCard);
            } else {
                // Case: the card do not exist in the player hand: error!
                System.err.println("ServerEnvironment log: playCard");
                System.err.println("Cannot find the card in the player hand"
                        + " or grimoire.");
            }
        } else {
            // Case: the player does not seem to exist.
            System.err.println("ServerEnvironment log: playCard");
            System.err.println("Invalid card owner.");
        }
    }

    /**
     * During its turn, a player can manage his grimoire. This method enables
     * the player to complete his grimoire with hand cards.
     *
     * @param aHandCard : the card to add to the grimoire.
     */
    public void completeGrimoire(Card aHandCard) {
        if (aHandCard.getCardOwner().getGrimoireCardsNumber() >= 3) {
            System.err.println("Too much card in grimoire");
        } else {
            players.get(aHandCard.getCardOwner().getId())
                    .getGrimoireCardsList().add(aHandCard);
        }
        this.updatePlayer(aHandCard.getCardOwner().getId());
    }

    /**
     * During its turn a player can manage his grimoire. Enable the exchange \
     * of cards between the player hand and his grimoire.
     * @param handCard (Card)
     * @param grimoireCard (Card)
     */
    public void exchangeHandCardsWithGrimoire(Card handCard,
            Card grimoireCard) {
        if (grimoireCard != null && handCard != null) {
            if (handCard.getCardOwner().getId() != grimoireCard.getCardOwner()
                    .getId()) {
                System.err.println("ServerEnvironment log: "
                        + "exchangeHandCardsWithGrimoire");
                System.err.println("Invalid card owner.");
            } else {
                Player player;
                player = players.get(handCard.getCardOwner().getId());
                int handCardPos = player.getHandCardsList().indexOf(handCard);
                int grimCardPos = player.getGrimoireCardsList().indexOf(
                        grimoireCard);
                player.getHandCardsList().remove(handCard);
                player.getGrimoireCardsList().remove(grimoireCard);
                player.getHandCardsList().add(handCardPos, grimoireCard);
                player.getGrimoireCardsList().add(grimCardPos, handCard);
                this.updatePlayer(player.getId());
            }
        } else if (grimoireCard == null && handCard != null) {
            Player player;
            player = players.get(handCard.getCardOwner().getId());
            player.getHandCardsList().remove(handCard);
            player.getGrimoireCardsList().add(handCard);
            this.updatePlayer(player.getId());
        } else {
            Player player;
            player = players.get(grimoireCard.getCardOwner().getId());
            player.getGrimoireCardsList().remove(grimoireCard);
            player.getHandCardsList().add(grimoireCard);
            this.updatePlayer(player.getId());
        }
    }

    /**
     * End the current player turn and allows him to pick a gem or to draw two\
     * cards.
     *
     * @param pickGem : true if the current player wants to pick a gem false \
     * if he want to pick cards
     */
    public void endPlayerTurn(boolean pickGem) {
        boolean erreur = false;
        if (pickGem) {
            if (!this.pickGems(1, currentPlayer)) {
                erreur = true;
            }
        } else {
            this.drawCards(2, currentPlayer);
        }
        if (!erreur) {
            currentPlayer = (currentPlayer + 1) % players.size();
            this.updateCurrentPlayer();
        }
    }

    /**
     * sendFourCardStack.
     * @param player : player
     */
    public void sendFourCardStack(Player player) {
        ArrayList<Card> stack = new ArrayList<>();
        if (this.stack.getStack().size() < 4) {
            this.shuffle(discard);
            this.stack.getStack().addAll(discard);
            discard.clear();
        }
        for (int i = 0; i < 4; i++) {
            stack.add(this.stack.getStack().get(i));
        }
        this.proxies.get(player.getId()).sendStack(stack);
    }

    /**
     * finishCardReorder.
     * @param cardList : list of cards
     */
    public void finishCardReorder(ArrayList<Card> cardList) {
        for (int i = 0; i < 4; i++) {
            this.stack.getStack().remove(0);
        }
        for (int i = 3; i >= 0; i--) {
            this.stack.getStack().add(0, cardList.get(i));
        }
    }

    /**
     * notifyNewPlayer.
     * @param p : player
     */
    public void notifyNewPlayer(Player p) {
        for (Map.Entry<Integer, ClientProxy> cp : this.proxies.entrySet()) {
            if (cp.getKey().equals(p.getId())) {
                // If p is the new Player, throw him all players
                for (Player pl : players.values()) {
                    cp.getValue().playerJoined(pl);
                }
            } else {
                // This is an already connected player, just send him the new
                // player.
                cp.getValue().playerJoined(p);
            }
        }
    }

    /**
     * isQuickGame.
     * @return the game mode (boolean) : true -> quick | false -> normal
     */
    public boolean isQuickGame() {
        return quickGame;
    }
}
