/**
 * Player.java
 *
 * Version 0.1
 *
 * Date 11.25.2013
 *
 * Copyright CEMAJ
 *
 * Developers: M.D. & C.L.
 */

package fr.cemaj.hp.game;

import java.io.Serializable;
import java.util.ArrayList;

import fr.cemaj.hp.game.card.Card;

/**
 * This class describes the player. It is used from the step of joining a party
 * to the end of the game.
 *
 * @author M.D. & C.L.
 */
public class Player implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private ArrayList<Card> grimoireCardsList;
    private ArrayList<Card> handCardsList;
    private boolean ready;
    private int gems;
    private String name;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    /**
     * Creates a Player.
     */
    public Player() {
        this.grimoireCardsList = new ArrayList<Card>();
        this.handCardsList = new ArrayList<Card>();
    }

    /**
     * Creates a player.
     *
     * @param anId
     *            the identifier of the player
     */
    public Player(int anId) {
        this();
        this.setId(anId);
    }

    /**
     * Returns the player identifier.
     *
     * @return The player identifier
     */
    public int getId() {
        return this.id;
    }

    /**
     * Set the player ID.
     *
     * @param anId
     *            to set
     */
    public void setId(int anId) {
        this.id = anId;
    }

    /**
     * Returns the number of cards in the player hand.
     *
     * @return Number of cards in player's hand (0 if empty or out of game)
     */
    public int getHandCardsNumber() {
        return this.handCardsList.size();
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param aName
     *            the name to set
     */
    public void setName(String aName) {
        this.name = aName;
    }

    /**
     * It returns the number of cards in the player grimoire.
     *
     * @return grimoireCardsNumber : Number of cards in player's \ grimoire (0 \
     *         if empty or out of game)
     */
    public int getGrimoireCardsNumber() {
        return this.grimoireCardsList.size();
    }

    /**
     * Returns the list of cards in the player's grimoire.
     *
     * @return grimoireCardsList
     */
    public ArrayList<Card> getGrimoireCardsList() {
        return this.grimoireCardsList;
    }

    /**
     * Sets the list of cards in the player's grimoire from scratch. (erase
     * previous grimoire if not empty)
     *
     * @param cardsList
     *            : list of cards composing the grimoire
     */
    public void setGrimoireCardsList(ArrayList<Card> cardsList) {
        this.grimoireCardsList = cardsList;
    }

    /**
     * Returns the list of cards in the player's hand.
     *
     * @return handCardsList : list of cards composing the player's hand
     */
    public ArrayList<Card> getHandCardsList() {
        return this.handCardsList;
    }

    /**
     * Sets the list of cards in the player's hand from scratch.(erase previous
     * hand if not empty).
     *
     * @param cardsList
     *            : list of cards that compose the player's hand
     */
    public void setHandCardsList(ArrayList<Card> cardsList) {
        this.handCardsList = cardsList;
    }

    /**
     * Add a card to the player's hand.
     *
     * @param card
     *            : the Card to add
     */
    public void addHandCard(Card card) {
        card.getPower();
        card.setCardOwner(this);
        this.handCardsList.add(card);
    }

    /**
     * Remove a card from the player's hand.
     *
     * @param card : the Card to remove
     */
    public void removeHandCard(Card card) {
        card.setCardOwner(null);
        this.handCardsList.remove(card);
    }

    /**
     * Remove a card from the player's grimoire.
     *
     * @param card : the Card to remove
     */
    public void removeGrimoireCard(Card card) {
        card.setCardOwner(null);
        this.grimoireCardsList.remove(card);
    }

    /**
     * Add a card to the player's grimoire.
     *
     * @param card : the Card to add
     */
    public void addGrimoireCard(Card card) {
        card.setCardOwner(this);
        this.grimoireCardsList.add(card);
    }

    /**
     * Set the player ready state.
     *
     * @param state : state of the player
     */
    public void setReady(boolean state) {
        this.ready = state;
    }

    /**
     * Get the player's state.
     *
     * @return return the player state
     */
    public boolean getReady() {
        return this.ready;
    }

    /**
     * Get the players gems.
     *
     * @return the number of gems
     */
    public int getGems() {
        return this.gems;
    }

    /**
     * Set the gems.
     *
     * @param aGemNumber : number to set
     */
    public void setGems(int aGemNumber) {
        this.gems = aGemNumber;
    }

    /**
     * Research the card in the player's hand.
     *
     * @param aCard : card to search
     * @return the card reference, null if not found
     */
    public Card searchInHand(Card aCard) {
        if (this.handCardsList.contains(aCard)) {
            return aCard;
        }
        return null;
    }

    /**
     * Research the card in the player's grimoire.
     *
     * @param aCard : a card to search
     * @return the card reference, null if not found
     */
    public Card searchInGrimoire(Card aCard) {
        try {
            return this.grimoireCardsList
                    .get(this.grimoireCardsList.indexOf(aCard));
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
    /**
     * Used to change the owner of the grimoire after a vortex.
     */
    public void changeGrimoireOwner() {
        for (Card card : this.grimoireCardsList) {
            card.setCardOwner(this);
        }
    }

    /**
     * changeHandOwner : change the card owner.
     */
    public void changeHandOwner() {
        for (Card card : this.handCardsList) {
            card.setCardOwner(this);
        }
    }

    @Override
    public String toString() {
        return "Player : " + "\n\tName : " + this.name
                + "\n\tID : " + this.id;
    }
}
