/**
 * PutCardTimer.java
 *
 * Version 0.1
 *
 * Date 13 dec. 2013
 *
 * Copyright CEMAJ
 *
 * Developers: C.L.
 */
package fr.cemaj.hp.game;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class is used to simulate a timer to end card playing sessions.
 */
public class StartGameTimer {

    private Timer timer;
    private static int timeout = 5;
    private ServerEnvironment environment;
    private TimerTask task;

    /**
     * Create and initialize a card timer.
     * @param anEnvironment (ServerEnvironment)
     */
    public StartGameTimer(ServerEnvironment anEnvironment) {
        this.environment = anEnvironment;
    }

    /**
     * Reset timer.
     */
    public void resetTimer() {
        this.timer.cancel();
        System.out.println("Timer reseted.");
        this.initializeTimer();
    }

    /**
     * Initialize the timer.
     */
    public void initializeTimer() {
        this.timer = new Timer();
        this.task = new StartGameTask(this.environment);
        this.timer.schedule(this.task, timeout * 1000);
        System.out.format("Timer initialized.");
    }

    /**
     * Cancel the timer.
     */
    public void cancelTimer() {
        if (this.timer != null) {
            this.timer.cancel();
            System.out.println("Timer canceled !");
        }
    }

    /**
     * StartGameTask.
     */
    private class StartGameTask extends TimerTask {

        private ServerEnvironment environment;

        public StartGameTask(ServerEnvironment serverEnvironment) {
            this.environment = serverEnvironment;
        }

        @Override
        public void run() {
            System.out.format("Time's up!");
            this.environment.launchGame();
            timer.cancel(); // Terminate the timer thread
        }
    }
}
