package fr.cemaj.hp.gui;

import java.awt.Color;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * This class represents the popup displayed when the user want to join a game\
 * on a server already created.
 *
 * @author Jérémy Longin
 */
public class JoinGamePopup extends JDialog {

    private static final long serialVersionUID = 1L;

    JLabel nameLabel;
    JLabel erreurNameLabel;
    JLabel ipLabel;
    JLabel erreurIpLabel;
    JTextField nameField;
    JTextField ipField;
    JButton validateBut;
    JButton cancelBut;
    MainFrame mainFrame;

    /**
     * Class constructor.
     *
     * @param mf : a reference to the main frame
     * @param listener : all GUI listeners
     */
    JoinGamePopup(MainFrame mf, InterfaceListeners listener) {
        super();

        this.mainFrame = mf;
        if (this.mainFrame.isMac()) {
            this.setSize(350, 200);
        } else {
            this.setSize(350, 220);
        }
        this.setTitle("Rejoindre une partie");
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setAlwaysOnTop(true);
        this.setModal(true);

        // Initialisation popup elements
        this.nameLabel = new JLabel("Entrer un pseudonyme");
        this.nameField = new JTextField();
        this.erreurNameLabel = new JLabel("Pseudo invalide");
        this.erreurNameLabel.setVisible(false);
        this.ipLabel = new JLabel("Entrer l'adresse IP du serveur");
        this.ipField = new JTextField("127.0.0.1");
        this.erreurIpLabel = new JLabel("IP invalide");
        this.erreurIpLabel.setVisible(false);
        this.validateBut = new JButton("Ok");
        this.validateBut.addActionListener(listener.actionListener);
        this.cancelBut = new JButton("Annuler");
        this.cancelBut.addActionListener(listener.actionListener);

        // Set layout
        this.setLayout(null);
        this.setLocationRelativeTo(null);

        // Add elements
        this.add(this.nameLabel);
        this.add(this.nameField);
        this.add(this.erreurNameLabel);
        this.add(this.ipLabel);
        this.add(this.ipField);
        this.add(this.erreurIpLabel);
        this.add(this.validateBut);
        this.add(this.cancelBut);

        // Placement des éléments
        int topSpace = 10;
        int height = 20;
        int width = 300;
        this.nameLabel.setBounds(10, topSpace, width, height);
        this.nameField.setBounds(20, height + topSpace, width, height);
        this.erreurNameLabel
                .setBounds(10, height * 2 + topSpace, width, height);
        this.ipLabel.setBounds(10, height * 3 + 5 + topSpace, width, height);
        this.ipField.setBounds(20, height * 4 + 5 + topSpace, width, height);
        this.erreurIpLabel.setBounds(10, height * 5 + 5 + topSpace, width,
                height);
        this.validateBut.setBounds(180, height * 6 + 10 + topSpace, 80,
                height + 10);
        this.cancelBut.setBounds(260, height * 6 + 10 + topSpace, 80,
                height + 10);

        // this.setVisible(true);
        this.addKeyListener(listener.keyListener);
        this.nameField.addKeyListener(listener.keyListener);
        this.ipField.addKeyListener(listener.keyListener);
    }

    /**
     * Function to validate the form.
     */
    public void validation() {
        boolean erreur;
        Pattern r;
        erreur = false;
        r = Pattern.compile("^[a-zA-Z0-9]{3,16}$");
        if (!(r.matcher(this.nameField.getText()).find())) {
            this.erreurNameLabel.setForeground(Color.red);
            this.erreurNameLabel.setVisible(true);
            erreur = true;
        } else {
            this.erreurNameLabel.setVisible(false);
        }
        r = Pattern
                .compile("^(?:(?:[1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
        if (!(r.matcher(this.ipField.getText()).find())) {
            this.erreurIpLabel.setForeground(Color.red);
            this.erreurIpLabel.setVisible(true);
            erreur = true;
        } else {
            this.erreurIpLabel.setVisible(false);
        }
        if (!erreur) {
            // JOptionPane.showMessageDialog(this.mainFrame,
            // "Connexion au serveur " + ipField.getText());
            if (this.mainFrame.client.connect(this.nameField.getText(),
                    this.ipField.getText())) {
                this.dispose();
                this.mainFrame.changePanel(this.mainFrame.getLobbyPan());
            } else {
                JOptionPane.showMessageDialog(this,
                        "Impossible de se connecter au serveur à l'adresse "
                                + this.ipField.getText()
                                + ".\nVérifier l'adresse et réessayer...",
                        "Impossible de rejoindre le serveur",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
