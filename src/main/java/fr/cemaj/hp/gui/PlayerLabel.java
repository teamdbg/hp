package fr.cemaj.hp.gui;

import fr.cemaj.hp.game.Player;

/**
 * This class represents a player on GUI.
 *
 * @author Jérémy Longin
 */
public class PlayerLabel extends PictureLabel {

    private static final long serialVersionUID = 1L;

    private Player player;

    /**
     * Class constructor.
     */
    public PlayerLabel() {
    }

    /**
     * Player getter.
     *
     * @return player associated to the label.
     */
    public Player getPlayer() {
        if (this.player != null) {
            return this.player;
        } else {
            return null;
        }
    }

    /**
     * Player setter.
     * Set a player associated to the label.
     * @param p Player
     */
    public void setPlayer(Player p) {
        this.player = p;
        if (this.player == null) {
            this.setText("");
            this.setIcon(null);
        } else {
            if (p.getReady()) {
                this.setIcon(this.resize("./res/img/Ready.png",
                        this.getSize().height, this.getSize().height));
            } else {
                this.setIcon(this.resize("./res/img/NotReady.png",
                        this.getSize().height, this.getSize().height));
            }
            this.setText(p.getName());
        }
    }

    /**
     * Update player ready statement.
     *
     * @param b : new ready statement.
     */
    public void setReady(boolean b) {
        if (b) {
            this.setIcon(this.resize("./res/img/Ready.png",
                    this.getSize().height, this.getSize().height));
        } else {
            this.setIcon(this.resize("./res/img/NotReady.png",
                    this.getSize().height, this.getSize().height));
        }
    }
}
