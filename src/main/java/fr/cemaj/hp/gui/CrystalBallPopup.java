package fr.cemaj.hp.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import fr.cemaj.hp.game.card.Card;

/**
 * This class represents the popup that appear after a validation of\
 * a crystall ball card (for the player who played it).
 */
public class CrystalBallPopup extends JDialog implements ActionListener {

    private static final long serialVersionUID = 1L;

    private MainFrame mainFrame;
    private JLabel info;
    private CardsVisualizer cardsLabels;
    private JButton validateBut;
    private int maxWidth;

    /**
     * Class constructor.
     *
     * @param mf : a reference to the main frame.
     * @param width :the max width of the main frame.
     */
    CrystalBallPopup(MainFrame mf, int width) {
        super();

        this.mainFrame = mf;
        this.maxWidth = width;
        if (this.mainFrame.isMac()) {
            this.setSize(350, 200);
        } else {
            this.setSize(350, 220);
        }
        this.setTitle("Boule de cristal");
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setAlwaysOnTop(true);
        this.setModal(true);

        // Initialisation popup elements
        this.info = new JLabel("Réorganisez les cartes de la pioche. "
                + "Les cartes les plus à gauche seront sur le dessus "
                + "de la pile.");
        this.cardsLabels = new CardsVisualizer(width);
        this.validateBut = new JButton("Ok");
        this.validateBut.addActionListener(this);

        // Set layout
        //this.setLayout(null);
        this.setLayout(new BorderLayout());
        this.setLocationRelativeTo(null);

        // Add elements
        this.add(this.info, BorderLayout.NORTH);
        this.add(this.cardsLabels, BorderLayout.CENTER);
        this.add(this.validateBut, BorderLayout.SOUTH);

        // // Placement des éléments
        // int topSpace = 10, height = 20, width = 300;
        // this.info.setBounds(10, topSpace, width, height);
        // this.validateBut.setBounds(180, height * 6 + 10 + topSpace, 80,
        // height + 10);
    }

    /**
     * Method called by Client class to display the popup.
     *
     * @param tabCards : the cards to be reorganized.
     */
    public void reorganizeCards(Card[] tabCards) {
        this.cardsLabels = new CardsVisualizer(this.maxWidth);
        this.add(this.cardsLabels, BorderLayout.CENTER);
        int cardLabelWidth
                = (int) ((double) this.maxWidth / (tabCards.length + 2));
        int cardLabelHeight = (int) ((double) cardLabelWidth * 1.4);
        this.setSize(tabCards.length * cardLabelWidth,
                cardLabelHeight + 80);
        this.cardsLabels.reorganizeCards(tabCards,
                cardLabelWidth,
                cardLabelHeight);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.validateBut) {
            this.dispose();
            ArrayList<Card> cards = new ArrayList<Card>();
            for(int i = 0; i < this.cardsLabels.getCards().length; i++) {
                cards.add(this.cardsLabels.getCards()[i].getCard());
            }
            this.mainFrame.client.sendFourCardStack(cards);
        }
    }
}
