package fr.cemaj.hp.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

/**
 * This class represents the credits panel.
 *
 * @author Jérémy Longin
 */
public class CreditPanel extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    JLabel creditTitle;
    JLabel softwareVersion;
    JLabel title1;
    JLabel content1;
    JLabel title2;
    JLabel title3;
    JLabel content3;
    JLabel title4;
    JLabel content4;
    JLabel title5;
    JLabel content5;
    JLabel title6;
    JLabel content6;
    JLabel title7;
    JLabel content7;
    JTextArea information;
    PictureLabel content2;
    JButton returnBut;

    /**
     * Class constructor.
     *
     * @param width : the width of the main frame
     * @param height : the height of the main frame
     * @param listener : a reference to all listeners
     * @param picture : the picture displayed in background
     * @throws FontFormatException : exception
     * @throws IOException : exception
     */
    public CreditPanel(int width, int height, InterfaceListeners listener,
            Image picture) throws FontFormatException, IOException {
        super(picture);

        this.addMouseListener(listener.mouseListener);
        this.addMouseMotionListener(listener.mouseListener);


        // Informations générales et bouton
        this.creditTitle = new JLabel("A propos");
        this.information = new JTextArea(
                "CEMAJ est une entreprise fictive créée dans le cadre d’un "
                + "projet scolaire de génie logiciel, pour CPE Lyon, dont le "
                + "but était de réaliser une version informatique du jeu "
                + "Hocus Pocus.");
        this.softwareVersion = new JLabel("V1.0");
        this.returnBut = new JButton("Retour");

        // Cadres colorés
        this.title1 = new JLabel("Auteur du jeu original");
        this.content1 = new JLabel("Elliot Hogg");
        this.title2 = new JLabel("Developpement logiciel");
        this.content2 = new PictureLabel();
        this.title3 = new JLabel("Chef de projet");
        this.content3 = new JLabel("Clemence LOP");
        this.title4 = new JLabel("Responsable qualite");
        this.content4 = new JLabel("Adrien FAURE");
        this.title5 = new JLabel("Support IT");
        this.content5 = new JLabel("Eric GILLET");
        this.title6 = new JLabel("Responsable developpement");
        this.content6 = new JLabel("Mathieu DEGAINE");
        this.title7 = new JLabel("Ergonome");
        this.content7 = new JLabel("Jeremy LONGIN");

        // Parametrage fenêtre
        this.setLayout(null);

        // Ajout des composants au panneau
        int elementWidth;
        int elementHeight;
        int leftElementPositionX;
        int rightElementPositionX;
        elementWidth = (int) ((double) width * 0.3);
        elementHeight = (int) ((double) height * 0.1);
        leftElementPositionX = (int) ((double) width * 0.5) - elementWidth
                - (int) ((double) width * 0.01);
        rightElementPositionX = (int) ((double) width * 0.5)
                + (int) ((double) width * 0.01);

        this.add(this.creditTitle);
        this.creditTitle.setBounds((int) ((double) width * 0.5)
                - (elementWidth / 2), (int) ((double) height * 0.03),
                elementWidth, (int) ((double) height * 0.08));
        this.add(this.softwareVersion);
        this.softwareVersion.setBounds((int) ((double) width * 0.88),
                (int) ((double) height * 0.02), elementWidth,
                (int) ((double) width * 0.1));
        this.add(this.title1);
        this.title1.setBounds(leftElementPositionX,
                (int) ((double) height * 0.12), elementWidth, elementHeight);
        this.add(this.content1);
        this.content1.setBounds(rightElementPositionX,
                (int) ((double) height * 0.12), elementWidth, elementHeight);
        this.add(this.title2);
        this.title2.setBounds(leftElementPositionX,
                (int) ((double) height * 0.23), elementWidth, elementHeight);
        this.add(this.content2);
        this.content2.setBounds(rightElementPositionX,
                (int) ((double) height * 0.23), elementWidth, elementHeight);
        this.content2.setIcon(this.content2.resize("./res/img/CemajLogoS.png",
                (int) ((double) this.content2.getHeight() * 1.935),
                this.content2.getHeight()));
        this.content2.setBackground(Color.WHITE);
        this.add(this.title3);
        this.title3.setBounds(leftElementPositionX,
                (int) ((double) height * 0.34), elementWidth, elementHeight);
        this.add(this.content3);
        this.content3.setBounds(rightElementPositionX,
                (int) ((double) height * 0.34), elementWidth, elementHeight);
        this.add(this.title4);
        this.title4.setBounds(leftElementPositionX,
                (int) ((double) height * 0.45), elementWidth, elementHeight);
        this.add(this.content4);
        this.content4.setBounds(rightElementPositionX,
                (int) ((double) height * 0.45), elementWidth, elementHeight);
        this.add(this.title5);
        this.title5.setBounds(leftElementPositionX,
                (int) ((double) height * 0.56), elementWidth, elementHeight);
        this.add(this.content5);
        this.content5.setBounds(rightElementPositionX,
                (int) ((double) height * 0.56), elementWidth, elementHeight);
        this.add(this.title6);
        this.title6.setBounds(leftElementPositionX,
                (int) ((double) height * 0.67), elementWidth, elementHeight);
        this.add(this.content6);
        this.content6.setBounds(rightElementPositionX,
                (int) ((double) height * 0.67), elementWidth, elementHeight);
        this.add(this.title7);
        this.title7.setBounds(leftElementPositionX,
                (int) ((double) height * 0.78), elementWidth, elementHeight);
        this.add(this.content7);
        this.content7.setBounds(rightElementPositionX,
                (int) ((double) height * 0.78), elementWidth, elementHeight);
        this.add(this.information);
        this.information.setBounds((width / 2) - elementWidth,
                height - elementHeight, elementWidth * 2, elementHeight);
        this.information.setEditable(true);
        this.information.setLineWrap(true);
        this.information.setWrapStyleWord(true);
        this.information.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.information.setAlignmentY(Component.CENTER_ALIGNMENT);
        this.information.setForeground(Color.white);
        this.information.setFont(new Font(null, Font.ITALIC,
                (int) ((double) width * 0.010)));
        //this.setBackground(Color.white);
        this.information.setOpaque(false);

        this.add(this.returnBut);
        this.returnBut.setBounds((int) ((double) width * 0.85),
                (int) ((double) height * 0.8), (int) ((double) width * 0.12),
                elementHeight);
        this.returnBut.addActionListener(listener.actionListener);

        // Mise en forme des infos générales
        this.creditTitle.setFont(new Font("Grinched", Font.ITALIC,
                (int) ((double) width * 0.03)));
        this.creditTitle.setForeground(Color.white);
        this.softwareVersion.setFont(new Font("Grinched", Font.ITALIC,
                (int) ((double) width * 0.03)));
        this.softwareVersion.setForeground(Color.white);
        this.creditTitle.setVerticalAlignment(SwingConstants.CENTER);
        this.creditTitle.setHorizontalAlignment(SwingConstants.CENTER);

        // Mise en forme des cadres colorés
        paramElement(this.title1, 0, width);
        paramElement(this.content1, 0, width);
        paramElement(this.title2, 1, width);
        paramElement(this.content2, 1, width);
        paramElement(this.title3, 2, width);
        paramElement(this.content3, 2, width);
        paramElement(this.title4, 3, width);
        paramElement(this.content4, 3, width);
        paramElement(this.title5, 4, width);
        paramElement(this.content5, 4, width);
        paramElement(this.title6, 5, width);
        paramElement(this.content6, 5, width);
        paramElement(this.title7, 6, width);
        paramElement(this.content7, 6, width);

        // Paramétrage bouton
        paramElement(this.returnBut, 6, width);

        this.setOpaque(false);
    }
}
