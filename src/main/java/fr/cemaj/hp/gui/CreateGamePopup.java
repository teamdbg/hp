package fr.cemaj.hp.gui;

import java.awt.Color;
import java.util.regex.Pattern;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 * This class represents the game creation popup.
 *
 * @author Jérémy Longin
 */
public class CreateGamePopup extends JDialog {

    private static final long serialVersionUID = 1L;

    JLabel nameLabel;
    JLabel gameModeLabel;
    JLabel erreurNameLabel;

    JTextField nameField;

    JRadioButton quickGameMode;
    JRadioButton classicGameMode;
    ButtonGroup radioGroup;

    JButton validateBut;
    JButton cancelBut;

    MainFrame mainFrame;

    /**
     * Class constructor.
     *
     * @param mf : a reference to the mainFrame object
     * @param listener : a reference to the listeners
     */
    CreateGamePopup(MainFrame mf, InterfaceListeners listener) {
        super();

        this.mainFrame = mf;
        if(this.mainFrame.isMac()) {
            this.setSize(350, 200);
        } else {
            this.setSize(350, 220);
        }
        this.setTitle("Créer une partie");
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setAlwaysOnTop(true);
        this.setModal(true);

        // Initialisation popup elements
        this.nameLabel = new JLabel("Entrer un pseudonyme");
        this.nameField = new JTextField();
        this.erreurNameLabel = new JLabel("Pseudo invalide");
        this.erreurNameLabel.setVisible(false);
        this.gameModeLabel = new JLabel("Choisir un mode de jeu");
        this.quickGameMode = new JRadioButton("Partie rapide");
        this.classicGameMode = new JRadioButton("Partie classique");
        this.radioGroup = new ButtonGroup();
        this.radioGroup.add(this.classicGameMode);
        this.radioGroup.add(this.quickGameMode);
        this.classicGameMode.setSelected(true);
        this.validateBut = new JButton("Ok");
        this.validateBut.addActionListener(listener.actionListener);
        this.cancelBut = new JButton("Annuler");
        this.cancelBut.addActionListener(listener.actionListener);

        // Set layout
        this.setLayout(null);
        this.setLocationRelativeTo(null);

        // Add elements
        this.add(this.nameLabel);
        this.add(this.nameField);
        this.add(this.erreurNameLabel);
        this.add(this.gameModeLabel);
        this.add(this.classicGameMode);
        this.add(this.quickGameMode);
        this.add(this.validateBut);
        this.add(this.cancelBut);

        // Placement des éléments
        int topSpace = 10;
        int height = 20;
        int width = 300;
        this.nameLabel.setBounds(10, topSpace, width, height);
        this.nameField.setBounds(20, height + topSpace, width, height);
        this.erreurNameLabel
                .setBounds(10, height * 2 + topSpace, width, height);
        this.gameModeLabel.setBounds(10, height * 3 + 5 + topSpace, width,
                height);
        this.classicGameMode.setBounds(20, height * 4 + 5 + topSpace, width,
                height);
        this.quickGameMode.setBounds(20, height * 5 + 5 + topSpace, width,
                height);
        this.validateBut.setBounds(180, height * 6 + 10 + topSpace, 80,
                height + 10);
        this.cancelBut.setBounds(260, height * 6 + 10 + topSpace, 80,
                height + 10);

        //this.setVisible(true);
        this.addKeyListener(listener.keyListener);
        this.nameField.addKeyListener(listener.keyListener);
        this.quickGameMode.addKeyListener(listener.keyListener);
        this.classicGameMode.addKeyListener(listener.keyListener);
    }

    /**
     * This method validate the form, it checks that the user
     * correctly enter elements.
     */
    public void validation() {
        Pattern r = Pattern.compile("^[a-zA-Z0-9]{3,16}$");
        if (!(r.matcher(this.nameField.getText()).find())) {
            this.erreurNameLabel.setForeground(Color.red);
            this.erreurNameLabel.setVisible(true);
        } else {
            this.dispose();
            //JOptionPane.showMessageDialog(this.mainFrame,
            // "Lancement du serveur...");
            this.mainFrame.client
                .setAndLaunchServer(this.quickGameMode.isSelected());
            this.mainFrame.client
                .connect(this.nameField.getText(), "127.0.0.1");
            this.mainFrame.changePanel(this.mainFrame.getLobbyPanel());
        }
    }
}
