/**
 * package-info: fr.cemaj.hp.ihm
 *
 * Version 0.1
 *
 * Date 11.26.2013
 *
 * Copyright CEMAJ
 *
 * Developers: C.L.
 */

/**
 * This package contents the GUI classes.
 */
package fr.cemaj.hp.gui;
