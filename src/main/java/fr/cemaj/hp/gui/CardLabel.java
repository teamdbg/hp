package fr.cemaj.hp.gui;

import java.awt.Color;

import javax.swing.border.LineBorder;

import fr.cemaj.hp.game.card.Card;

/**
 * This class represents a card on the GUI.
 *
 * @author Jérémy Longin
 */
public class CardLabel extends PictureLabel {

    private static final long serialVersionUID = 1L;

    /**
     * The card to display.
     */
    private Card card;
    private boolean targeted;

    /**
     * CardLabel constructor.
     */
    CardLabel() {
        super();

        this.card = null;
    }

    /**
     * CardLabel constructor.
     *
     * @param s : Text to write in the label
     */
    CardLabel(String s) {
        super(s);

        this.card = null;
    }

    /**
     * CardLabel constructor.
     *
     * @param c : the card to display
     * @param width : the width that the component have to take
     * @param height : the height that the component have to take
     */
    CardLabel(Card c, int width, int height) {
        super();
        this.setSize(width, height);
        setCard(c);
    }

    /**
     * CardLabel constructor.
     *
     * @param c : the card to display
     */
    CardLabel(CardLabel c) {
        this(c.getCard(), c.getSize().width, c.getSize().height);
    }

    /**
     * Function to set the card to display. It built the path for card picture\
     * file, display this picture and set the label enable.
     *
     * @param c : the card to display
     */
    public void setCard(Card c) {
        this.card = c;

        if (this.card != null) {
            String type = this.card.getClass().getSimpleName();
            if (this.card.getPower() == 0) {
                this.pictureFilePath = "./res/img/cards/" + type + ".png";
            } else {
                this.pictureFilePath = "./res/img/cards/" + type
                        + this.card.getPower() + ".png";
            }
            this.setEnabled(true);
        } else {
            this.pictureFilePath = "./res/img/cards/Back.png";
            this.setVisible(true);
            this.setEnabled(false);
        }
        // System.out.println("CardLabel > affichage d'une carte : "+
        // c.getClass().getSimpleName());
        //System.out.println("CardLabel > "+pictureFilePath);
        this.setIcon(this.resize(this.pictureFilePath, this.getWidth(),
                this.getHeight()));
    }

    /**
     * Function to get the card contained by the PictureLabel.
     *
     * @return the card which is displayed
     */
    public Card getCard() {
        return this.card;
    }

    /**
     * Function to switch the label card by another. It replace the local card\
     * by the parameter.
     *
     * @param c : replacement card
     * @return the card replaced
     */
    public Card switchCards(Card c) {
        Card cTemp = this.card;
        this.setCard(c);
        this.setVisible(true);
        return cTemp;
    }

    /**
     * Method use to set target visibility.
     *
     * @param b : new visibily state
     */
    public void setTargeted(boolean b) {
        this.targeted = b;
        if (b) {
            this.setBorder(new LineBorder(Color.red, 3));
        } else {
            this.setBorder(null);
        }
    }

    /**
     * isTargeted.
     * @return true -> targeted | false -> not targeted
     */
    public boolean isTargeted() {
        return this.targeted;
    }
}
