package fr.cemaj.hp.gui;

import java.awt.Color;
import java.awt.FontFormatException;
import java.io.IOException;

/**
 * This class represents the splash screen display on launching the application.
 * 
 * @author Jérémy Longin
 */
public class SlashPanel extends GraphicPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    PictureLabel cemajLogo, hocusLogo;
    MainFrame mainFrame;

    /**
     * Class constructor
     * 
     * @param mf : a reference to the main frame
     * @param width : the width of the main frame
     * @param height : the height of the main frame
     */
    SlashPanel(MainFrame mf, int width, int height) throws FontFormatException, IOException{
        super();
        
        this.mainFrame = mf;
        this.cemajLogo = new PictureLabel();
        this.hocusLogo = new PictureLabel();

        int labelWidth = (int)((double)width*0.323);
        int labelHeight = (int)((double)height*0.405);
        
        this.setSize(width, height);
        this.add(this.cemajLogo);
        this.add(this.hocusLogo);
        this.setBackground(Color.WHITE);
        this.setLayout(null);
        
        this.cemajLogo.setBounds((width/2)-(labelWidth/2), (height/2)-(labelHeight/2), labelWidth, labelHeight);
        this.cemajLogo.setIcon(this.cemajLogo.resize("./res/img/CemajLogo.png", labelWidth, labelHeight));
    }
    
    void display(){
        this.cemajLogo.setVisible(true);
        try {
            this.mainFrame.setVisible(true);
            Thread.sleep(2500);
        } catch (Exception e) {
            System.out.println("Error");
        }
    }
}
