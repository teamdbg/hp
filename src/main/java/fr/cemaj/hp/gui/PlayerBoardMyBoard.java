package fr.cemaj.hp.gui;

import java.awt.FontFormatException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import fr.cemaj.hp.game.Player;
import fr.cemaj.hp.game.card.Card;

/**
 * This class represents current player board.
 * 
 * @authors Jérémy Longin
 */
public class PlayerBoardMyBoard extends PlayerBoard {

    private static final long serialVersionUID = 1L;

    private ArrayList<CardLabel> handCards;
    private PictureLabel gemsIcon;
    private JLabel gemsNumber, playerName;
    private int yTopPos, yBotPos, cardLabelWidth, cardLabelHeight, width,
            handCardNumber;
    private int[] xPos;
    private InterfaceListeners listeners;

    /**
     * Class constructor
     * 
     * @param width
     *            : main frame width
     * @param height
     *            : main frame height
     * @param boardWidth
     *            : the width of the board
     * @param boardHeight
     *            : the height of the board
     * @param listener
     *            : all GUI listeners
     * @param position
     *            : N (north) or S (south) to put cards up or down hand and gem
     *            icons.
     */
    public PlayerBoardMyBoard(int width, int height, int boardWidth,
            InterfaceListeners listeners) throws FontFormatException,
            IOException {
        super();

        this.listeners = listeners;
        this.addMouseListener(this.listeners.mouseListener);
        this.addMouseMotionListener(this.listeners.mouseListener);

        // this.setOpaque(false);
        this.cardLabelWidth = (int) ((double) width * 0.084);
        this.cardLabelHeight = (int) ((double) height * 0.209);
        this.xPos = new int[5];
        xPos[0] = (int) (((double) boardWidth / 2) - ((cardLabelWidth * 2.5) + 6));
        xPos[1] = (int) (((double) boardWidth / 2) - ((cardLabelWidth * 1.5) + 3));
        xPos[2] = (int) (((double) boardWidth / 2) - (cardLabelWidth / 2));
        xPos[3] = (int) (((double) boardWidth / 2) + ((cardLabelWidth * 0.5) + 3));
        xPos[4] = (int) (((double) boardWidth / 2) + ((cardLabelWidth * 1.5) + 6));
        yTopPos = 0;
        yBotPos = (int) ((double) height * 0.209) + 3;

        this.setLayout(null);

        this.grim1 = new CardLabel();
        this.grim2 = new CardLabel();
        this.grim3 = new CardLabel();

        // Init handCards list
        this.handCards = new ArrayList<CardLabel>();
        for (int i = 0; i < 5; i++) {
            this.handCards.add(i, new CardLabel());
            this.add(this.handCards.get(i));
            this.handCards.get(i).setBounds(xPos[i], yBotPos, cardLabelWidth,
                    cardLabelHeight);
            this.paramElement(this.handCards.get(i), 7, width);
            this.handCards.get(i)
                    .addMouseListener(this.listeners.mouseListener);
        }

        // Init grimoire
        this.add(this.grim1);
        this.add(this.grim2);
        this.add(this.grim3);
        this.grim1.setBounds(xPos[1], yTopPos, cardLabelWidth, cardLabelHeight);
        this.grim2.setBounds(xPos[2], yTopPos, cardLabelWidth, cardLabelHeight);
        this.grim3.setBounds(xPos[3], yTopPos, cardLabelWidth, cardLabelHeight);
        this.paramElement(this.grim1, 7, width);
        this.paramElement(this.grim2, 7, width);
        this.paramElement(this.grim3, 7, width);
        this.grim1.addMouseListener(this.listeners.mouseListener);
        this.grim2.addMouseListener(this.listeners.mouseListener);
        this.grim3.addMouseListener(this.listeners.mouseListener);

        // Positionnement du nombre de gemmes
        this.gemsIcon = new PictureLabel();
        this.gemsNumber = new JLabel("x30");
        this.add(this.gemsIcon);
        this.add(this.gemsNumber);
        this.gemsIcon.setBounds(xPos[4], (int) (yBotPos / 1.5),
                getIconWidth(width),
                (int) ((double) getIconHeight(height) * 0.8));
        this.gemsNumber.setBounds((int) ((double) xPos[4] * 1.12),
                (int) (yBotPos / 1.5), getIconWidth(width),
                getIconHeight(height));
        this.gemsIcon.setIcon(this.gemsIcon.resize("./res/img/GemIcon.png",
                this.gemsIcon.getWidth(), this.gemsIcon.getHeight()));
        this.lightParamElement(this.gemsNumber, 1, width);

        // Positionnement du nom du joueur
        this.playerName = new JLabel("Jaylbralon");
        this.add(this.playerName);
        this.playerName.setBounds((int) ((double) xPos[4] * 1.02), yBotPos / 2,
                100, 20);

        this.setVisible(true);
    }

    /**
     * Function to initialize the board.
     * 
     * @param p
     *            : the player to associate.
     */
    public void initPlayer(Player p) {
        this.setPlayer(p);

        // Initialisation valeurs numériques
        this.gemsNumber.setText("x" + this.player.getGems());
        this.playerName.setText(this.player.getName());
        for (int i = 0; i < p.getHandCardsList().size(); i++) {
            if (i <= this.handCards.size())
                this.handCards.get(i).setCard(p.getHandCardsList().get(i));
        }
    }

    public void updatePlayer(Player p) {
        //System.out.println("PlayerBoardMyBoard > UpdatePlayer()");
        int i;
        // Update player
        this.player = p;
        // Delete des cartes
        //System.out.println("PlayerBoardMyBoard > Update grimoire cards");
        this.grim1.setCard(null);
        this.grim2.setCard(null);
        this.grim3.setCard(null);
        // Update grimoire cards
        if (this.player.getGrimoireCardsNumber() >= 1) {
            this.grim1.setCard(this.player.getGrimoireCardsList().get(0));
            if (this.player.getGrimoireCardsNumber() >= 2) {
                this.grim2.setCard(this.player.getGrimoireCardsList().get(1));
                if (this.player.getGrimoireCardsNumber() == 3) {
                    this.grim3.setCard(this.player.getGrimoireCardsList()
                            .get(2));
                }
            }
        }
        // Update gem number
        this.gemsNumber.setText("x" + p.getGems());
        // Update hand cards
        // System.out.println("PlayerBoardMyBoard > Update hand cards");
        // Remove all handCards from GUI
        for (i = 0; i < this.handCards.size(); i++) {
            this.remove(this.handCards.get(i));
        }
        this.handCards.clear();
        this.handCardNumber = 0;
        for (i = 0; i < p.getHandCardsList().size() || i < 5; i++) {
            if (i >= p.getHandCardsList().size())
                addHandCard(null);
            else
                addHandCard(p.getHandCardsList().get(i));
        }
    }

    public void addHandCard(Card c) { // TODO
        int i;
        // Remove elements handCards from GUI
        for (i = 0; i < this.handCards.size(); i++) {
            this.remove(this.handCards.get(i));
        }
        // Add new card to list
        int index = this.handCards.size();
        if (index == this.handCardNumber) {
            this.handCards.add(index, new CardLabel());
        } else {
            for (i = 0; i < this.handCards.size(); i++) {
                if (this.handCards.get(i).getCard() == null) {
                    this.handCards.get(i).setCard(c);
                    break;
                }
            }
        }
        // Update cardLabels
        for (i = 0; i < this.handCards.size(); i++) {
                this.add(this.handCards.get(i));
            if (this.handCards.size() <= 5) {
                this.handCards.get(i).setBounds(xPos[i], yBotPos,
                        cardLabelWidth, cardLabelHeight);
            } else {
                if (i == 0) {
                    this.handCards.get(i).setBounds(xPos[i], yBotPos,
                            cardLabelWidth, cardLabelHeight);
                } else {
                    this.handCards
                            .get(i)
                            .setBounds(
                                    xPos[0]
                                            + (i * ((this.getSize().width - cardLabelWidth) / this.handCards
                                                    .size())),
                                    yBotPos, cardLabelWidth, cardLabelHeight);
                }
            }
            this.paramElement(this.handCards.get(i), 7, width);
            this.handCards.get(i)
                    .addMouseListener(this.listeners.mouseListener);
        }
        // Add card to new label
        this.handCards.get(index).setCard(c);
        this.handCardNumber++;
        this.repaint();
    }

    public void removeHandCard(CardLabel c) {
        int i;
        // remove card from list
        if (this.handCards.size() > 5) {
            this.handCards.remove(c);
        } else {
            this.handCards.remove(c);
            c.setCard(null);
            this.handCards.add(c);
            c.setCard(null);
        }
        // replace other cards on GUI
        for (i = 0; i < this.handCards.size(); i++) {
            this.add(this.handCards.get(i));
            if (this.handCards.size() <= 5) {
                this.handCards.get(i).setBounds(xPos[i], yBotPos,
                        cardLabelWidth, cardLabelHeight);
            } else {
                if (i == 0) {
                    this.handCards.get(i).setBounds(xPos[i], yBotPos,
                            cardLabelWidth, cardLabelHeight);
                } else {
                    this.handCards
                            .get(i)
                            .setBounds(
                                    xPos[0]
                                            + (i * ((this.getSize().width - cardLabelWidth) / this.handCards
                                                    .size())),
                                    yBotPos, cardLabelWidth, cardLabelHeight);
                }
            }
            this.paramElement(this.handCards.get(i), 7, width);
            this.handCards.get(i)
                    .addMouseListener(this.listeners.mouseListener);
        }
        this.handCardNumber--;
        this.repaint();
    }

    public ArrayList<CardLabel> getHandCards() {
        return this.handCards;
    }

    public boolean isCardInHand() {
        for (int i = 0; i < this.handCards.size(); i++) {
            if(this.handCards.get(i) != null && this.handCards.get(i).getCard() != null)
                return true;
        }
        return false;
    }
}
