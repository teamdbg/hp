package fr.cemaj.hp.gui;

import java.awt.FontFormatException;
import java.io.IOException;

import javax.swing.ImageIcon;

import fr.cemaj.hp.game.Player;

/**
 * This class represents a player in results panel.
 * 
 * @authors Jérémy Longin
 */
public class PlayerResultPanel extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    private PictureLabel nameLabel, gemLabel;

    /**
     * Class constructor
     * 
     * @param width
     *            : the width of the main frame
     * @param height
     *            : the height of the main frame
     * @param listener
     *            : a reference to all listeners
     * @param picture
     *            : the picture displayed in background
     */
    public PlayerResultPanel(int width, int panelWidth, int panelHeight, int pos)
            throws FontFormatException, IOException {
        super(new ImageIcon("./res/img/WhiteBackground.jpg").getImage());
        this.setLayout(null);

        nameLabel = new PictureLabel();
        gemLabel = new PictureLabel();

        this.add(nameLabel);
        this.add(gemLabel);

        this.nameLabel.setBounds(0, 0, (int) ((double) panelWidth * 0.6),
                panelHeight);
        this.gemLabel.setBounds((int) ((double) panelWidth * 0.6), 0,
                (int) ((double) panelWidth * 0.4), panelHeight);

        switch (pos){
        case 0 :
            this.nameLabel.setIcon(this.nameLabel.resize("./res/img/GoldMedal.png",
                    (int) ((double) this.nameLabel.getSize().height * 0.8),
                    (int) ((double) this.nameLabel.getSize().height * 0.8)));
            break;
        case 1 :
            this.nameLabel.setIcon(this.nameLabel.resize("./res/img/SilverMedal.png",
                    (int) ((double) this.nameLabel.getSize().height * 0.8),
                    (int) ((double) this.nameLabel.getSize().height * 0.8)));
            break;
        case 2 :
            this.nameLabel.setIcon(this.nameLabel.resize("./res/img/BronzeMedal.png",
                    (int) ((double) this.nameLabel.getSize().height * 0.8),
                    (int) ((double) this.nameLabel.getSize().height * 0.8)));
            break;
        default :
            this.nameLabel.setIcon(this.nameLabel.resize("./res/img/NotReady.png",
                    (int) ((double) this.nameLabel.getSize().height * 0.8),
                    (int) ((double) this.nameLabel.getSize().height * 0.8)));
            break;
        }
        this.gemLabel.setIcon(this.gemLabel.resize("./res/img/gemIcon.png",
                (int) ((double) this.nameLabel.getSize().height * 0.8),
                (int) ((double) this.nameLabel.getSize().height * 0.6)));
        this.lightParamElement(nameLabel, 6, width);
        this.lightParamElement(gemLabel, 6, width);

        this.setVisible(true);
    }

    /**
     * Set the player associated to the label.
     * 
     * @param p : the player to associates.
     */
    public void setPlayer(Player p) {
        this.nameLabel.setText(p.getName());
        this.gemLabel.setText("x" + p.getGems());
    }
}
