package fr.cemaj.hp.gui;

import java.awt.Dimension;
import java.awt.FontFormatException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.border.LineBorder;

import fr.cemaj.hp.game.card.Card;

/**
 * This class represents the game zone.
 * It contains a Hocus card and a list of Pocus cards.
 *
 * @author Jérémy Longin
 */
public class GameZone extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    private CardLabel hocusCard;
    private ArrayList<CardLabel> pocusCards;
    private int cardLabelWidth;
    private int cardLabelHeight;
    private int pocusCardsIndex;

    /**
     * Class constructor.
     *
     * @param width : the width of the main frame.
     * @param height : the height of the main frame.
     * @param listener : all GUI listeners
     * @throws FontFormatException : exception
     * @throws IOException : exception
     */
    GameZone(int width, int height, InterfaceListeners listener)
            throws FontFormatException, IOException {
        super();

        this.addMouseListener(listener.mouseListener);
        this.addMouseMotionListener(listener.mouseListener);

        // Initialisation list
        this.pocusCards = new ArrayList<CardLabel>();

        // this.setOpaque(false);
        this.cardLabelWidth = (int) ((double) width * 0.084);
        this.cardLabelHeight = (int) ((double) height * 0.209);
        this.setLayout(null);

        this.hocusCard = new CardLabel();
        this.add(this.hocusCard);
        this.hocusCard.setBounds((int) ((double) height * 0.015),
                (int) ((double) height * 0.015), this.cardLabelWidth,
                this.cardLabelHeight);

        this.hocusCard.addMouseListener(listener.mouseListener);

        this.pocusCardsIndex = 0;

        int borderSize = (int) ((double) width * 0.002);
        this.setBorder(new LineBorder(this.colorGrey, borderSize));

        this.setVisible(true);
    }

    /**
     * Method to add a Pocus card to the list of Pocus cards.
     *
     * @param c : the card to add.
     * @param width : the width of the main frame.
     */
    public void addPocusCard(Card c, int width) {
        this.pocusCards.add(this.pocusCardsIndex, new CardLabel());
        this.add(this.pocusCards.get(this.pocusCardsIndex));
        this.pocusCards
                .get(this.pocusCardsIndex)
                .setBounds(
                        (int) (((double) this.cardLabelWidth * 1.15)
                        + (this.pocusCardsIndex * (this.cardLabelWidth * 0.5))),
                        this.hocusCard.getX(), this.cardLabelWidth,
                        this.cardLabelHeight);
        this.pocusCards.get(this.pocusCardsIndex).setCard(c);
        this.pocusCards.get(this.pocusCardsIndex).addMouseListener(
                this.getMouseListeners()[0]);
        this.pocusCardsIndex = this.pocusCardsIndex + 1;
    }

    /**
     * Method to add a Hocus card to the game zone.
     *
     * @param c : the card to add.
     * @param width : the width of the main frame.
     */
    public void addHocusCard(Card c, int width) {
        this.hocusCard.setCard(c);
        this.hocusCard.setVisible(true);
        this.paramElement(this.hocusCard, 7, width);
    }

    /**
     * getCardSize.
     * @return a Dimension
     */
    public Dimension getCardSize() {
        return new Dimension(this.cardLabelWidth, this.cardLabelHeight);
    }

    /**
     * getHocusCard.
     * @return a CardLabel
     */
    public CardLabel getHocusCard() {
        return this.hocusCard;
    }

    /**
     * getPocusCards.
     * @return an ArrayList<CardLabel>
     */
    public ArrayList<CardLabel> getPocusCards() {
        return this.pocusCards;
    }

    /**
     * removeCards.
     */
    public void removeCards() {
        this.hocusCard.setCard(null);
        this.hocusCard.setVisible(false);
        for(int i = 0; i < this.pocusCards.size(); i++) {
            this.pocusCards.get(i).setVisible(false);
        }
        this.pocusCards.clear();
        this.pocusCardsIndex = 0;
    }
}
