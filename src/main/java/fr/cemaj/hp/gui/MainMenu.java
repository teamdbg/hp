package fr.cemaj.hp.gui;

import java.awt.FontFormatException;
import java.awt.Image;
import java.io.IOException;

import javax.swing.JButton;

/**
 * This class represents the main menu panel in GUI.
 *
 * @author Jérémy Longin
 */
public class MainMenu extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    JButton createBut;
    JButton joinBut;
    JButton rulesBut;
    JButton creditsBut;
    JButton exitBut;
    PictureLabel gameBox;

    /**
     * Class constructor.
     * @param width a
     * @param height b
     * @param listener c
     * @param picture d
     * @throws FontFormatException e
     * @throws IOException f
     */
    MainMenu(int width, int height, InterfaceListeners listener, Image picture)
            throws FontFormatException, IOException {
        super(picture);

        addMouseListener(listener.mouseListener);
        addMouseMotionListener(listener.mouseListener);

        // Création des boutons
        this.createBut = new JButton("Creer une partie");
        this.joinBut = new JButton("Rejoindre une partie");
        this.rulesBut = new JButton("Regles du jeu");
        this.creditsBut = new JButton("A propos");
        this.exitBut = new JButton("Quitter");
        this.createBut.addActionListener(listener.actionListener);
        this.joinBut.addActionListener(listener.actionListener);
        this.rulesBut.addActionListener(listener.actionListener);
        this.creditsBut.addActionListener(listener.actionListener);
        this.exitBut.addActionListener(listener.actionListener);

        // Mise en place des boutons
        int buttonWidth;
        int buttonHeight;
        buttonWidth = (int) ((double) width * 0.3);
        buttonHeight = (int) ((double) height * 0.1);
        this.setLayout(null);
        this.add(this.createBut);
        this.add(this.joinBut);
        this.add(this.rulesBut);
        this.add(this.creditsBut);
        this.add(this.exitBut);
        this.createBut.setBounds((int) ((double) width * 0.5),
                (int) ((double) height * 0.15), buttonWidth, buttonHeight);
        this.joinBut.setBounds((int) ((double) width * 0.5),
                (int) ((double) height * 0.28), buttonWidth, buttonHeight);
        this.rulesBut.setBounds((int) ((double) width * 0.5),
                (int) ((double) height * 0.41), buttonWidth, buttonHeight);
        this.creditsBut.setBounds((int) ((double) width * 0.5),
                (int) ((double) height * 0.54), buttonWidth, buttonHeight);
        this.exitBut.setBounds((int) ((double) width * 0.5),
                (int) ((double) height * 0.67), buttonWidth, buttonHeight);

        // Création de l'image de la boite
        this.gameBox = new PictureLabel();
        this.add(this.gameBox);
        String filePath = "./res/img/GameBox.png";
        this.gameBox.setIcon(this.gameBox.resize(filePath,
                (int) ((double) width * 0.3), height));
        this.gameBox.setBounds((int) ((double) width * 0.15), 0,
                (int) ((double) width * 0.3), height);

        // Ergonomie des boutons
        paramElement(this.createBut, 1, width);
        paramElement(this.joinBut, 2, width);
        paramElement(this.rulesBut, 3, width);
        paramElement(this.creditsBut, 4, width);
        paramElement(this.exitBut, 5, width);

        this.setOpaque(false);
    }
}
