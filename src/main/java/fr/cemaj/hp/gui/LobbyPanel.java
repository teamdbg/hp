package fr.cemaj.hp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import fr.cemaj.hp.game.Player;

/**
 * This class represents the lobby panel, displayed
 * when players are waiting others.
 * 
 * @authors Jérémy Longin
 */
public class LobbyPanel extends GraphicPanel {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private static final int MAXPLAYERS = 6;

    PlayerLabel[] players;
    int nbPlayers;
    JLabel gameMode, lobbyTitle; //ipServer
    JButton readyBut, returnBut;

    /**
     * Class constructor
     * 
     * @param width : the width of the main frame
     * @param height : the height of the main frame
     * @param listener : a reference to all listeners
     * @param picture : the picture displayed in background
     */
    LobbyPanel(int width, int height, InterfaceListeners listener, Image picture)
            throws FontFormatException, IOException {
        super(picture);

        addMouseListener(listener.mouseListener);
        addMouseMotionListener(listener.mouseListener);

        // Création des labels
        players = new PlayerLabel[MAXPLAYERS];
        for (int i = 0; i < MAXPLAYERS; i++) {
            this.players[i] = new PlayerLabel();
        }
        this.gameMode = new JLabel("Partie classique");
        //this.ipServer = new JLabel("127.0.0.1");
        this.lobbyTitle = new JLabel("Salle d'attente");
        this.readyBut = new JButton("Pret");
        this.returnBut = new JButton("Retour");

        // createBut.addActionListener(listener);

        // Mise en place des boutons
        int elementWidth, elementHeight;
        elementWidth = (int) ((double) width * 0.3);
        elementHeight = (int) ((double) height * 0.1);
        this.setLayout(null);
        this.add(this.players[0]);
        this.players[0].setBounds((int) ((double) width * 0.5) - elementWidth,
                (int) ((double) height * 0.10), elementWidth, elementHeight);
        this.add(this.players[1]);
        this.players[1].setBounds((int) ((double) width * 0.5) - elementWidth,
                (int) ((double) height * 0.23), elementWidth, elementHeight);
        this.add(this.players[2]);
        this.players[2].setBounds((int) ((double) width * 0.5) - elementWidth,
                (int) ((double) height * 0.36), elementWidth, elementHeight);
        this.add(this.players[3]);
        this.players[3].setBounds((int) ((double) width * 0.5) - elementWidth,
                (int) ((double) height * 0.49), elementWidth, elementHeight);
        this.add(this.players[4]);
        this.players[4].setBounds((int) ((double) width * 0.5) - elementWidth,
                (int) ((double) height * 0.62), elementWidth, elementHeight);
        this.add(this.players[5]);
        this.players[5].setBounds((int) ((double) width * 0.5) - elementWidth,
                (int) ((double) height * 0.75), elementWidth, elementHeight);
        this.add(this.gameMode);
        this.gameMode.setBounds((int) ((double) width * 0.5)
              + (int) ((double) width * 0.05),
              (int) ((double) height * 0.36), elementWidth, elementHeight);
//        this.gameMode.setBounds((int) ((double) width * 0.5)
//                + (int) ((double) width * 0.05),
//                (int) ((double) height * 0.23), elementWidth, elementHeight);
//        this.add(this.ipServer);
//        this.ipServer.setBounds((int) ((double) width * 0.5)
//                + (int) ((double) width * 0.05),
//                (int) ((double) height * 0.36), elementWidth, elementHeight);
        this.add(this.readyBut);
        this.readyBut.setBounds((int) ((double) width * 0.5)
                + (int) ((double) width * 0.05),
                (int) ((double) height * 0.49), elementWidth, elementHeight);
        this.readyBut.addActionListener(listener.actionListener);
        this.add(this.returnBut);
        this.returnBut.setBounds((int) ((double) width * 0.85),
                (int) ((double) height * 0.8), (int) ((double) width * 0.12),
                elementHeight);
        this.returnBut.addActionListener(listener.actionListener);

        // Ergonomie des éléments
        for (int i = 0; i < MAXPLAYERS; ++i)
            paramElement(this.players[i], i, width);
        paramElement(this.gameMode, 6, width);
        //paramElement(this.ipServer, 6, width);
        paramElement(this.readyBut, 6, width);
        this.paramElement(this.returnBut, 6, width);

        // Affichage du titrecredit
        this.add(this.lobbyTitle);
        this.lobbyTitle.setBounds((int) ((double) width * 0.5)
                - (elementWidth / 2), (int) ((double) height * 0.002),
                elementWidth, (int) ((double) height * 0.08));
        this.lobbyTitle.setFont(new Font("Grinched", Font.ITALIC,
                (int) ((double) width * 0.03)));
        this.lobbyTitle.setForeground(Color.white);
        this.lobbyTitle.setVerticalAlignment(SwingConstants.CENTER);
        this.lobbyTitle.setHorizontalAlignment(SwingConstants.CENTER);

        this.setOpaque(false);
    }

    /**
     * Function to add a player to the table of players
     * 
     * @param p : the player to add
     */
    public void addPlayer(Player p) {
        for (int i = 0; i < MAXPLAYERS; i++) {
            if (this.players[i].getPlayer() == null) {
                this.players[i].setPlayer(p);
                this.players[i].setReady(p.getReady());
                this.nbPlayers++;
                return;
            }
        }
    }

    /**
     * Remove a player from the table
     * 
     * @param id : the id of the player to remove.
     */
    public void removePlayer(int id) {
        for (int i = 0; i < this.nbPlayers; i++) {
            if (this.players[i].getPlayer().getId() == id) {
                for (int j = i; j < nbPlayers; j++) {
                    if (j != nbPlayers - 1) {
                        this.players[j].setPlayer(players[j + 1].getPlayer());
                    } else {
                        this.players[j].setPlayer(null);
                    }
                }
                nbPlayers--;
                break;
            }
        }
    }

    /**
     * Function to update player name in GUI
     * 
     * @param id : the id of the player to update
     * @param name : the new name of the player
     */
    public void updatePlayerName(int id, String name) {
        this.findPlayer(id).setName(name);
    }

    /**
     * Function to update player ready statement
     * 
     * @param id : the id of the player to update
     * @param state : the new state of the player
     */
    public void setReady(int id, boolean state) {
        findPlayer(id).setReady(state);
    }

    /**
     * Function to find a player in the table by his ID
     * 
     * @param id : the id of the player to find
     * @return the label corresponding to this player
     */
    private PlayerLabel findPlayer(int id) {
        for (int i = 0; i < this.nbPlayers; i++)
            if (this.players[i].getPlayer().getId() == id)
                return this.players[i];
        return null;
    }

    /**
     * Update game mode label
     * 
     * @param quick : game mode indicator
     */
    public void setGameMode(boolean quick) {
        if(quick)
            this.gameMode.setText("Partie rapide");
        else
            this.gameMode.setText("Partie classique");
    }
}
