package fr.cemaj.hp.gui;

import java.awt.FontFormatException;
import java.io.IOException;

import javax.swing.JLabel;

import fr.cemaj.hp.game.Player;
import fr.cemaj.hp.game.card.Abracadabra;
import fr.cemaj.hp.game.card.Inspiration;
import fr.cemaj.hp.game.card.ThunderBolt;

/**
 * This class represents enemy board.
 * 
 * @authors Jérémy Longin
 */
public class PlayerBoardEnemyBoard extends PlayerBoard  {

    private static final long serialVersionUID = 1L;

    PictureLabel handIcon, gemsIcon;
    JLabel playerName, handNumber, gemsNumber;

    /**
     * Class constructor
     * 
     * @param width : main frame width
     * @param height : main frame height
     * @param boardWidth : the width of the board
     * @param boardHeight : the height of the board
     * @param listener : all GUI listeners
     * @param position : N (north) or S (south) to put cards
     *                   up or down hand and gem icons.
     */
    public PlayerBoardEnemyBoard(int width, int height, int boardWidth, int boardHeight,
            InterfaceListeners listener, char position) throws FontFormatException, IOException {
        super();

        this.setLayout(null);

        this.playerName = new JLabel("Enemy");
        this.add(this.playerName);
        this.playerName.setBounds(2, 2, 100, 20);

        // this.setOpaque(false);
        int cardLabelWidth = (int) ((double) width * 0.055);
        int cardLabelHeight = (int) ((double) height * 0.138);
        int xPos2 = (int) (((double) boardWidth / 2) - ((cardLabelWidth * 1.5) + 3));
        int xPos3 = (int) (((double) boardWidth / 2) - (cardLabelWidth / 2));
        int xPos4 = (int) (((double) boardWidth / 2) + ((cardLabelWidth * 0.5) + 3));
        int yTopPos, yBotPos;
        if (position == 'N') {
            yTopPos = boardHeight - cardLabelHeight;
            yBotPos = getSpace(height);
        } else {
            yTopPos = getSpace(height);
            yBotPos = cardLabelHeight + (int)((double)getSpace(height)*1.5);
        }

        this.setLayout(null);

        this.grim1 = new CardLabel();
        this.grim2 = new CardLabel();
        this.grim3 = new CardLabel();
        this.handIcon = new PictureLabel();
        this.handNumber = new JLabel("x13");
        this.gemsIcon = new PictureLabel();
        this.gemsNumber = new JLabel("x13");

        this.add(this.grim1);
        this.add(this.grim2);
        this.add(this.grim3);
        this.add(this.handIcon);
        this.add(this.handNumber);
        this.add(this.gemsIcon);
        this.add(this.gemsNumber);

        this.grim1.setBounds(xPos2, yTopPos, cardLabelWidth, cardLabelHeight);
        this.grim2.setBounds(xPos3, yTopPos, cardLabelWidth, cardLabelHeight);
        this.grim3.setBounds(xPos4, yTopPos, cardLabelWidth, cardLabelHeight);
        this.handIcon.setBounds(0, yBotPos, getIconWidth(width),
                getIconHeight(height));
        this.gemsIcon.setBounds((int) ((double) getIconWidth(width) * 1.8),
                yBotPos + 10, getIconWidth(width),
                (int) ((double) getIconHeight(height) * 0.8));
        if (position == 'N') {
            this.handNumber.setBounds(
                    (int) ((double) getIconWidth(width) * 0.8),
                    (int) ((double) yBotPos * 1.5), getIconWidth(width),
                    getIconHeight(height));
            this.gemsNumber
                    .setBounds((int) ((double) getIconWidth(width) * 2.6),
                            (int) ((double) yBotPos * 1.5),
                            (int) ((double) getIconWidth(width)),
                            getIconHeight(height));
        } else {
            this.handNumber.setBounds(
                    (int) ((double) getIconWidth(width) * 0.8), yBotPos
                            + (int) ((double) getSpace(height) * 0.5),
                    getIconWidth(width), getIconHeight(height));
            this.gemsNumber
                    .setBounds((int) ((double) getIconWidth(width) * 2.6),
                            yBotPos + (int) ((double) getSpace(height) * 0.5),
                            (int) ((double) getIconWidth(width)),
                            getIconHeight(height));
        }

        this.grim1.setCard(new Abracadabra());
        this.grim2.setCard(new Inspiration(2));
        this.grim3.setCard(new ThunderBolt());
        handIcon.setIcon(this.handIcon.resize("./res/img/HandIcon.png",
                this.handIcon.getWidth(), this.handIcon.getHeight()));
        gemsIcon.setIcon(this.gemsIcon.resize("./res/img/GemIcon.png",
                this.gemsIcon.getWidth(), this.gemsIcon.getHeight()));
        this.lightParamElement(handNumber, 1, width);
        this.lightParamElement(gemsNumber, 1, width);

        // ajout des listeners
        this.grim1.addMouseListener(listener.mouseListener);
        this.grim2.addMouseListener(listener.mouseListener);
        this.grim3.addMouseListener(listener.mouseListener);

        this.setVisible(true);
    }
    
    /**
     * Function to initialize the board.
     * 
     * @param p : the player to associate.
     */
    public void initPlayer(Player p){
        this.setPlayer(p);
        
        //Initialisation valeurs numériques
        this.gemsNumber.setText("x"+this.player.getGems());
        this.handNumber.setText("x"+this.player.getHandCardsNumber());
        this.playerName.setText(this.player.getName());
    }
    
    public void updatePlayer(Player p){
        this.player = p;
        // Set null on all cards
        this.grim1.setCard(null);
        this.grim2.setCard(null);
        this.grim3.setCard(null);
        // Update grimoire cards
        if (this.player.getGrimoireCardsNumber() >= 1) {
            this.grim1.setCard(this.player.getGrimoireCardsList().get(0));
            if (this.player.getGrimoireCardsNumber() >= 2) {
                this.grim2.setCard(this.player.getGrimoireCardsList().get(1));
                if (this.player.getGrimoireCardsNumber() == 3) {
                    this.grim3.setCard(this.player.getGrimoireCardsList().get(2));
                }
            }
        }
        // Update numbers
        this.handNumber.setText("x"+this.player.getHandCardsNumber());
        this.gemsNumber.setText("x"+this.player.getGems());
    }
}
