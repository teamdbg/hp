package fr.cemaj.hp.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JTextArea;

import fr.cemaj.hp.game.card.Card;

/**
 * This class represents the game part of the game board.
 * It contains all game board GUI elements usefull for the game.
 *
 * @author Jérémy Longin
 */
public class GameBoardGamePart extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    public PlayerBoardEnemyBoard topLeftEnemyBoard;
    public PlayerBoardEnemyBoard topCenterEnemyBoard;
    public PlayerBoardEnemyBoard topRightEnemyBoard;
    public PlayerBoardEnemyBoard bottomLeftEnemyBoard;
    public PlayerBoardEnemyBoard bottomRightEnemyBoard;
    public PlayerBoardMyBoard playerBoard;
    private GameZone gameZone;
    private PictureLabel stock;
    private Discarding discarding;
    private Cauldron cauldron;
    private JTextArea gameInformation;
    private ArrayList<String> tabGameInformations;
    private ArrayList<PictureLabel> coinsLabel;
    private int width;
    private int height;

    /**
     * Class constructor.
     *
     * @param width : the width of the main frame.
     * @param height : the height of the main frame.
     * @param listener : all GUI listeners.
     * @param picture : the picture to be displayed on the background.
     * @throws FontFormatException : exception
     * @throws IOException : exception
     */
    GameBoardGamePart(int width, int height, InterfaceListeners listener,
            Image picture) throws FontFormatException, IOException {
        super(picture);

        this.width = width;
        this.height = height;

        addMouseListener(listener.mouseListener);
        addMouseMotionListener(listener.mouseListener);

        this.addMouseListener(listener.mouseListener);

        int enemyBoardWidth = (int) ((double) width * 0.181);
        int enemyBoardHeight = (int) ((double) height * 0.26);
        int playerBoardWidth = (int) ((double) width * 0.451);
        int playerBoardHeight = (int) ((double) height * 0.423);

        // Paramétrage général du panneau
        this.setLayout(null);

        // Creation des zones joueurs
        this.topLeftEnemyBoard = new PlayerBoardEnemyBoard(width,
                height,
                enemyBoardWidth,
                enemyBoardHeight, listener, 'N');
        this.topCenterEnemyBoard = new PlayerBoardEnemyBoard(width, height,
                enemyBoardWidth, enemyBoardHeight, listener, 'N');
        this.topRightEnemyBoard = new PlayerBoardEnemyBoard(width, height,
                enemyBoardWidth, enemyBoardHeight, listener, 'N');
        this.bottomLeftEnemyBoard = new PlayerBoardEnemyBoard(width, height,
                enemyBoardWidth, enemyBoardHeight, listener, 'S');
        this.bottomRightEnemyBoard = new PlayerBoardEnemyBoard(width, height,
                enemyBoardWidth, enemyBoardHeight, listener, 'S');
        this.playerBoard = new PlayerBoardMyBoard(width,
                height,
                playerBoardWidth,
                listener);

        // Ajout des zones joueurs
        this.add(this.topLeftEnemyBoard);
        this.add(this.topCenterEnemyBoard);
        this.add(this.topRightEnemyBoard);
        this.add(this.bottomLeftEnemyBoard);
        this.add(this.playerBoard);
        this.add(this.bottomRightEnemyBoard);

        // Mise en forme des zones joueurs
        this.topLeftEnemyBoard.setBounds(10, 10, enemyBoardWidth,
                enemyBoardHeight);
        this.topCenterEnemyBoard
            .setBounds(
                (int)
                    ((((double) width * 0.844) * 0.5) - (enemyBoardWidth / 2)),
                        10, enemyBoardWidth, enemyBoardHeight);
        this.topRightEnemyBoard.setBounds(
                (int) (((double) width * 0.844) - 10 - enemyBoardWidth), 10,
                enemyBoardWidth, enemyBoardHeight);
        this.bottomLeftEnemyBoard.setBounds((int) ((double) 10), height - 10
                - enemyBoardHeight, enemyBoardWidth, enemyBoardHeight);
        this.playerBoard
            .setBounds(
                (int)
                    ((((double) width * 0.844) * 0.5) - (playerBoardWidth / 2)),
                        height - 10 - playerBoardHeight, playerBoardWidth,
                        playerBoardHeight);
        this.bottomRightEnemyBoard.setBounds(
                (int) (((double) width * 0.844) - 10 - enemyBoardWidth),
                height - 10 - enemyBoardHeight,
                enemyBoardWidth,
                enemyBoardHeight);
        this.paramPanel(this.topLeftEnemyBoard, width);
        this.paramPanel(this.topCenterEnemyBoard, width);
        this.paramPanel(this.topRightEnemyBoard, width);
        this.paramPanel(this.bottomLeftEnemyBoard, width);
        this.paramPanel(this.playerBoard, width);
        this.paramPanel(this.bottomRightEnemyBoard, width);

        // Mise en place zone de jeu
        this.gameZone = new GameZone(width, height, listener);
        this.add(this.gameZone);
        this.gameZone
                .setBounds(
                        (int)
                            ((((double) width * 0.82) * 0.5)
                            - (playerBoardWidth / 2)),
                        (int) ((double) playerBoardHeight * 0.7),
                        (int) ((double) playerBoardWidth * 0.8),
                        (int) ((double) playerBoardHeight * 0.56));

        // Mise en place des autres zones
        this.stock = new PictureLabel();
        this.discarding = new Discarding(
                new ImageIcon("./res/img/HocusPocus.png").getImage(),
                    this.gameZone.getCardSize());
        this.cauldron = new Cauldron("./res/img/Cauldron.png",
                (int) ((double) width * 0.1146),
                (int) ((double) width * 0.1146),
                listener);
        this.gameInformation = new JTextArea();
        this.specificParamElement(this.gameInformation, width);
        this.tabGameInformations = new ArrayList<String>();
        addAction("Bienvenue dans Hocus Pocus by CEMAJ");
        this.add(this.stock);
        this.add(this.cauldron);
        this.add(this.discarding);
        this.add(this.gameInformation);
        this.gameInformation.setEditable(false);
        this.stock.setBounds((int) ((double) width * 0.58),
                (int) ((double) height * 0.32),
                (int) ((double) width * 0.0938),
                (int) ((double) height * 0.2333));
        this.cauldron.setBounds((int) ((double) width * 0.7),
                (int) ((double) height * 0.32),
                (int) ((double) width * 0.1146),
                (int) ((double) width * 0.1146));
        this.discarding.setBounds((int) ((double) width * 0.04),
                (int) ((double) height * 0.4), (int) ((double) height * 0.25),
                (int) ((double) height * 0.25));
        this.gameInformation.setBounds((int) ((double) width * 0.652),
                (int) ((double) height * 0.57), (int) ((double) width * 0.19),
                (int) ((double) width * 0.057));

        this.gameInformation.setBackground(Color.white);
        this.stock.setIcon(this.stock.resize("./res/img/Stock.png",
                this.stock.getWidth(), this.stock.getHeight()));

        this.stock.addMouseListener(listener.mouseListener);

        initCoinsPictureLabel(width, height);
        this.setVisible(true);
    }

    /**
     * This function add a line of text into the list tabGameInformation\
     * and display it in the textArea on gameBoard.
     *
     * @param s : the text to print
     */
    public void addAction(String s) {
        //Ajout du texte à la liste
        this.tabGameInformations.add(s);
        //Ajout à l'affichage sur le gameBoard
        String toDisplay = new String("");
        this.gameInformation.setText("");
        for(int i = this.tabGameInformations.size() - 5;
                i < this.tabGameInformations.size();
                i++) {
            if(i >= 0) {
                if(i != this.tabGameInformations.size() - 1) {
                    toDisplay += this.tabGameInformations.get(i) + "\n";
                } else {
                    toDisplay += this.tabGameInformations.get(i);
                }
            }
        }
        this.gameInformation.setText(toDisplay);
    }

    /**
     * Function to initialize positions of coins labels.
     *
     * @param width : the width of the main frame.
     * @param height : the height of the main frame.
     */
    public void initCoinsPictureLabel(int width, int height) {
        this.coinsLabel = new ArrayList<PictureLabel>();
        for (int i = 0; i < 6; i++) {
            this.coinsLabel.add(i, new PictureLabel());
            this.add(this.coinsLabel.get(i));
            this.coinsLabel.get(i).setVisible(false);
        }
        int coinSize = (int) ((double) width * 0.05);
        this.coinsLabel.get(0).setBounds((int) ((double) width * 0.04),
                (int) ((double) height * 0.27), coinSize, coinSize);
        this.coinsLabel.get(1).setBounds((int) ((double) width * 0.29),
                (int) ((double) height * 0.14), coinSize, coinSize);
        this.coinsLabel.get(2).setBounds((int) ((double) width * 0.615),
                (int) ((double) height * 0.18), coinSize, coinSize);
        this.coinsLabel.get(3).setBounds((int) ((double) width * 0.1),
                (int) ((double) height * 0.67), coinSize, coinSize);
        this.coinsLabel.get(4).setBounds((int) ((double) width * 0.24),
                (int) ((double) height * 0.68), coinSize, coinSize);
        this.coinsLabel.get(5).setBounds((int) ((double) width * 0.75),
                (int) ((double) height * 0.67), coinSize, coinSize);
        for (int i = 0; i < 6; i++) {
            this.coinsLabel.get(i).setPicture("./res/img/Coin.png");
        }

        //Attribution des coinLabel aux Boardpanel
        this.topLeftEnemyBoard.setCoin(this.coinsLabel.get(0));
        this.topCenterEnemyBoard.setCoin(this.coinsLabel.get(1));
        this.topRightEnemyBoard.setCoin(this.coinsLabel.get(2));
        this.bottomLeftEnemyBoard.setCoin(this.coinsLabel.get(3));
        this.playerBoard.setCoin(this.coinsLabel.get(4));
        this.bottomRightEnemyBoard.setCoin(this.coinsLabel.get(5));
    }

    /**
     * Function to get visible player boards.
     *
     * @return a list of PlayerBoard
     */
    public ArrayList<PlayerBoard> getActivePlayerBoards() {
        ArrayList<PlayerBoard> players = new ArrayList<PlayerBoard>();
        if(this.playerBoard.isVisible()) {
            players.add(this.playerBoard);
        }
        if(this.topLeftEnemyBoard.isVisible()) {
            players.add(this.topLeftEnemyBoard);
        }
        if(this.topCenterEnemyBoard.isVisible()) {
            players.add(this.topCenterEnemyBoard);
        }
        if(this.topRightEnemyBoard.isVisible()) {
            players.add(this.topRightEnemyBoard);
        }
        if(this.bottomLeftEnemyBoard.isVisible()) {
            players.add(this.bottomLeftEnemyBoard);
        }
        if(this.bottomRightEnemyBoard.isVisible()) {
            players.add(this.bottomRightEnemyBoard);
        }
        return players;
    }

    /**
     * Getter.
     *
     * @return cauldron object
     */
    public Cauldron getCauldron() {
        return this.cauldron;
    }

    /**
     * Getter.
     *
     * @return stock object
     */
    public PictureLabel getStock() {
        return this.stock;
    }

    /**
     * Getter.
     *
     * @return game zone object
     */
    public GameZone getGameZone() {
        return this.gameZone;
    }

    /**
     * Method to update discard.
     *
     * @param cards : list of cards
     * @throws FontFormatException : exception
     * @throws IOException : exception
     */
    public void updateDiscard(ArrayList<Card> cards)
            throws FontFormatException, IOException {
        this.remove(this.discarding);
        this.discarding = new Discarding(resize("./res/img/Discard.png",
                (int) ((double) this.width * 0.1286),
                (int) ((double) this.width * 0.1443)).getImage(),
                this.gameZone.getCardSize());
        this.add(this.discarding);
        this.discarding.setBounds((int) ((double) this.width * 0.04),
                (int) ((double) this.height * 0.4),
                (int) ((double) this.width * 0.1286),
                (int) ((double) this.width * 0.1443));
        this.discarding.setDiscard(cards.get(cards.size() - 1));
    }

    /**
     * Function to resize a picture (Same in MainFrame class).
     *
     * @param filePath : the path of picture file.
     * @param newWidth : the new width of the picture.
     * @param newHeight : the new height of the picture.
     * @return an ImageIcon of the picture with the new size.
     */
    public ImageIcon resize(String filePath, int newWidth, int newHeight) {
        int type = BufferedImage.TYPE_INT_ARGB;

        Image originalImage = new ImageIcon(filePath).getImage();
        ImageIcon returnedImage;

        BufferedImage resizedImage = new BufferedImage(newWidth,
                newHeight,
                type);
        Graphics2D g = resizedImage.createGraphics();

        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.drawImage(originalImage, 0, 0, newWidth, newHeight, this);
        g.dispose();

        returnedImage = new ImageIcon(resizedImage);
        return returnedImage;
    }

    /**
     * getMyPlayerBoard.
     * @return a PlayerBoardMyBoard
     */
    public PlayerBoardMyBoard getMyPlayerBoard() {
        return this.playerBoard;
    }
}
