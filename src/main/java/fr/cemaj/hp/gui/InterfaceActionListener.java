package fr.cemaj.hp.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * This class contains actions for buttons in all GUI.
 *
 * @author Jérémy Longin
 */
public class InterfaceActionListener implements ActionListener {

    MainFrame mainFrame;

    /**
     * Class constructor.
     * @param mf MainFrame
     */
    InterfaceActionListener(MainFrame mf) {
        this.mainFrame = mf;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // MainMenuPanel and MenuBar actions
        if (e.getSource() == this.mainFrame.getMainMenuPan().createBut
                || e.getSource() == this.mainFrame.getCreateItem()) {
            this.mainFrame.getCreaPopup().setVisible(true);
        }
        if (e.getSource() == this.mainFrame.getMainMenuPan().joinBut
                || e.getSource() == this.mainFrame.getJoinItem()) {
            this.mainFrame.getJoinPopup().setVisible(true);
        }
        if (e.getSource() == this.mainFrame.getRulesItem()
                || e.getSource() == this.mainFrame.getMainMenuPan().rulesBut) {
            this.mainFrame.changePanel(this.mainFrame.getRulesPan());
        }
        if (e.getSource() == this.mainFrame.getCreditsItem()
                || e.getSource()
                        == this.mainFrame.getMainMenuPan().creditsBut) {
            this.mainFrame.changePanel(this.mainFrame.getCreditPan());
        }
        if (e.getSource() == this.mainFrame.getExitGameItem()) {
            if(this.mainFrame.getVisiblePan() instanceof GameBoard
                    || this.mainFrame.getLastVisiblePan()
                            instanceof GameBoard) {
                if (JOptionPane
                        .showOptionDialog(
                                this.mainFrame,
                                "Vous êtes sur le point de quitter une partie "
                                + "en cours,\nceci mettra fin au jeu pour tous "
                                + "les joueurs connectés.\nContinuer malgré "
                                + "tout ?",
                                "Quitter la partie",
                                JOptionPane.OK_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                null,
                                null) == 0) {
                    this.mainFrame.changePanel(this.mainFrame.getMainMenuPan());
                }
            } else {
                if(this.mainFrame.getVisiblePan() instanceof LobbyPanel) {
                    if (JOptionPane
                            .showOptionDialog(
                                    this.mainFrame,
                                    "Vous êtes sur le point de commencer une "
                                    + "partie,\nquitter maintenant vous "
                                    + "déconnectera du serveur.\nContinuer "
                                    + "malgré tout ?",
                                    "Quitter la partie",
                                    JOptionPane.OK_CANCEL_OPTION,
                                    JOptionPane.QUESTION_MESSAGE,
                                    null,
                                    null,
                                    null) == 0) {
                        this.mainFrame.client.leaveGame();
                        this.mainFrame
                            .changePanel(this.mainFrame.getMainMenuPan());
                    }
                }
            }
        }
        if (e.getSource() == this.mainFrame.getExitAppItem()
                || e.getSource() == this.mainFrame.getMainMenuPan().exitBut) {
            if(this.mainFrame.getVisiblePan() instanceof GameBoard
                    || this.mainFrame.getLastVisiblePan()
                            instanceof GameBoard) {
                if (JOptionPane
                        .showOptionDialog(
                                this.mainFrame,
                                "Vous êtes sur le point de quitter une partie "
                                + "en cours,\nceci mettra fin au jeu pour tous "
                                + "les joueurs connectés.\nContinuer malgré "
                                + "tout ?",
                                "Quitter le jeu", JOptionPane.OK_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE,
                                null,
                                null,
                                null) == 0) {
                    this.mainFrame.client.leaveGame();
                    System.exit(0);
                }
            } else {
                if(this.mainFrame.getVisiblePan() instanceof LobbyPanel
                        || this.mainFrame.getLastVisiblePan()
                                instanceof LobbyPanel) {
                    if (JOptionPane
                            .showOptionDialog(
                                    this.mainFrame,
                                    "Vous êtes sur le point de commencer une "
                                    + "partie,\nquitter maintenant vous "
                                    + "déconnectera du serveur.\nContinuer "
                                    + "malgré tout ?",
                                    "Quitter le jeu",
                                    JOptionPane.OK_CANCEL_OPTION,
                                    JOptionPane.QUESTION_MESSAGE,
                                    null,
                                    null,
                                    null) == 0) {
                        this.mainFrame.client.leaveGame();
                        System.exit(0);
                    }
                } else {
                    System.exit(0);
                }
            }
        }
        // LobbyPanel actions
        if (e.getSource() == this.mainFrame.getLobbyPan().readyBut) {
            this.mainFrame.client.toggleReady();
        }
        if (e.getSource() == this.mainFrame.getLobbyPan().returnBut) {
            if (JOptionPane
                    .showOptionDialog(
                            this.mainFrame,
                            "Vous êtes sur le point de commencer une partie,\n"
                                    + "quitter maintenant vous déconnectera du "
                                    + "serveur.\nContinuer malgré tout ?",
                            "Quitter la partie", JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            null,
                            null) == 0) {
                this.mainFrame.client.leaveGame();
                this.mainFrame.changePanel(this.mainFrame.getMainMenuPan());
            }
        }
        // CreditsPanel actions
        if (e.getSource() == this.mainFrame.getCreditPan().returnBut
                || e.getSource() == this.mainFrame.getRulesPan().returnBut) {
            this.mainFrame.backToLastPanel();
        }
        // RulesPanel actions
        if(e.getSource() == this.mainFrame.getRulesPan().previousBut) {
            this.mainFrame.getRulesPan().previousPages();
        }
        if(e.getSource() == this.mainFrame.getRulesPan().nextBut) {
            this.mainFrame.getRulesPan().nextPages();
        }
        // ResultsPanel actions
        if(e.getSource() == this.mainFrame.getResultsPan().returnBut) {
            this.mainFrame.changePanel(this.mainFrame.getMainMenuPan());
        }
        // CreateGamePopup actions
        if (e.getSource() == this.mainFrame.getCreaPopup().validateBut) {
            this.mainFrame.getCreaPopup().validation();
        }
        if (e.getSource() == this.mainFrame.getCreaPopup().cancelBut) {
            this.mainFrame.getCreaPopup().dispose();
        }
        // JoinGamePopup actions
        if (e.getSource() == this.mainFrame.getJoinPopup().validateBut) {
            this.mainFrame.getJoinPopup().validation();
        }
        if (e.getSource() == this.mainFrame.getJoinPopup().cancelBut) {
            this.mainFrame.getJoinPopup().dispose();
        }
    }

    /**
     * Function to close the game.
     */
    void endOfGame() {
        //TODO stop the server if "hébergeur"
        System.exit(0);
    }
}
