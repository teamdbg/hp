package fr.cemaj.hp.gui;

import java.awt.Color;
import java.awt.FontFormatException;
import java.awt.Image;
import java.io.IOException;

import fr.cemaj.hp.game.Player;

/**
 * This class represents the game board on GUI.
 * It contains the two parts of the game board.
 *
 * @author Jérémy Longin
 */
public class GameBoard extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    private GameBoardGamePart gamePart;
    private GameBoardZoomPart zoomPart;

    /**
     * Class constructor.
     *
     * @param width : the width of the main frame
     * @param height : the height of the main frame
     * @param listener : a reference to all listeners
     * @param picture : the picture displayed in background
     * @throws FontFormatException : an exception
     * @throws IOException : an exception
     */
    GameBoard(int width, int height, InterfaceListeners listener,
            Image picture) throws FontFormatException,
            IOException {
        super();

        // paramétrage général du panneau
        this.addMouseListener(listener.mouseListener);
        addMouseMotionListener(listener.mouseListener);
        this.setLayout(null);

        // Création panneau board et zoom
        this.gamePart = new GameBoardGamePart(width, height, listener, picture);
        this.zoomPart = new GameBoardZoomPart(width, height, listener);

        this.add(this.gamePart);
        this.add(this.zoomPart);

        this.gamePart.setBounds(0, 0, (int) ((double) width * 0.844), height);
        this.zoomPart.setBounds((int) ((double) width * 0.844), 0,
                (int) ((double) width * 0.156), height);
        this.setBackground(Color.WHITE);
    }

    /**
     * Getter.
     *
     * @return the gamePart panel
     */
    public GameBoardGamePart getGamePart() {
        return this.gamePart;
    }

    /**
     * Getter.
     *
     * @return the zoomPart panel
     */
    public GameBoardZoomPart getZoomPart() {
        return this.zoomPart;
    }

    /**
     * Method to initialize and display the gameBoardPanel at\
     * the beginning of the game.
     *
     * @param players : a list of the players in game
     * @param aCauldron : number of gems contain in the cauldron
     * @param myId : a reference to local player Id in game
     */
    public void displayGameBoard(Object[] players, int aCauldron, int myId) {
        int i;
        int myI = 0;
        int tabSize = players.length;

        // Initialisation des successeurs
        this.gamePart.topLeftEnemyBoard.setVisible(false);
        this.gamePart.topCenterEnemyBoard.setVisible(false);
        this.gamePart.topRightEnemyBoard.setVisible(false);
        this.gamePart.bottomLeftEnemyBoard.setVisible(false);
        this.gamePart.bottomRightEnemyBoard.setVisible(false);
        this.gamePart.playerBoard.setVisible(true);
        switch (tabSize) {
        case 2:
            /*
             * Initialize game board for 2 players
             */
            this.gamePart.topCenterEnemyBoard.setVisible(true);

            this.gamePart.topCenterEnemyBoard
                    .setNext(this.gamePart.playerBoard);
            this.gamePart.playerBoard
                    .setNext(this.gamePart.topCenterEnemyBoard);
            break;
        case 3:
            /*
             * Initialize game board for 3 players
             */
            this.gamePart.topLeftEnemyBoard.setVisible(true);
            this.gamePart.topRightEnemyBoard.setVisible(true);

            this.gamePart.topLeftEnemyBoard
                    .setNext(this.gamePart.topRightEnemyBoard);
            this.gamePart.topRightEnemyBoard.setNext(this.gamePart.playerBoard);
            this.gamePart.playerBoard.setNext(this.gamePart.topLeftEnemyBoard);
            break;
        case 4:
            /*
             * Initialize game board for 4 players
             */
            this.gamePart.topLeftEnemyBoard.setVisible(true);
            this.gamePart.topCenterEnemyBoard.setVisible(true);
            this.gamePart.topRightEnemyBoard.setVisible(true);

            this.gamePart.topLeftEnemyBoard
                    .setNext(this.gamePart.topCenterEnemyBoard);
            this.gamePart.topCenterEnemyBoard
                    .setNext(this.gamePart.topRightEnemyBoard);
            this.gamePart.topRightEnemyBoard.setNext(this.gamePart.playerBoard);
            this.gamePart.playerBoard.setNext(this.gamePart.topLeftEnemyBoard);
            break;
        case 5:
            /*
             * Initialize game board for 5 players
             */
            this.gamePart.bottomLeftEnemyBoard.setVisible(true);
            this.gamePart.topLeftEnemyBoard.setVisible(true);
            this.gamePart.topRightEnemyBoard.setVisible(true);
            this.gamePart.bottomRightEnemyBoard.setVisible(true);

            this.gamePart.bottomLeftEnemyBoard
                    .setNext(this.gamePart.topLeftEnemyBoard);
            this.gamePart.topLeftEnemyBoard
                    .setNext(this.gamePart.topRightEnemyBoard);
            this.gamePart.topRightEnemyBoard
                    .setNext(this.gamePart.bottomRightEnemyBoard);
            this.gamePart.bottomRightEnemyBoard
                    .setNext(this.gamePart.playerBoard);
            this.gamePart.playerBoard
                    .setNext(this.gamePart.bottomLeftEnemyBoard);
            break;
        case 6:
            /*
             * Initialize game board for 6 players
             */
            this.gamePart.bottomLeftEnemyBoard.setVisible(true);
            this.gamePart.topLeftEnemyBoard.setVisible(true);
            this.gamePart.topCenterEnemyBoard.setVisible(true);
            this.gamePart.topRightEnemyBoard.setVisible(true);
            this.gamePart.bottomRightEnemyBoard.setVisible(true);

            this.gamePart.bottomLeftEnemyBoard
                    .setNext(this.gamePart.topLeftEnemyBoard);
            this.gamePart.topLeftEnemyBoard
                    .setNext(this.gamePart.topCenterEnemyBoard);
            this.gamePart.topCenterEnemyBoard
                    .setNext(this.gamePart.topRightEnemyBoard);
            this.gamePart.topRightEnemyBoard
                    .setNext(this.gamePart.bottomRightEnemyBoard);
            this.gamePart.bottomRightEnemyBoard
                    .setNext(this.gamePart.playerBoard);
            this.gamePart.playerBoard
                    .setNext(this.gamePart.bottomLeftEnemyBoard);
            break;
        }

        // Association boards with players
        for (i = 0; i < tabSize; i++) {
            if (((Player) (players[i])).getId() == myId) {
                this.gamePart.playerBoard.initPlayer((Player) (players[i]));
                myI = i;
            }
        }
        PlayerBoardEnemyBoard currentBoard
            = (PlayerBoardEnemyBoard) this.gamePart.playerBoard.getNext();
        for (i = myI + 1; i < tabSize; i++) {
            currentBoard.initPlayer((Player) players[i]);
            if (currentBoard.getNext() instanceof PlayerBoardEnemyBoard) {
                currentBoard = (PlayerBoardEnemyBoard) currentBoard.getNext();
            }
        }
        for (i = 0; i < myI; i++) {
            currentBoard.initPlayer((Player) players[i]);
            if (i != myI - 1) {
                currentBoard = (PlayerBoardEnemyBoard) currentBoard.getNext();
            }
        }
    }
}
