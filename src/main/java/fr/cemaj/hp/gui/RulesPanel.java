package fr.cemaj.hp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * This class represents the panel to see game rules.
 * 
 * @authors Jérémy Longin
 */
public class RulesPanel extends GraphicPanel {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    JLabel rulesTitle;
    JButton returnBut, previousBut, nextBut;
    PictureLabel leftPage, rightPage;
    String tabPages[];
    int leftPrintedPage, rightPrintedPage;
    
    /**
     * Class constructor
     * 
     * @param width : the width of the main frame
     * @param height : the height of the main frame
     * @param listener : a reference to all listeners
     * @param picture : the picture displayed in background
     */
    RulesPanel(int width, int height, InterfaceListeners listener, Image picture)
            throws FontFormatException, IOException {
        super(picture);

        this.addMouseListener(listener.mouseListener);
        this.addMouseMotionListener(listener.mouseListener);

        int elementWidth, elementHeight;
        elementWidth = (int) ((double) width * 0.2552);
        elementHeight = (int) ((double) height * 0.8833);

        // Paramétrage panneau
        this.setLayout(null);

        // Création des éléments
        this.returnBut = new JButton("Retour");
        this.leftPage = new PictureLabel();
        this.rightPage = new PictureLabel();
        this.rulesTitle = new JLabel("Règles du jeu");
        this.previousBut = new JButton();
        this.nextBut = new JButton();

        // Initialisation tableau
        this.tabPages = new String[16];
        for (int i = 0; i < 16; i++) {
            this.tabPages[i] = "./res/img/rules/page" + (i + 1) + ".png";
        }

        // Mise en forme titre
        this.add(this.rulesTitle);
        this.rulesTitle.setBounds((int) ((double) width * 0.5)
                - (elementWidth / 2), (int) ((double) height * 0.002),
                elementWidth, (int) ((double) height * 0.08));
        this.rulesTitle.setFont(new Font("Grinched", Font.ITALIC,
                (int) ((double) width * 0.03)));
        this.rulesTitle.setForeground(Color.white);
        this.rulesTitle.setVerticalAlignment(SwingConstants.CENTER);
        this.rulesTitle.setHorizontalAlignment(SwingConstants.CENTER);

        // Mise en forme bouton retour
        this.add(this.returnBut);
        this.returnBut.setBounds((int) ((double) width * 0.85),
                (int) ((double) height * 0.8), (int) ((double) width * 0.12),
                (int) ((double) height * 0.1));
        this.returnBut.addActionListener(listener.actionListener);

        // Mise en forme labels
        this.add(this.leftPage);
        this.add(this.rightPage);
        this.leftPage.setBounds((width / 2) - (elementWidth) - 10,
                this.rulesTitle.getHeight(), elementWidth, elementHeight);
        this.rightPage.setBounds((width / 2) + 10, this.rulesTitle.getHeight(),
                elementWidth, elementHeight);
        this.leftPage.setIcon(null);
        this.rightPage.setIcon(this.rightPage.resize(this.tabPages[0],
                this.rightPage.getWidth(), this.rightPage.getHeight()));
        this.leftPrintedPage = -1;
        this.rightPrintedPage = 0;

        // Mise en forme boutons previous/next
        int nextButSize = (int) ((double) width * 0.06);
        int nextButPictureSize = (int) ((double) nextButSize * 0.8);
        this.add(this.previousBut);
        this.add(this.nextBut);
        this.previousBut.setEnabled(false);
        this.previousBut.setBounds(nextButSize, height / 2 - nextButSize / 2,
                nextButSize, nextButSize);
        this.nextBut.setBounds(width - nextButSize * 2, height / 2
                - nextButSize / 2, nextButSize, nextButSize);
        this.previousBut.setIcon((new PictureLabel()).resize(
                "./res/img/previous.png", nextButPictureSize,
                nextButPictureSize));
        this.nextBut.setIcon((new PictureLabel()).resize("./res/img/next.png",
                nextButPictureSize, nextButPictureSize));
        this.previousBut.addActionListener(listener.actionListener);
        this.nextBut.addActionListener(listener.actionListener);

        this.addKeyListener(listener.keyListener);

        this.paramElement(this.returnBut, 6, width);
    }

    /**
     * Function to display previous pages.
     */
    void previousPages() {
        if (this.leftPrintedPage != -1) {
            this.leftPrintedPage = this.leftPrintedPage - 2;
            this.rightPrintedPage = this.rightPrintedPage - 2;
            if (this.leftPrintedPage != -1) {
                this.leftPage.setIcon(this.leftPage.resize(
                        this.tabPages[this.leftPrintedPage],
                        this.leftPage.getWidth(), this.leftPage.getHeight()));
                this.previousBut.setEnabled(true);
                this.nextBut.setEnabled(true);
            } else {
                this.leftPage.setIcon(null);
                this.previousBut.setEnabled(false);
                this.nextBut.setEnabled(true);
            }
            this.rightPage.setIcon(this.rightPage.resize(
                    this.tabPages[this.rightPrintedPage],
                    this.rightPage.getWidth(), this.rightPage.getHeight()));
        }
    }

    /**
     * Function to display next pages.
     */
    void nextPages() {
        if (this.rightPrintedPage != 16) {
            this.leftPrintedPage = this.leftPrintedPage + 2;
            this.rightPrintedPage = this.rightPrintedPage + 2;
            this.leftPage.setIcon(this.leftPage.resize(
                    this.tabPages[this.leftPrintedPage],
                    this.leftPage.getWidth(), this.leftPage.getHeight()));
            if (this.rightPrintedPage != 16) {
                this.rightPage.setIcon(this.rightPage.resize(
                        this.tabPages[this.rightPrintedPage],
                        this.leftPage.getWidth(), this.rightPage.getHeight()));
                this.previousBut.setEnabled(true);
                this.nextBut.setEnabled(true);
            } else {
                this.rightPage.setIcon(null);
                this.previousBut.setEnabled(true);
                this.nextBut.setEnabled(false);
            }
        }
    }
}
