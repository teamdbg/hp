package fr.cemaj.hp.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Image;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 * This abstract class represent the panels in the game. It's use to stock
 * elements colors in the GUI, the Cemaj font used for this game and some
 * methods/functions to parameter elements or set background.
 *
 * @author Jérémy Longin
 */
public abstract class GraphicPanel extends JPanel {

    private static final long serialVersionUID = 1L;

    Image picture;

    Color colorBlue = new Color(0, 87, 237);
    Color colorGreen = new Color(124, 179, 0);
    Color colorYellow = new Color(235, 255, 50);
    Color colorOrange = new Color(252, 105, 6);
    Color colorRed = new Color(255, 3, 3);
    Color colorViolet = new Color(196, 0, 255);
    Color colorGrey = new Color(150, 150, 150);

    Font cemajFont;

    /**
     * Clacc constructor.
     * @throws FontFormatException : an exception
     * @throws IOException : an exception
     */
    GraphicPanel() throws FontFormatException, IOException {
        super();

        InputStream is = new FileInputStream("./res/font/Grinch.ttf");
        this.cemajFont = Font.createFont(Font.TRUETYPE_FONT, is);
    }

    /**
     * Class constructor.
     *
     * @param img (Image)
     * @throws FontFormatException : an exception
     * @throws IOException : an exception
     */
    public GraphicPanel(Image img) throws FontFormatException, IOException {
        this();

        this.picture = img;
        Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
        this.setPreferredSize(size);
        this.setMinimumSize(size);
        this.setMaximumSize(size);
        this.setSize(size);
        this.setLayout(null);
    }

    /**
     * Method to display the background with the picture.
     * @param g (Graphics)
     */
    public void paintComponent(Graphics g) {
        if (this.picture != null) {
            g.drawImage(this.picture, 0, 0, null);
        }
    }

    /**
     * General method to set components.
     *
     * @param comp : the component to set.
     * @param bor : the number corresponding to the color of the component.
     * @param width : the width of the main frame (to adjust the font size).
     */
    public void paramElement(JComponent comp, int bor, int width) {
        Border border;
        int borderSize = (int) ((double) width * 0.002);
        switch (bor) {
        case 0:
            border = new LineBorder(this.colorViolet, borderSize);
            break;
        case 1:
            border = new LineBorder(this.colorBlue, borderSize);
            break;
        case 2:
            border = new LineBorder(this.colorGreen, borderSize);
            break;
        case 3:
            border = new LineBorder(this.colorYellow, borderSize);
            break;
        case 4:
            border = new LineBorder(this.colorOrange, borderSize);
            break;
        case 5:
            border = new LineBorder(this.colorRed, borderSize);
            break;
        case 6:
            border = new LineBorder(this.colorGrey, borderSize);
            break;
        default:
            border = new LineBorder(this.colorGrey, 0);
            break;
        }
        comp.setBorder(border);
        if (!(comp instanceof PictureLabel)) {
            comp.setBackground(Color.white);
        }
        comp.setOpaque(true);
        if (!(comp instanceof JTextArea)) {
            lightParamElement(comp, bor, width);
        }
    }

    /**
     * Method to set components Font and specific settings.
     *
     * @param comp : JComponent
     * @param bor : int
     * @param width : int
     */
    public void lightParamElement(JComponent comp, int bor, int width) {
        // comp.setFont(new Font("Grinched", Font.ITALIC,
        // (int) ((double) width * 0.03)));
        comp.setFont(this.cemajFont.deriveFont(Font.ITALIC,
                (int) ((double) width * 0.03)));
        if (comp instanceof JLabel) {
            paramElementSpe((JLabel) comp);
        } else {
            if (comp instanceof JButton) {
                paramElementSpe((JButton) comp);
            }
        }
    }

    /**
     * specificParamElement.
     * @param comp : JTextArea
     * @param width : int
     */
    public void specificParamElement(JTextArea comp, int width) {
        Border border;
        int borderSize = (int) ((double) width * 0.002);
        border = new LineBorder(this.colorGrey, borderSize);
        comp.setBorder(border);
        comp.setBackground(Color.white);
    }

    /**
     * Specific method to set JButton components.
     *
     * @param comp : the jbutton to set
     */
    private void paramElementSpe(JButton comp) {
        comp.setVerticalAlignment(SwingConstants.CENTER);
        comp.setHorizontalAlignment(SwingConstants.CENTER);
    }

    /**
     * Specific method to set JButton components.
     *
     * @param comp : the JLabel to set
     */
    public void paramElementSpe(JLabel comp) {
        comp.setVerticalAlignment(SwingConstants.CENTER);
        comp.setHorizontalAlignment(SwingConstants.CENTER);
    }

    /**
     * Specific method to set current player gameBoard.
     *
     * @param pan : the panel
     * @param width : main frame width.
     */
    public void paramPanel(PlayerBoardMyBoard pan, int width) {
        // pan.setBackground(this.colorGreen);
        pan.setOpaque(false);
        pan.setVisible(true);
    }

    /**
     * Specific method to set enemies gameBoard.
     *
     * @param pan : the panel
     * @param width : main frame width.
     */
    public void paramPanel(PlayerBoardEnemyBoard pan, int width) {
        // pan.setBackground(this.colorRed);
        pan.setOpaque(false);
        pan.setVisible(true);
    }

    /**
     * Function to calculate iconWidth for player gameBoard.
     *
     * @param width : main frame width
     * @return calculateWidth
     */
    int getIconWidth(int width) {
        return (int) ((double) width * 0.05);
    }

    /**
     * Function to calculate iconHeight for player gameBoard.
     *
     * @param height : main frame width
     * @return calculateHeight
     */
    int getIconHeight(int height) {
        return (int) ((double) height * 0.08);
    }

    /**
     * Function to calculate space.
     *
     * @param height : main frame width
     * @return calculateSpace
     */
    int getSpace(int height) {
        return (int) ((double) height * 0.03);
    }
}
