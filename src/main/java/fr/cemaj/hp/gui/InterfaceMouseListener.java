package fr.cemaj.hp.gui;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import fr.cemaj.hp.game.card.Card;
import fr.cemaj.hp.game.card.CardTargeting;
import fr.cemaj.hp.game.card.PlayerTargeting;

/**
 * This class contains actions for mouse events in all GUI\
 * Except for CrystalBallPopup.
 *
 * @author Jérémy Longin
 */
public class InterfaceMouseListener implements MouseListener,
        MouseMotionListener {

    private MainFrame mainFrame;
    private CardLabel originDragLabel;
    private JComponent lastEntered;
    private CardLabel onComponent;
    private Card cardMemory;
    private int targeting;
    private int cardsSelectedNb;
    private ArrayList<Card> selectedCards;
    private boolean secondTarget;
    private boolean grimoireToComplete;

    /**
     * Class constructor.
     *
     * @param mf : a reference to the main frame
     */
    InterfaceMouseListener(MainFrame mf) {
        this.mainFrame = mf;
        this.targeting = 0;
        this.secondTarget = false;
        this.cardsSelectedNb = 0;
        this.grimoireToComplete = false;
        this.selectedCards = new ArrayList<Card>();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // Cauldron or stock click allow if it's your turn
        if (this.mainFrame.getBoardPan().getGamePart().getMyPlayerBoard()
                .getCoinVisibility()
                && !this.mainFrame.client.isCardInGame()
                && !this.grimoireToComplete) {
            // Cauldron click
            if (e.getSource() instanceof JLabel
                    && e.getSource() == this.mainFrame.getBoardPan()
                            .getGamePart().getCauldron().getGemNbLabel()) {
                this.mainFrame.client.endTurn(true);
            }
            // Stock click
            if (e.getSource() == this.mainFrame.getBoardPan().getGamePart()
                    .getStock()) {
                this.mainFrame.client.endTurn(false);
            }
        }
        // Card click
        if (e.getSource() instanceof CardLabel && e.getButton() == 1) {
            // Select card for drag
            if (this.targeting == 0
                    && !((CardLabel) e.getSource()).isTargeted()) {
                if (((JComponent) e.getSource()).getParent() == this.mainFrame
                        .getBoardPan().getGamePart().playerBoard) {
                    if (this.onComponent != null
                            && this.onComponent.getCard() != null) {
                        this.originDragLabel = (CardLabel) e.getSource();
                        this.originDragLabel.setVisible(false);
                        Toolkit toolkit = Toolkit.getDefaultToolkit();
                        Image image = iconToImage(this.originDragLabel
                                .getIcon());
                        Cursor c = toolkit.createCustomCursor(image, new Point(
                                this.mainFrame.getX(), this.mainFrame.getY()),
                                "img");
                        this.mainFrame.setCursor(c);
                        // Reset targeting parameters
                        this.targeting = 0;
                        this.cardsSelectedNb = 0;
                        this.selectedCards.clear();
                    }
                }
            }
        }
        // Targeting session
        if (this.targeting != 0) {
            if (this.targeting == 1) {
                // Select card as target
                if (e.getSource() instanceof CardLabel
                        && e.getButton() == 1
                        && ((JComponent) e.getSource()).getParent()
                                instanceof PlayerBoardEnemyBoard
                        && !((CardLabel) e.getSource()).isTargeted()) {
                    if (this.secondTarget) {
                        // System.out.println("SecondTarget");
                        // System.out.println("Add target : "
                        // + ((CardLabel) e.getSource()).getCard()
                        // .getClass().getSimpleName());
                        this.selectedCards.add(((CardLabel) e.getSource())
                                .getCard());
                        ((CardLabel) e.getSource()).setTargeted(true);

                        this.cardsSelectedNb++;
                        // System.out.println("Update hocus : "
                        // + this.mainFrame.client.getHocusPlayed()
                        // .getClass().getSimpleName());
                        // for (int k = 0; k < this.selectedCards.size(); k++) {
                        // System.out.println("TargetList : "
                        // + this.selectedCards.get(k));
                        // }
                        ((CardTargeting) this.mainFrame.client.getHocusPlayed())
                                .setTargetedCards(this.selectedCards);
                    } else {
                        // si le joueur sélectionne une carte dans le meme
                        // grimoire
                        if (((CardTargeting) this.cardMemory)
                                .getTargetedPlayer() == null
                                || ((PlayerBoard) ((JComponent) e.getSource())
                                        .getParent()).getPlayer().getId()
                                            == ((CardTargeting)
                                                    this.cardMemory)
                                        .getTargetedPlayer().getId()) {
                            this.selectedCards.add(((CardLabel) e.getSource())
                                    .getCard());
                            ((CardLabel) e.getSource()).setTargeted(true);
                            ((CardTargeting) this.cardMemory)
                                    .setTargetedPlayer(
                                            ((PlayerBoard) ((JComponent) e
                                            .getSource()).getParent())
                                            .getPlayer());
                            this.cardsSelectedNb++;
                            if (this.cardsSelectedNb == this.originDragLabel
                                    .getCard().getPower() || ((CardTargeting) this.cardMemory)
                                    .getTargetedPlayer().getGrimoireCardsList().size() < this.originDragLabel
                                    .getCard().getPower()) {
                                ((CardTargeting) this.cardMemory)
                                        .setTargetedCards(this.selectedCards);
                                this.playCard();
                            }
                        } else {
                            JOptionPane
                                    .showMessageDialog(
                                            this.mainFrame,
                                            "Vous devez sélectionner les "
                                            + "cartes visées\nparmi les cartes "
                                            + "d'un même joueur",
                                            null, 0);
                        }
                    }
                }
            } else {
                // Select player as target
                if (e.getButton() == 1) {
                    // System.out.println("e.getSource().getParent() : "
                    // + ((JComponent) e.getSource()).getParent()
                    // .getClass().getSimpleName());
                    // System.out.println("e.getSource() : "
                    // + ((JComponent) e.getSource()).getClass()
                    // .getSimpleName());
                    if (((JComponent) e.getSource()).getParent() instanceof PlayerBoardEnemyBoard) {
                        // System.out.println("Here we are");
                        ((PlayerBoard) ((JComponent) e.getSource()).getParent())
                                .setTargeted(true);
                        ((PlayerTargeting) this.cardMemory)
                                .setTargetedPlayer(
                                        ((PlayerBoard) ((JComponent) e
                                        .getSource()).getParent()).getPlayer());
                        this.playCard();
                    } else {
                        if (((JComponent) e.getSource()) instanceof PlayerBoardEnemyBoard) {
                            // System.out.println("Nope we are here !");
                            ((PlayerBoard) e.getSource()).setTargeted(true);
                            ((PlayerTargeting) this.cardMemory)
                                    .setTargetedPlayer(
                                            ((PlayerBoard) ((JComponent) e
                                            .getSource())).getPlayer());
                            this.playCard();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        if (e.getButton() == 3) { // clic droit
            if (e.getSource() instanceof CardLabel
                    && ((CardLabel) e.getSource()).getCard() != null) {
                CardLabel pLab = (CardLabel) e.getSource();
                this.mainFrame.getBoardPan().getZoomPart()
                        .setZoomedCard(pLab.getCard());
            }
        }
        if (e.getButton() == 1 && this.targeting == 0) {
            if (this.originDragLabel != null) {
                // Release in GameZone
                this.cardMemory = this.originDragLabel.getCard();
                // Targeting test
                if (this.cardMemory.getClass().getSuperclass().getSimpleName()
                        .compareTo("CardTargeting") == 0
                        && this.lastEntered instanceof GameZone
                        && !this.grimoireToComplete) {
                    this.targeting = 1;
                    JOptionPane
                            .showMessageDialog(
                                    this.mainFrame,
                                    "Choisissez une carte dans le grimoire de "
                                    + "l'un de vos adversaire",
                                    null, 0);
                } else {
                    if (this.cardMemory.getClass().getSuperclass()
                            .getSimpleName().compareTo("PlayerTargeting") == 0
                            && this.lastEntered instanceof GameZone
                            && !this.grimoireToComplete) {
                        this.targeting = 2;
                        JOptionPane.showMessageDialog(this.mainFrame,
                                "Choisissez l'un de vos adversaire", null, 0);
                    } else {
                        this.playCard();
                    }
                }
                this.mainFrame.setCursor(Cursor
                        .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }

    private void playCard() {
        if (this.lastEntered instanceof GameZone && !this.grimoireToComplete) {
            if (this.mainFrame.client.playCard(this.originDragLabel.getCard())) {
                // Add Hocus card
                if (this.originDragLabel.getCard().isHocus()) {
                    this.mainFrame
                            .getBoardPan()
                            .getGamePart()
                            .getGameZone()
                            .addHocusCard(this.originDragLabel.getCard(),
                                    this.mainFrame.getWidth());
                    // Card played is a grimoire card
                    if (this.originDragLabel == this.mainFrame.getBoardPan()
                            .getGamePart().playerBoard.grim1
                            || this.originDragLabel
                                == this.mainFrame.getBoardPan()
                                    .getGamePart().playerBoard.grim2
                            || this.originDragLabel
                                == this.mainFrame.getBoardPan()
                                    .getGamePart().playerBoard.grim3) {
                        this.originDragLabel.setCard(null);
                    } else { // Card played is a hand card
                        this.mainFrame.getBoardPan().getGamePart().playerBoard
                                .removeHandCard(this.originDragLabel);
                    }
                    // Add Pocus card
                } else {
                    this.mainFrame
                            .getBoardPan()
                            .getGamePart()
                            .getGameZone()
                            .addPocusCard(this.originDragLabel.getCard(),
                                    this.mainFrame.getWidth());
                    // Card played is a grimoire card
                    if (this.originDragLabel == this.mainFrame.getBoardPan()
                            .getGamePart().playerBoard.grim1
                            || this.originDragLabel
                                == this.mainFrame.getBoardPan()
                                    .getGamePart().playerBoard.grim2
                            || this.originDragLabel
                                == this.mainFrame.getBoardPan()
                                    .getGamePart().playerBoard.grim3) {
                        this.originDragLabel.setCard(null);
                    } else { // Card played is a hand card
                        this.mainFrame.getBoardPan().getGamePart().playerBoard
                                .removeHandCard(this.originDragLabel);
                    }
                }
            } else {
                // play card return false, reset targets
                this.mainFrame.client.resetTargetStates();
                this.originDragLabel.setVisible(true);
            }
        } else {
            // Release in another PictureLabel in GameBoard
            if (this.onComponent != null
                    && this.onComponent.getParent()
                        == this.mainFrame.getBoardPan()
                            .getGamePart().playerBoard) {
                // Switch cards
                boolean ret = false;
                if (this.mainFrame.getBoardPan().getGamePart().playerBoard
                        .getHandCards().contains(this.originDragLabel)) {
                    ret = this.mainFrame.client.exchangeHandCardsWithGrimoire(
                            this.originDragLabel.getCard(),
                            this.onComponent.getCard());
                } else {
                    if (this.mainFrame.getBoardPan().getGamePart().playerBoard
                            .getHandCards().contains(this.onComponent)) {
                        ret = this.mainFrame.client
                                .exchangeHandCardsWithGrimoire(
                                        this.onComponent.getCard(),
                                        this.originDragLabel.getCard());
                    }
                }
                if (ret || this.grimoireToComplete) {
                    this.onComponent.setCard(this.originDragLabel
                            .switchCards(this.onComponent.getCard()));
                } else {
                    this.mainFrame.client
                            .displayMessage("Vous ne pouvez échanger les "
                                    + "cartes de votre grimoire\navec celle "
                                    + "de votre main que lorsque c'est votre "
                                    + "tour.");
                    this.originDragLabel.setVisible(true);
                }
            } else {
                // Release in something else
                this.originDragLabel.setVisible(true);
            }
        }
        try {
            Thread.sleep(30);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.grimoireToComplete = false;
        //System.out.println("Targeting : " + this.targeting);
        if (((this.mainFrame.getBoardPan().getGamePart().getMyPlayerBoard().grim1 != null && this.mainFrame
                .getBoardPan().getGamePart().getMyPlayerBoard().grim1.getCard() == null)
                || (this.mainFrame.getBoardPan().getGamePart()
                        .getMyPlayerBoard().grim2 != null && this.mainFrame
                        .getBoardPan().getGamePart().getMyPlayerBoard().grim2
                        .getCard() == null) || (this.mainFrame.getBoardPan()
                .getGamePart().getMyPlayerBoard().grim3 != null && this.mainFrame
                .getBoardPan().getGamePart().getMyPlayerBoard().grim3.getCard() == null))
                && (this.mainFrame.getBoardPan().getGamePart()
                        .getMyPlayerBoard().isCardInHand())) {
            this.mainFrame.client
                    .displayMessage("Vous devez compléter votre grimoire.");
            this.grimoireToComplete = true;
        }
        this.targeting = 0;
        this.originDragLabel = null;
        this.secondTarget = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (this.targeting == 0) {
            if (e.getComponent() instanceof GraphicPanel) {
                this.lastEntered = (JComponent) e.getComponent();
                this.onComponent = null;
            }
            if (e.getComponent() instanceof CardLabel) {
                this.onComponent = (CardLabel) e.getComponent();
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.mainFrame.getJMenuBar().setVisible(e.getYOnScreen() < 80);
    }

    /**
     * Function to change an Icon Object to an Image Object.
     *
     * @param icon : Icon
     * @return smth
     */
    private Image iconToImage(Icon icon) {
        if (icon instanceof ImageIcon) {
            return ((ImageIcon) icon).getImage();
        } else {
            BufferedImage image = new BufferedImage(icon.getIconWidth(),
                    icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
            icon.paintIcon(null, image.getGraphics(), 0, 0);
            return image;
        }
    }

    /**
     * setSecondTarget.
     * @param b : boolean
     */
    public void setSecondTarget(boolean b) {
        this.secondTarget = b;
        this.targeting = 1;
        this.cardsSelectedNb = 0;
        this.selectedCards.clear();
    }
}
