package fr.cemaj.hp.gui;

import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import fr.cemaj.hp.game.card.Card;

/**
 * This class allowed to see a list of cards in a popup.
 * It automatically resize cards pictures to be contained in the window.
 *
 * @author Jérémy Longin
 */
public class CardsVisualizer extends JPanel implements MouseListener {

    private static final long serialVersionUID = 1L;

    private CardLabel[] cardLabels;
    private CardLabel originDragLabel;
    private CardLabel draggedLabel;
    private CardLabel onReleasedLabel;

    /**
     * Class constructor.
     *
     * @param width : window max width
     */
    public CardsVisualizer(int width) {
        super();

        // Initialisation paramètres
        this.cardLabels = new CardLabel[4];

        // initialisation listener
        this.addMouseListener(this);
    }

    /**
     * Function call by the container to initialize the cards.
     *
     * @param tabCards : the cards to be displayed
     * @param cardLabelWidth : the width that the cards have to get
     * @param cardLabelHeight : the height that the cards have to get
     */
    public void reorganizeCards(Card[] tabCards, int cardLabelWidth,
            int cardLabelHeight) {
        // Initialisation Layout
        this.setLayout(new GridLayout(1, tabCards.length));
        for (int i = 0; i < tabCards.length; i++) {
            this.cardLabels[i] = new CardLabel(tabCards[i], cardLabelWidth,
                    cardLabelHeight);
            this.add(this.cardLabels[i]);
            this.cardLabels[i].addMouseListener(this);
        }
    }

    /**
     * getCards.
     * @return cardLabels
     */
    public CardLabel[] getCards() {
        return this.cardLabels;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getSource() instanceof CardLabel) {
            this.originDragLabel = (CardLabel) e.getSource();
            this.draggedLabel = new CardLabel(this.originDragLabel);
            this.originDragLabel.setCard(null);
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Image image = iconToImage(this.draggedLabel.getIcon());
            Cursor c = toolkit.createCustomCursor(image, new Point(this.getX(),
                    this.getY()), "img");
            this.setCursor(c);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getComponent() instanceof CardLabel
                && this.onReleasedLabel.getCard() != null) {
            if (this.draggedLabel != null) {
                this.onReleasedLabel.setCard(this.draggedLabel
                        .switchCards(this.onReleasedLabel.getCard()));
                this.originDragLabel.setCard(this.draggedLabel.getCard());
            }
        } else {
            this.originDragLabel.setCard(this.draggedLabel.getCard());
            this.originDragLabel = null;
            this.draggedLabel = null;
        }
        this.setCursor(Cursor
                .getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        this.originDragLabel = null;
        this.draggedLabel = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        if (e.getComponent() instanceof CardLabel) {
            this.onReleasedLabel = (CardLabel) e.getComponent();
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    /**
     * iconToImage.
     * @param icon (Icon)
     * @return a picture
     */
    private Image iconToImage(Icon icon) {
        if (icon instanceof ImageIcon) {
            return ((ImageIcon) icon).getImage();
        } else {
            BufferedImage image = new BufferedImage(icon.getIconWidth(),
                    icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
            icon.paintIcon(null, image.getGraphics(), 0, 0);
            return image;
        }
    }
}
