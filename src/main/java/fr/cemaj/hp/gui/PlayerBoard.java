package fr.cemaj.hp.gui;

import java.awt.Color;
import java.awt.FontFormatException;
import java.io.IOException;

import javax.swing.border.LineBorder;

import fr.cemaj.hp.game.Player;

/**
 * This abstract class represent a player board.
 * It's use to initiate gameBoard and have a list of
 * board as we have a list of player.
 *
 * @author Jérémy Longin
 */
public abstract class PlayerBoard extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    public CardLabel grim1;
    public CardLabel grim2;
    public CardLabel grim3;
    protected Player player;
    private PlayerBoard next;
    private PlayerBoard previous;
    private PictureLabel coin;

    /**
     * Class constructor.
     * @throws FontFormatException : exception
     * @throws IOException : exception
     */
    PlayerBoard() throws FontFormatException, IOException {
        super();
    }

    /**
     * Function to set the previous player board in game turn.
     *
     * @param playerBoard : the previous player board
     */
    public void setPrevious(PlayerBoard playerBoard) {
        this.previous = playerBoard;
    }

    /**
     * Function to set the next player board in game turn.
     *
     * @param playerBoard : the next player board
     */
    public void setNext(PlayerBoard playerBoard) {
        this.next = playerBoard;
        playerBoard.setPrevious(this);
    }

    /**
     * Previous board getter.
     *
     * @return previous board.
     */
    public PlayerBoard getPrevious() {
        return this.previous;
    }

    /**
     * Next board getter.
     *
     * @return next board.
     */
    public PlayerBoard getNext() {
        return this.next;
    }

    /**
     * Set associated coin.
     *
     * @param c : Coin label to associate to the board.
     */
    public void setCoin(PictureLabel c) {
        this.coin = c;
    }

    /**
     * Update associated Coin visibility.
     *
     * @param b : new visibility of the coin.
     */
    public void setCoinVisibility(boolean b) {
        this.coin.setVisible(b);
    }

    /**
     * Associated coin visibility getter.
     *
     * @return associated coin visibility.
     */
    public boolean getCoinVisibility() {
        return this.coin.isVisible();
    }

    /**
     * Associated player id getter.
     *
     * @return associated player id.
     */
    public int getPlayerId() {
        return this.player.getId();
    }

    /**
     * Associated player name getter.
     *
     * @return associated player name.
     */
    public String getPlayerName() {
        return this.player.getName();
    }

    /**
     * Set an associated player.
     *
     * @param p : the player to associate to the board.
     */
    public void setPlayer(Player p) {
        this.player = p;

        //initialisation grimoire
        this.grim1.setCard(this.player.getGrimoireCardsList().get(0));
        this.grim2.setCard(this.player.getGrimoireCardsList().get(1));
        this.grim3.setCard(this.player.getGrimoireCardsList().get(2));
    }

    /**
     * Method use to set target visibility.
     *
     * @param b : new visibily state
     */
    public void setTargeted(boolean b) {
        if (b) {
            this.setBorder(new LineBorder(Color.red, 3));
        } else {
            this.setBorder(null);
        }
    }

    /**
     * Method to reset every target to null.
     */
    public void resetTargets() {
        this.setTargeted(false);
        this.grim1.setTargeted(false);
        this.grim2.setTargeted(false);
        this.grim3.setTargeted(false);
    }

    /**
     * getPlayer.
     * @return Player
     */
    public Player getPlayer() {
        return this.player;
    }
}
