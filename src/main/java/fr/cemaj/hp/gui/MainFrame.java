package fr.cemaj.hp.gui;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

//import com.apple.eawt.Application;

import org.omg.CORBA.portable.InputStream;

import fr.cemaj.hp.Client;

/**
 * This class creates and contains all GUI elements : panels, popup... It also
 * contains a reference to Client class.
 * 
 * @author Jérémy Longin
 */
@SuppressWarnings("restriction")
public class MainFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    Client client;

    private MainMenu mainMenuPan;
    private LobbyPanel lobbyPan;
    private GameBoard boardPan;
    private RulesPanel rulesPan;
    private CreditPanel creditPan;
    private ResultPanel resultsPan;

    private JMenuBar menubar;
    private JMenu hpMenu, helpMenu;
    private JMenuItem createItem, joinItem, rulesItem, creditsItem,
            exitGameItem, exitAppItem;
    private JPanel visiblePan, lastVisiblePan;

    private CreateGamePopup creaPopup;
    private JoinGamePopup joinPopup;
    private CrystalBallPopup cristalBallPopup;

    private InterfaceListeners listener;

    private boolean isMac;

    /**
     * Class constructor : it updates the window to screen size and OS.
     * 
     * @param isMac
     *            : OS indicator
     * @param hp
     *            : a reference to Client class
     */
    @SuppressWarnings("restriction")
    public MainFrame(boolean isMac, Client hp) throws FontFormatException,
            IOException {
        super();

        this.client = hp;

        this.isMac = isMac;

        // Mise en place de la fenêtre
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int height, width = (int) screenSize.getWidth();
        if (isMac){
            height = (int) screenSize.getHeight() - 100;
            Image image = Toolkit.getDefaultToolkit().getImage("./res/img/logo.gif");
            //Application.getApplication().setDockIconImage(image);
        }else{
            height = (int) screenSize.getHeight();
            try {
                InputStream inputStream= (InputStream) ClassLoader.getSystemClassLoader().getResourceAsStream("./res/img/logo.gif");

              BufferedImage img = ImageIO.read(inputStream);
                final TrayIcon trayIcon = new TrayIcon(img, "Hocus Pocus by CEMAJ");
                SystemTray.getSystemTray().add(trayIcon);
            }catch (Exception e) {}
        }
        this.setSize(width, height);
        height = height - 20;

        // Affichage du splashScreen
        SlashPanel splash = new SlashPanel(this, width, height);
        this.add(splash);
        splash.setBounds(0, 0, width, height);
        splash.display();

        // this.setExtendedState(MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setResizable(false);

        // Création du listener
        this.listener = new InterfaceListeners(this);

        this.addMenu(this.listener);
        this.setLayout(null);

        // Mise en place background
        Image backgroundGeneral, backgroundGameBoard;
        backgroundGeneral = this.resize("./res/img/GeneralBackground.jpg",
                width, height).getImage();
        backgroundGameBoard = this.resize("./res/img/BoardBackground.png",
                width, height).getImage();

        // Création du menu principal
        this.mainMenuPan = new MainMenu(width, height, this.listener,
                backgroundGeneral);
        this.add(this.mainMenuPan);
        splash.setVisible(false);
        this.mainMenuPan.setVisible(true);
        this.mainMenuPan.setBounds(0, 0, width, height);
        this.visiblePan = this.mainMenuPan;

        // Création du panneau lobby
        this.lobbyPan = new LobbyPanel(width, height, this.listener,
                backgroundGeneral);
        this.add(this.lobbyPan);
        this.lobbyPan.setVisible(false);
        this.lobbyPan.setBounds(0, 0, width, height);

        // Création du plateau de jeu
        this.boardPan = new GameBoard(width, height, this.listener,
                backgroundGameBoard);
        this.add(this.boardPan);
        this.boardPan.setVisible(false);
        this.boardPan.setBounds(0, 0, width, height);

        // Création du panneau rules
        this.rulesPan = new RulesPanel(width, height, this.listener,
                backgroundGeneral);
        this.add(this.rulesPan);
        this.rulesPan.setVisible(false);
        this.rulesPan.setBounds(0, 0, width, height);

        // Création du panneau credits
        this.creditPan = new CreditPanel(width, height, this.listener,
                backgroundGeneral);
        this.add(this.creditPan);
        this.creditPan.setVisible(false);
        this.creditPan.setBounds(0, 0, width, height);

        // Création du panneau des résultats
        this.resultsPan = new ResultPanel(width, height, this.listener,
                backgroundGeneral);
        this.add(this.resultsPan);
        this.resultsPan.setVisible(false);
        this.resultsPan.setBounds(0, 0, width, height);

        // Ajout des listeners
        addMouseListener(this.listener.mouseListener);
        addMouseMotionListener(this.listener.mouseListener);
        addKeyListener(this.listener.keyListener);

        // initialisation des popups
        creaPopup = new CreateGamePopup(this, listener);
        joinPopup = new JoinGamePopup(this, listener);
        this.cristalBallPopup = new CrystalBallPopup(this, width);
        // Test cristalBallPopup
        /*
         * Card[] cards = new Card[4]; cards[0]= new Inspiration(2); cards[1]=
         * new MagicStaff(); cards[2]= new CrystalBall(); cards[3]= new
         * BlackCat(); cristalBallPopup.reorganizeCards(1, cards);
         */
        // Fin test cristalBallPopup
    }

    /**
     * Function that create the menus
     * 
     * @param listener
     *            : all GUI listeners
     */
    private void addMenu(InterfaceListeners listener) {

        if (isMac) {
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            System.setProperty(
                    "com.apple.mrj.application.apple.menu.about.name", "SSA");
        }

        // Création du menu HocusPocus
        this.menubar = new JMenuBar();
        this.setJMenuBar(this.menubar);
        this.menubar.setVisible(false);
        this.hpMenu = new JMenu("HocusPocus");
        this.menubar.add(this.hpMenu);

        this.createItem = new JMenuItem("Créer une partie");
        this.createItem.addActionListener(listener.actionListener);
        this.hpMenu.add(this.createItem);

        this.joinItem = new JMenuItem("Rejoindre une partie");
        this.joinItem.addActionListener(listener.actionListener);
        this.hpMenu.add(this.joinItem);

        this.exitGameItem = new JMenuItem("Quitter la partie");
        this.exitGameItem.addActionListener(listener.actionListener);
        this.exitGameItem.setEnabled(false);
        this.hpMenu.add(this.exitGameItem);

        this.exitAppItem = new JMenuItem("Quitter le jeu");
        this.exitAppItem.addActionListener(listener.actionListener);
        this.hpMenu.add(exitAppItem);

        // Création du menu aide
        this.helpMenu = new JMenu("?");
        this.rulesItem = new JMenuItem("Règles du jeu");
        this.rulesItem.addActionListener(listener.actionListener);
        this.helpMenu.add(this.rulesItem);
        this.creditsItem = new JMenuItem("A propos");
        this.creditsItem.addActionListener(listener.actionListener);
        this.helpMenu.add(this.creditsItem);
        this.menubar.add(this.helpMenu);

        if (isMac) {
            this.menubar.setVisible(true);
        }
    }

    /**
     * Function to change the displayed panel
     * 
     * @param g
     *            : new panel to display
     */
    public void changePanel(GraphicPanel g) {
        this.lastVisiblePan = this.visiblePan;
        this.visiblePan = g;
        this.lastVisiblePan.setVisible(false);
        this.visiblePan.setVisible(true);
        if (this.visiblePan instanceof MainMenu) {
            this.lastVisiblePan = this.visiblePan;
        }
        updateMenuItemsVisibility();
        this.requestFocusInWindow();
    }

    /**
     * Function to get back to the last panel before the active one.
     */
    public void backToLastPanel() {
        this.visiblePan.setVisible(false);
        this.visiblePan = this.lastVisiblePan;
        this.visiblePan.setVisible(true);
        updateMenuItemsVisibility();
        this.requestFocusInWindow();
    }

    /**
     * Function to update menu items visibility according to user
     * actions on GUI.
     */
    private void updateMenuItemsVisibility() {
        // TODO Auto-generated method stub
        // System.out.println("Here we are - visiblePan : " +
        // this.visiblePan.getClass().getSimpleName() + " - lastVisiblePan : " +
        // this.lastVisiblePan.getClass().getSimpleName());
        if (this.visiblePan instanceof GameBoard
                || this.lastVisiblePan instanceof GameBoard
                || this.visiblePan instanceof LobbyPanel
                || this.lastVisiblePan instanceof LobbyPanel) {
            this.createItem.setEnabled(false);
            this.joinItem.setEnabled(false);
            this.rulesItem.setEnabled(true);
            this.creditsItem.setEnabled(true);
            this.exitGameItem.setEnabled(true);
            this.exitAppItem.setEnabled(true);
            if (this.visiblePan instanceof RulesPanel
                    || this.visiblePan instanceof CreditPanel) {
                this.rulesItem.setEnabled(false);
                this.creditsItem.setEnabled(false);
            }
        } else {
            if (this.visiblePan instanceof ResultPanel) {
                this.createItem.setEnabled(false);
                this.joinItem.setEnabled(false);
                this.rulesItem.setEnabled(false);
                this.creditsItem.setEnabled(false);
                this.exitGameItem.setEnabled(false);
                this.exitAppItem.setEnabled(false);
            } else {
                this.createItem.setEnabled(true);
                this.joinItem.setEnabled(true);
                this.rulesItem.setEnabled(true);
                this.creditsItem.setEnabled(true);
                this.exitGameItem.setEnabled(false);
                this.exitAppItem.setEnabled(true);
                if (this.visiblePan instanceof RulesPanel
                        || this.visiblePan instanceof CreditPanel) {
                    this.rulesItem.setEnabled(false);
                    this.creditsItem.setEnabled(false);
                }
            }
        }
    }

    /**
     * Function to resize a picture (Same in PictureLabel class)
     * 
     * @param filePath : the path of picture file.
     * @param newWidth : the new width of the picture.
     * @param newHeight : the new height of the picture.
     * @return an ImageIcon of the picture with the new size.
     */
    public ImageIcon resize(String filePath, int newWidth, int newHeight) {
        int type = BufferedImage.TYPE_INT_ARGB;

        Image originalImage = new ImageIcon(filePath).getImage();
        ImageIcon returnedImage;

        BufferedImage resizedImage = new BufferedImage(newWidth, newHeight,
                type);
        Graphics2D g = resizedImage.createGraphics();

        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.drawImage(originalImage, 0, 0, newWidth, newHeight, this);
        g.dispose();

        returnedImage = new ImageIcon(resizedImage);
        return returnedImage;
    }

    /**
     * Getters
     */
    
    public CreateGamePopup getCreaPopup() {
        return creaPopup;
    }

    public JoinGamePopup getJoinPopup() {
        return joinPopup;
    }

    public LobbyPanel getLobbyPanel() {
        return this.lobbyPan;
    }

    public MainMenu getMainMenuPan() {
        return mainMenuPan;
    }

    public LobbyPanel getLobbyPan() {
        return lobbyPan;
    }

    public GameBoard getBoardPan() {
        return boardPan;
    }

    public RulesPanel getRulesPan() {
        return rulesPan;
    }

    public CreditPanel getCreditPan() {
        return creditPan;
    }

    public ResultPanel getResultsPan() {
        return resultsPan;
    }

    public JMenuBar getMenubar() {
        return menubar;
    }

    public JMenu getHpMenu() {
        return hpMenu;
    }

    public JMenu getHelpMenu() {
        return helpMenu;
    }

    public JMenuItem getCreateItem() {
        return createItem;
    }

    public JMenuItem getJoinItem() {
        return joinItem;
    }

    public JMenuItem getRulesItem() {
        return rulesItem;
    }
    public JMenuItem getCreditsItem() {
        return creditsItem;
    }

    public JMenuItem getExitGameItem() {
        return exitGameItem;
    }

    public JMenuItem getExitAppItem() {
        return exitAppItem;
    }

    public JPanel getVisiblePan() {
        return visiblePan;
    }

    public JPanel getLastVisiblePan() {
        return lastVisiblePan;
    }

    public InterfaceListeners getListener() {
        return listener;
    }
    
    public boolean isMac() {
        return isMac;
    }

    public void setMac(boolean isMac) {
        this.isMac = isMac;
    }

    public CrystalBallPopup getCristalBallPopup() {
        return cristalBallPopup;
    }
}
