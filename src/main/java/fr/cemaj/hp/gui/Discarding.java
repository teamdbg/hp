package fr.cemaj.hp.gui;

import java.awt.Dimension;
import java.awt.FontFormatException;
import java.awt.Image;
import java.io.IOException;

import fr.cemaj.hp.game.card.Card;

/**
 * Discarding class.
 */
public class Discarding extends GraphicPanel {

    private static final long serialVersionUID = 1L;
    private CardLabel lastCardPlayed;
    private int cardWidth;
    private int cardHeight;

    /**
     * Class constructor.
     *
     * @param img : picture background for the panel
     * @param cardSize (Dimension) a card size
     * @throws FontFormatException : an exception
     * @throws IOException : an exception
     */
    public Discarding(Image img, Dimension cardSize)
            throws FontFormatException, IOException {
        super(img);

        this.cardWidth = (int) cardSize.getWidth();
        this.cardHeight = (int) cardSize.getHeight();
    }

    /**
     * Method to set the last card played on the discard.
     *
     * @param c (Card)
     */
    public void setDiscard(Card c) {
        this.lastCardPlayed = new CardLabel(c, this.cardWidth, this.cardHeight);
        this.add(this.lastCardPlayed);
        this.lastCardPlayed.setBounds((int) ((double) this.cardWidth * 0.3),
                (int) ((double) this.cardWidth * 0.16),
                this.cardWidth,
                this.cardHeight);
        this.lastCardPlayed.setOpaque(false);
    }
}
