package fr.cemaj.hp.gui;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JTextArea;

import fr.cemaj.hp.game.card.Card;

/**
 * This class represents the zoom part of the game board.
 * It contains elements used to display a zoom of a card.
 *
 * @author Jérémy Longin
 */
public class GameBoardZoomPart extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    CardLabel picture;
    JTextArea rules;

    /**
     * Class constructor.
     *
     * @param width : the width of the main frame.
     * @param height : the height of the main frame.
     * @param listener : all GUI listeners.
     * @throws FontFormatException : exception
     * @throws IOException : exception
     */
    GameBoardZoomPart(int width, int height, InterfaceListeners listener)
            throws FontFormatException, IOException {
        super();
        addMouseListener(listener.mouseListener);
        addMouseMotionListener(listener.mouseListener);

        // Paramétrage général du panneau
        this.setLayout(null);

        this.picture = new CardLabel("Clic droit pour afficher ici");
        this.rules = new JTextArea();

        this.add(this.picture);
        this.add(this.rules);

        int zoomSize = (int) ((double) width * 0.2285);
        this.picture.setBounds(0, 0, (int) ((double) width * 0.156), zoomSize);
        this.rules.setBounds(0, zoomSize, (int) ((double) width * 0.156),
                height - zoomSize);

        this.paramElement(this.picture, 6, width);
        this.picture.setFont(new Font("Arial", Font.ITALIC,
                (int) ((double) width * 0.01)));
        // picture.setOpaque(false);
        this.paramElement(this.rules, 6, width);
        this.rules.setEditable(false);
        // rules.setOpaque(false);
    }

    /**
     * Function to change the zoomed card.
     *
     * @param c : the new card to display in the zoom zone.
     */
    void setZoomedCard(Card c) {
        this.picture.setText(null);
        this.picture.setCard(c);
        BufferedReader file;
        String line;
        String text;
        // Chargement des règles
        String rulesFilePath = "./res/cardsRules/"
                + c.getClass().getSimpleName() + ".txt";
        try {
            file = new BufferedReader(new FileReader(new File(rulesFilePath)));
            text = "";
            do {
                line = file.readLine();
                if (line != null) {
                    text = text + line;
                }
            } while (line != null);
            this.rules.setLineWrap(true);
            this.rules.setWrapStyleWord(true);
            this.rules.setText(text);
            file.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
