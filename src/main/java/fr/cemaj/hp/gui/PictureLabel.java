package fr.cemaj.hp.gui;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * This class represents a Label which\
 * the main goal is to display a picture.
 *
 * @author Jérémy Longin
 */
public class PictureLabel extends JLabel {

    private static final long serialVersionUID = 1L;
    String pictureFilePath;

    /**
     * Class constructor.
     */
    PictureLabel() {
        super();
        this.setVisible(true);
    }

    /**
     * Class constructor.
     *
     * @param s : text to display on the label
     */
    PictureLabel(String s) {
        this();
        this.setText(s);
    }

    /**
     * Method to set an icon to the label.
     *
     * @param img : file path of the picture to display as icon.
     */
    public void setPicture(String img) {
        this.setIcon(this.resize(img,
                this.getSize().width,
                this.getSize().height));
    }

    /**
     * Function to resize a picture (Same in MainFrame class).
     *
     * @param filePath : the path of picture file.
     * @param newWidth : the new width of the picture.
     * @param newHeight : the new height of the picture.
     * @return an ImageIcon of the picture with the new size.
     */
    public ImageIcon resize(String filePath, int newWidth, int newHeight) {
        int type = BufferedImage.TYPE_INT_ARGB;

        Image originalImage = new ImageIcon(filePath).getImage();
        ImageIcon returnedImage;

        BufferedImage resizedImage = new BufferedImage(newWidth,
                newHeight,
                type);
        Graphics2D g = resizedImage.createGraphics();

        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g.drawImage(originalImage, 0, 0, newWidth, newHeight, this);
        g.dispose();

        returnedImage = new ImageIcon(resizedImage);
        return returnedImage;
    }
}
