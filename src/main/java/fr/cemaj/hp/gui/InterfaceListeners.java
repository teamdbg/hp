package fr.cemaj.hp.gui;

/**
 * This class contains all listener classes.
 *
 * @author Jérémy Longin
 */
public class InterfaceListeners {

    public InterfaceActionListener actionListener;
    public InterfaceKeyListener keyListener;
    public InterfaceMouseListener mouseListener;

    MainFrame mainFrame;

    /**
     * Class constructor.
     *
     * @param mf : a reference to the main frame
     */
    InterfaceListeners(MainFrame mf) {

        this.mainFrame = mf;

        this.actionListener = new InterfaceActionListener(this.mainFrame);
        this.keyListener = new InterfaceKeyListener(this.mainFrame);
        this.mouseListener = new InterfaceMouseListener(this.mainFrame);
    }
}
