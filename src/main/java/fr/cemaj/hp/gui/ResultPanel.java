package fr.cemaj.hp.gui;

import java.awt.FontFormatException;
import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;

import fr.cemaj.hp.game.Player;

/**
 * This class represents the game results panel.
 *
 * @author Jérémy Longin
 */
public class ResultPanel extends GraphicPanel {

    private static final long serialVersionUID = 1L;

    JButton returnBut;
    PlayerResultPanel[] players;

    /**
     * Class constructor.
     *
     * @param width : the width of the main frame
     * @param height : the height of the main frame
     * @param listener : a reference to all listeners
     * @param picture : the picture displayed in background
     * @throws FontFormatException : exception
     * @throws IOException : exception
     */
    ResultPanel(int width, int height, InterfaceListeners listener,
            Image picture) throws FontFormatException, IOException {
        super(picture);

        this.addMouseListener(listener.mouseListener);
        this.addMouseMotionListener(listener.mouseListener);

        this.setLayout(null);

        // Mise en forme bouton retour
        this.returnBut = new JButton("Retour");
        this.add(this.returnBut);
        this.returnBut.setBounds((int) ((double) width * 0.85),
                (int) ((double) height * 0.8), (int) ((double) width * 0.12),
                (int) ((double) height * 0.1));
        this.returnBut.addActionListener(listener.actionListener);
        this.paramElement(this.returnBut, 6, width);

        int winnerWidth = (int) ((double) width * 0.4);
        int winnerHeight = (int) ((double) height * 0.15);
        int otherWidth = (int) ((double) width * 0.3);
        int otherHeight = (int) ((double) height * 0.12);
        int yPosition = (int) ((double) height * 0.1);

        // Init tab
        this.players = new PlayerResultPanel[6];
        for (int i = 0; i < 6; i++) {
            if (i == 0) {
                this.players[i] = new PlayerResultPanel(width, winnerWidth,
                        winnerHeight, i);
                this.players[i].setBounds(width / 2 - winnerWidth / 2,
                        yPosition, winnerWidth, winnerHeight);
            } else {
                this.players[i] = new PlayerResultPanel(width, otherWidth,
                        otherHeight, i);
                this.players[i].setBounds(width / 2 - otherWidth / 2, yPosition
                        + winnerHeight + 10 + ((otherHeight + 10) * (i - 1)),
                        otherWidth, otherHeight);
            }
            this.add(this.players[i]);
            this.paramElement(this.players[i], i, width);
        }
    }

    /**
     * Function to initialize players.
     * @param tabPlayers a
     */
    public void initPlayers(Object[] tabPlayers) {
        int currentPos = 0;
        ArrayList<Player> players = new ArrayList<Player>();
        //Change tab to list
        for (int i = 0; i < tabPlayers.length; i++) {
            players.add((Player) (tabPlayers[i]));
        }
        while (!players.isEmpty()) {
            int maxGem = -1;
            int maxId = -1;
            for (int j = 0; j < players.size(); j++) {
                if (maxGem < players.get(j).getGems()) {
                    maxGem = players.get(j).getGems();
                    maxId = j;
                }
            }
            // Max find - Add player
            this.players[currentPos].setPlayer(players.get(maxId));
            currentPos++;
            // Remove player from list
            players.remove(maxId);
        }
    }
}
