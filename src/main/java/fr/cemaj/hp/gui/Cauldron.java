package fr.cemaj.hp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * This class represents the cauldron in the GUI.
 * It's a panel (GraphicPanel) to can display the
 * number of gems above the cauldron picture.
 *
 * @author Jérémy Longin
 */
public class Cauldron extends GraphicPanel {
    private static final long serialVersionUID = 1L;

    private int gemNumber;
    private JLabel gemNbLabel;

    /**
     * Class constructor.
     * @param img (String)
     * @param width (int)
     * @param height (int)
     * @param listener (InterfaceListeners)
     * @throws FontFormatException : exception font
     * @throws IOException : socket exception
     */
    public Cauldron(String img, int width, int height,
            InterfaceListeners listener) throws FontFormatException,
            IOException {
        super(new PictureLabel().resize(img, width, height).getImage());

        this.setLayout(new GridLayout(1, 1));
        //Creation du label nombre de gem
        this.gemNbLabel = new JLabel("x30");
        this.gemNbLabel.setForeground(Color.white);
        this.gemNbLabel.setFont(this.cemajFont.deriveFont(Font.ITALIC,
                (int) ((double) width * 0.3)));
        this.gemNbLabel.setVerticalAlignment(SwingConstants.CENTER);
        this.gemNbLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(this.gemNbLabel);
        this.repaint();

        //Ajout des listeners
        this.addMouseListener(listener.mouseListener);
        this.gemNbLabel.addMouseListener(listener.mouseListener);
    }

    /**
     * Update the gemNumber value and the value displayed on the GUI.
     *
     * @param gemNb : the new gem number
     */
    public void setGemNumber(int gemNb) {
        this.gemNumber = gemNb;
        this.gemNbLabel.setText("x" + this.gemNumber);
        this.setOpaque(false);
        this.repaint();
    }

    /**
     * getGemNbLabel.
     * @return a JLabel
     */
    public JLabel getGemNbLabel() {
        return this.gemNbLabel;
    }

    /**
     * setGemNbLabel.
     * @param gemNbLabel (JLabel)
     */
    public void setGemNbLabel(JLabel gemNbLabel) {
        this.gemNbLabel = gemNbLabel;
    }

    /**
     *  getGemNumber.
     * @return an int
     */
    public int getGemNumber() {
        return this.gemNumber;
    }
}
