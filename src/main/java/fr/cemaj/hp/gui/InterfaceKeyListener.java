package fr.cemaj.hp.gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * This class contains actions for keys in all GUI.
 *
 * @author Jérémy Longin
 */
public class InterfaceKeyListener implements KeyListener {

    MainFrame mainFrame;

    /**
     * Class constructor.
     * @param mf MainFrame
     */
    InterfaceKeyListener(MainFrame mf) {
        this.mainFrame = mf;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void keyPressed(KeyEvent e) {
        // TODO Auto-generated method stub
        // RulesPanel events
        if (this.mainFrame.getVisiblePan() == this.mainFrame.getRulesPan()) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                this.mainFrame.getRulesPan().previousPages();
            }
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                this.mainFrame.getRulesPan().nextPages();
            }
        }
        // Popups events
        if (this.mainFrame.getVisiblePan() == this.mainFrame.getMainMenuPan()) {
            if (this.mainFrame.getCreaPopup().isVisible()) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    this.mainFrame.getCreaPopup().validation();
                }
            }
            if (this.mainFrame.getJoinPopup().isVisible()) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    this.mainFrame.getJoinPopup().validation();
                }
            }
        }
        // MainMenu events
        if (this.mainFrame.getVisiblePan() == this.mainFrame.getMainMenuPan()) {
            if(e.isControlDown()) {
                if (e.getKeyCode() == KeyEvent.VK_C) {
                    this.mainFrame.getCreaPopup().setVisible(true);
                }
                if (e.getKeyCode() == KeyEvent.VK_J) {
                    this.mainFrame.getJoinPopup().setVisible(true);
                }
                if (e.getKeyCode() == KeyEvent.VK_R) {
                    this.mainFrame.changePanel(this.mainFrame.getRulesPan());
                }
                if (e.getKeyCode() == KeyEvent.VK_Q) {
                    this.mainFrame.changePanel(this.mainFrame.getCreditPan());
                }
                if (e.getKeyCode() == KeyEvent.VK_A) {
                    System.exit(0);
                }
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub
    }
}
