package fr.cemaj.hp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import fr.cemaj.hp.game.Player;
import fr.cemaj.hp.game.ServerEnvironment;
import fr.cemaj.hp.net.ClientProxy;

/**
 * Server : the class that listens to incoming connections.
 * @author Mathieu Degaine,
 * Adrien Faure,
 * Éric Gillet,
 * Jérémy Longin,
 * Clémence Lop
 */
public class Server implements Runnable {

    public static final int VERSION = 0;
    private ServerEnvironment env;
    private ServerSocket socket;
    private int pCount = 0;

    /**
     * Server class main function.
     * @param args (String[])
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("HP server requires 2 arguments");
            System.exit(-1);
        }

        final Server s = new Server(Integer.valueOf(args[0]),
                "quick".equals(args[1]) ? true : false);

        final Thread serverThread = new Thread(s);

        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run() {
                System.out.println("SIGINT Received, stopping game");
                s.endGame();
            }
        });

        serverThread.start();

        try {
            serverThread.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Server Constructor.
     * @param port : the listening port
     * @param quick : the game mode
     */
    public Server(int port, boolean quick) {
        this.env = new ServerEnvironment(quick, this);
        try {
            this.socket = new ServerSocket(port);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        Socket so;
        Player p;
        // Accept new clients until the game is launched.
        while (!this.env.isInGame() && !this.socket.isClosed()) {
            try {
                so = this.socket.accept();
                p = new Player(this.pCount++);
                System.out.println(String.format(
                        "New connection accepted, player %d", this.pCount));
                this.env.addPlayer(p, new ClientProxy(this.env, so, p));
            } catch (IOException e) {
                if (this.env.isInGame()) {
                    break;
                }
            }
        }
        try {
            this.socket.close();
        } catch (IOException e) {
            System.out.println("Socket close error : ");
            e.printStackTrace();
        }
    }

    /**
     * endGame : called when the game end.
     */
    protected void endGame() {
        try {
            this.socket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.env.endGame();
    }

    /**
     * stopAccepting : stop to listen to incoming connections.
     */
    public void stopAccepting() {
        try {
            this.socket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
