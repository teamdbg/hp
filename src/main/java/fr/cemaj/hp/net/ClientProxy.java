package fr.cemaj.hp.net;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import fr.cemaj.hp.game.Player;
import fr.cemaj.hp.game.ServerEnvironment;
import fr.cemaj.hp.game.card.Card;

/**
 * Interface between the server and client through network.
 */
public class ClientProxy extends Proxy {

    private Socket socket;
    private ObjectOutputStream output;
    private ServerEnvironment game;
    private Player p;
    private Thread receivingThread;

    /**
     * ClientProxy Constructor.
     *
     * @param se (ServerEnv)
     * @param s
     *            (Socket)
     * @param player
     *            (Player)
     */
    public ClientProxy(ServerEnvironment se, Socket s, Player player) {
        this.socket = s;
        this.game = se;
        this.p = player;

        try {
            this.output = new ObjectOutputStream(s.getOutputStream());
        } catch (IOException e) {
            // TODO Verify behavior
            dropPlayer();
        }
        this.runThread();
    }

    /**
     * Tell the client the Player p state (ready or not).
     *
     * @param p
     *            (Player) : the player that is ready
     * @param state
     *            (Boolean) : the player's state
     */
    public void playerReady(Player p, boolean state) {
        this.send(new Action(Order.READY, p, state));
    }

    /**
     * Send data throughout the network.
     * @param action (Action)
     */
    public void send(Action action) {
        try {
            // Objects are cached by ObjectOutputStream/ObjectIntputStream,
            // this
            // call reset the cache to allow modified objects to be sent.
            this.output.reset();

            this.output.writeObject(action);
            this.output.flush();
        } catch (IOException e) {
            dropPlayer();
        }
    }

    /**
     * Execute an action on ServerEnvironment.
     *
     * @param a
     *            (Action) Action to perform
     */
    protected void execute(Action a) {
        Player player;
        switch (a.getType()) {
        case EHLO:
            welcomeNewClient(a);
            break;

        case READY:
            boolean state = (boolean) a.getObjs(0);
            this.game.setReadyStatement(this.p, state);
            break;

        case PLAYCARD:
            Card c = (Card) a.getObjs(0);
            this.game.playCard(c);
            break;

        case COMPLETEGRIMOIRE:
            Card card = (Card) a.getObjs(0);
            this.game.completeGrimoire(card);
            break;

        case EXCHANGEHANDGRIMOIRE:
            Card handCard = (Card) a.getObjs(0);
            Card grimoireCard = (Card) a.getObjs(1);
            this.game.exchangeHandCardsWithGrimoire(handCard, grimoireCard);
            break;

        case CRYSTAL:
            @SuppressWarnings("unchecked")
            ArrayList<Card> stack = (ArrayList<Card>) a.getObjs(0);
            this.game.finishCardReorder(stack);
            break;

        case CHANGETURN:
            boolean pickGem = (boolean) a.getObjs(0);
            this.game.endPlayerTurn(pickGem);
            break;

        case PLAYERLEFT:
            player = (Player) a.getObjs(0);
            this.game.removePlayer(player, true);
            break;

        case PLAYERLOST:
            player = (Player) a.getObjs(0);
            this.game.removePlayer(player, false);
            break;

        default:
            System.err.println("Bad action received, check version");
            break;
        }
    }

    /**
     * Launching the receiving thread.
     */
    protected void runThread() {
        this.receivingThread = new Thread(new ReceivingThread(this,
                this.socket));
        this.receivingThread.start();
    }

    /**
     * Handling a connection loss.
     */
    public void dropPlayer() {
        System.err.println(String.format("Lost connection to player %d",
                this.p.getId()));
        this.game.removePlayer(this.p, false);
        try {
            this.output.close();
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sending to a freshly connected client its Player id.
     *
     * @param a
     *            (Action)
     */
    private void welcomeNewClient(Action a) {
        this.p.setName(((Player) a.getObjs(0)).getName());
        this.send(new Action(Order.EHLO, this.p, this.game.isQuickGame()));
        // this.send(new Action(Order.PLAYER, p));
        // Player join complete, notify other players
        this.game.notifyNewPlayer(this.p);

    }

    /**
     * Sending a Player object.
     *
     * @param player
     *            (Player) : the layer to send
     */
    public void sendPlayer(Player player) {
        Action a = new Action(Order.PLAYER, player);
        this.send(a);
    }

    /**
     * Sending the played cards list.
     *
     * @param playedCards
     *            (ArrayList<Card>) : the cards list
     */
    public void sendPlayedCards(ArrayList<Card> playedCards) {
        Action a = new Action(Order.PLAYEDCARDS, playedCards);
        this.send(a);
    }

    /**
     * Sending the number of gems in the cauldron.
     *
     * @param cauldron
     *            (Integer)
     */
    public void sendCauldron(int cauldron) {
        Action a = new Action(Order.CAULDRON, cauldron);
        this.send(a);
    }

    /**
     * Sending the discard.
     *
     * @param discard
     *            (ArrayList<Card>)
     */
    public void sendDiscard(ArrayList<Card> discard) {
        Action a = new Action(Order.DISCARD, discard);
        this.send(a);
    }

    /**
     * Game Initialization.
     *
     * @param players
     *            (HashMap<Integer, Player> players) : the list of participants
     * @param cauldron
     *            (Integer) : the number of gems in the cauldron
     */
    public void initGame(HashMap<Integer, Player> players, int cauldron) {
        Action a = new Action(Order.INITGAME, players, cauldron);
        this.send(a);
    }

    /**
     * Changing the current player.
     *
     * @param currentPlayer
     *            (Integer) : the current player ID
     */
    public void changePlayerTurn(int currentPlayer) {
        Action a = new Action(Order.CHANGETURN, currentPlayer);
        this.send(a);
    }

    /**
     * Sending the 4 first cards of the stack, used when a Crystal Card is
     * played.
     *
     * @param stack
     *            (ArrayList<Card>) : the list of the 4 cards
     */
    public void sendStack(ArrayList<Card> stack) {
        this.send(new Action(Order.CRYSTAL, stack));
    }

    /**
     * Sending before the final card resolution.
     */
    public void endSession() {
        this.send(new Action(Order.ENDSESSION));
    }

    /**
     * Say the client the game is finished.
     */
    public void endGame() {
        this.send(new Action(Order.ENDGAME));
    }

    /**
     * playerJoined.
     * @param p2 (Player)
     */
    public void playerJoined(Player p2) {
        this.send(new Action(Order.NEWPLAYER, p2));
    }

    /**
     * Say the client a player left the game.
     * @param p Player
     */
    public void playerLeft(Player p) {
        this.send(new Action(Order.PLAYERLEFT, p));
    }

    /**
     * Say the client a player lost his connection.
     * @param p Player
     */
    public void playerLost(Player p) {
        this.send(new Action(Order.PLAYERLOST, p));
    }
}
