package fr.cemaj.hp.net;

import java.io.Serializable;

/**
 *
 * Class is used to communicate between a player and the server.
 * Contains the action type and the associated parameters.
 *
 * @author Adrien Faure, Éric Gillet
 *
 */
public class Action implements Serializable {

    private static final long serialVersionUID = -8282363267762918511L;
    private Order type;
    private Object[] objs;

    /**
     * Action constructor.
     *
     * @param o : enum containing the action to perform.
     * @param objects : (ellipsis) \
     * list of objects needed to execute the action o.
     */
    public Action(Order o, Serializable ...objects) {
        this.type = o;
        this.objs = objects;
    }

    /**
     * @return action
     */
    public Order getType() {
        return this.type;
    }

    /**
     * @param o : the action type
     */
    public void setType(Order o) {
        this.type = o;
    }

    /**
     * @param index (int)
     * @return obj (Object)
     */
    public Object getObjs(int index) {
        return this.objs[index];
    }

    /**
     * @param objects : (ellipsis) \
     * list of objects needed to execute the action o.
     */
    public void setObjs(Object ...objects) {
        this.objs = objects;
    }

}
