package fr.cemaj.hp.net;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import fr.cemaj.hp.game.Player;
import fr.cemaj.hp.game.PlayerEnvironment;
import fr.cemaj.hp.game.card.Card;

/**
 * @author A.F. ServerProxy.
 */
public class ServerProxy extends Proxy {

    public static final int VERSION = 0;

    private Socket socket;
    private ObjectOutputStream output;
    private PlayerEnvironment playerGame;
    private Player player;

    /**
     * Class constructor.
     * @param s (Socket) : Socket used to communicate with the server
     * @param game (GameEnvironment)
     * @param p (Player)
     */
    public ServerProxy(PlayerEnvironment game, Socket s, Player p) {
        this.socket = s;
        this.playerGame = game;
        this.player = p;
        try {
            this.output = new ObjectOutputStream(this.socket.getOutputStream());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.runThread();
    }

    /**
     * Launch the ReceivingThread associated to the ServerProxy.
     */
    protected void runThread() {
        this.receivingThread = new Thread(
                new ReceivingThread(this, this.socket));
        this.receivingThread.start();

        this.send(new Action(Order.EHLO, this.player));
    }

    /**
     * Send data throughout the network.
     */
    private void send(Action action) {
        try {
            // Objects are cached by ObjectOutputStream/ObjectIntputStream, this
            // call reset the cache to allow modified objects to be sent.
            this.output.reset();

            this.output.writeObject(action);
            this.output.flush();
        } catch (IOException e) {
            dropPlayer();
        }
    }

    /**
     * Called by the ClientProxy when a query is received.
     *
     * @param a : Action object containing the action type and the \
     * associated objects
     */
    void execute(Action a) {
        Player player;
        switch (a.getType()) {
        case EHLO:
            System.out.println("Connection accepted");
            this.playerGame.setPlayerId(((Player) a.getObjs(0)).getId());
            this.playerGame.setGameMode((boolean) a.getObjs(1));
            break;

        case PLAYER:
            this.playerGame.receivePlayer((Player) a.getObjs(0));
            break;

        case CAULDRON:
            this.playerGame.receiveCauldron((Integer) a.getObjs(0));
            break;

        case CHANGETURN:
            int t = (Integer) a.getObjs(0);
            this.playerGame.changePlayerTurn(t);
            break;

        case DISCARD:
            @SuppressWarnings("unchecked")
            ArrayList<Card> discard = (ArrayList<Card>) a.getObjs(0);
            this.playerGame.setDiscard(discard);
            break;

        case CRYSTAL:
            @SuppressWarnings("unchecked")
            ArrayList<Card> crystalCards = (ArrayList<Card>) a.getObjs(0);
            this.playerGame.receiveStackToReorder(crystalCards);
            break;

        case INITGAME:
            @SuppressWarnings("unchecked")
            HashMap<Integer, Player> players = (HashMap<Integer, Player>) a
                    .getObjs(0);
            int cauldron = (Integer) a.getObjs(1);
            this.playerGame.launchGame(players, cauldron);
            break;

        case PLAYEDCARDS:
            @SuppressWarnings("unchecked")
            ArrayList<Card> cardsList = (ArrayList<Card>) a.getObjs(0);
            this.playerGame.receivePlayedCards(cardsList);
            break;

        case ENDSESSION:
            this.playerGame.endSession();
            break;

        case ENDGAME:
            this.playerGame.endGame();
            break;

        case NEWPLAYER:
            this.playerGame.addPlayer((Player) a.getObjs(0));
            break;

        case PLAYERLEFT:
            player = (Player) a.getObjs(0);
            this.playerGame.removePlayer(player, true);
            break;
        case PLAYERLOST:
            player = (Player) a.getObjs(0);
            this.playerGame.removePlayer(player, false);
            this.playerGame.connectionLost(player);
            break;

        case READY:
            this.playerGame.setReadyStatement((Player) a.getObjs(0),
                    (Boolean) a.getObjs(1));
            break;
        case CANNOTPICKLASTGEM:
            this.playerGame.canNotPickLastGem();
            break;
        default:
            System.err
                    .println("Wrong action received from client, check "
                            + "protocol version");
            break;
        }
    }

    @Override
    public void dropPlayer() {
        this.playerGame.connectionLost();
        try {
            this.output.close();
            this.socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Warn the server that I end my turn.
     *
     * @param pickGem
     *            (Boolean)
     */
    public void sendEndTurn(boolean pickGem) {
        this.send(new Action(Order.CHANGETURN, pickGem));
    }

    /**
     * Send Player object to the server.
     *
     * @param player
     *            (Player)
     */
    public void sendPlayer(Player player) {
        Action a = new Action(Order.PLAYER, player);
        this.send(a);
    }

    /**
     * Send the card i play to the server.
     *
     * @param card
     *            (Card)
     */
    public void playCard(Card card) {
        Action a = new Action(Order.PLAYCARD, card);
        this.send(a);
    }

    /**
     * Send the 4 cards on the stack top \ roerdered thanks to the CrystalBall
     * card.
     *
     * @param newStack
     *            (ArrayList<Card>)
     */
    public void sendReorderedStack(ArrayList<Card> newStack) {
        this.send(new Action(Order.CRYSTAL, newStack));
    }

    /**
     * Complete the players's grimoire with a player's hand cars.
     *
     * @param handCard
     *            (Card)
     */
    public void completeGrimoire(Card handCard) {
        this.send(new Action(Order.COMPLETEGRIMOIRE, handCard));
    }

    /**
     * exchange a players's hand card with a player's grimoire card.
     *
     * @param handCard
     *            (Card)
     * @param grimoireCard
     *            (Card)
     */
    public void exchangeGrimoireHand(Card handCard, Card grimoireCard) {
        this.send(new Action(Order.EXCHANGEHANDGRIMOIRE,
                handCard,
                grimoireCard));
    }

    /**
     * Say to the server that the Player is ready.
     *
     * @param state
     *            (boolean)
     */
    public void setReadyStatement(boolean state) {
        this.send(new Action(Order.READY, state));
    }

    /**
     * Updating a player name.
     *
     * @param name
     *            (String) : the new player name.
     */
    public void changePlayerName(String name) {
        this.send(new Action(Order.SETPLAYERNAME, name));
    }

    /**
     * Tell the server a player left the game.
     * @param player (Player)
     */
    public void playerLeft(Player player) {
        this.send(new Action(Order.PLAYERLEFT, player));
        if(this.player.equals(player)) {
            this.playerGame.removePlayer(player, true);
            this.dropPlayer();
        }
    }
}
