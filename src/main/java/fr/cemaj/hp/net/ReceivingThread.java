package fr.cemaj.hp.net;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * This class represents the thread receiving queries via a Socket.
 * @author E.G. & A.F.
 */
public class ReceivingThread implements Runnable {
    private Proxy proxy;
    private Socket socket;
    private ObjectInputStream input;
    private boolean running;

    /**
     * ReceivingThread Constructor.
     * @param proxy (Proxy)
     * @param so (Socket)
     */
    public ReceivingThread(Proxy proxy, Socket so) {
        this.proxy = proxy;
        this.socket = so;
    }

    @Override
    public void run() {
        this.running = true;
        try {
            this.input = new ObjectInputStream(this.socket.getInputStream());
            while(this.running) {
                Action a = (Action) this.input.readObject();
                this.proxy.execute(a);
            }
        } catch (IOException e) {
            if(this.running) {
                this.exit();
            }
        } catch (ClassNotFoundException e) {
            if(this.running) {
                this.exit();
            }
        }
    }

    /**
     * exit.
     */
    public void exit() {
        this.running = false;
        try {
            this.input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.proxy.dropPlayer();
    };
}
