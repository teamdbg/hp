/**
 * package-info: fr.cemaj.hp.net
 *
 * Version 0.1
 *
 * Date 11.26.2013
 *
 * Copyright CEMAJ
 *
 * Developers: E.G. & A.F.
 */

/**
 * This package contains classes handling the network communications.
 */
package fr.cemaj.hp.net;
