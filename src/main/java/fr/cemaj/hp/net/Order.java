package fr.cemaj.hp.net;

/**
 * @author E.G. & A.F.
 *
 * Types of Orders sent through proxies.
 */
public enum Order {
    /**
     * Welcome from client.
     */
    EHLO,
    /**
     * Game init.
     */
    INITGAME,
    /**
     * Send a Player.
     */
    PLAYER,
    /**
     * Announce to clients a new player turn.
     */
    CHANGETURN,
    /**
     * Set Ready Statement.
     */
    READY,
    /**
     * Play a Cards.
     */
    PLAYCARD,
    /**
     * List of played cards.
     */
    PLAYEDCARDS,
    /**
     * Nb of gemmes in the cauldron.
     */
    CAULDRON,
    /**
     * Discard.
     */
    DISCARD,
    /**
     * 4 cards to reorder.
     */
    CRYSTAL,
    /**
     * When card are resolved.
     */
    ENDSESSION,
    /**
     * Completing the grimoire with a hand card.
     */
    COMPLETEGRIMOIRE,
    /**
     * Exchanging a hand card with a grimoire card.
     */
    EXCHANGEHANDGRIMOIRE,
    /**
     * Set player's name.
     */
    SETPLAYERNAME,
    /**
     * When the game is finished.
     */
    ENDGAME,
    /**
     * New player incomes.
     */
    NEWPLAYER,
    /**
     * When a player leave the lobby.
     */
    PLAYERLEFT,
    /**
     * When a player lost his connection.
     */
    PLAYERLOST,
    /**
     * When a player try to pick the last gem of the cauldron.
     */
    CANNOTPICKLASTGEM;
}
