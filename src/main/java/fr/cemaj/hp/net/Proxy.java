package fr.cemaj.hp.net;

/**
 * Proxy super class.
 */
public abstract class Proxy {

    protected Thread receivingThread;


    /**
     * Proxy Constructor.
     */
    Proxy() {
    }

    /**
     * Launching the receiving thread.
     */
    protected abstract void runThread();

    /**
     * execute : used to handle received message from the network.
     * @param action (Action) : the message parameter
     */
    abstract void execute(Action action);

    /**
     * abstract dropPlayer.
     */
    public abstract void dropPlayer();

    /**
     * waitThread.
     */
    public void waitThread() {
        try {
            this.receivingThread.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
