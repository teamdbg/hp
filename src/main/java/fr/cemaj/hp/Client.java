package fr.cemaj.hp;

import java.awt.FontFormatException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import fr.cemaj.hp.game.Player;
import fr.cemaj.hp.game.PlayerEnvironment;
import fr.cemaj.hp.game.card.Card;
import fr.cemaj.hp.game.card.CardTargeting;
import fr.cemaj.hp.game.card.EnchantedMirror;
import fr.cemaj.hp.game.card.PlayerTargeting;
import fr.cemaj.hp.gui.MainFrame;
import fr.cemaj.hp.gui.PlayerBoard;
import fr.cemaj.hp.gui.PlayerBoardEnemyBoard;
import fr.cemaj.hp.gui.PlayerBoardMyBoard;
import fr.cemaj.hp.net.ServerProxy;

/**
 * This class is the main class of the game, it launch the application and make
 * the link between GUI classes and Game classes.
 *
 * @author Jérémy Longin
 */
public class Client {

    /** -------------------- Attributes --------------------. **/
    private static String os = System.getProperty("os.name").toLowerCase();
    private static final int PORT = 50001;

    private PlayerEnvironment gameEnv;
    private ServerProxy sp;
    private Server hpServer;
    private MainFrame mainFrame;
    private Player player;
    private PlayerBoard turnToPlay;

    /**
     * The main program : it launch the application.
     *
     * @param args (String[])
     * @throws FontFormatException (Exception)
     * @throws IOException (Exception)
     */
    public static void main(String[] args) throws FontFormatException,
            IOException {
        Client c = new Client();
        c.mainFrame = new MainFrame(isMac(), c);
        c.mainFrame.setVisible(true);
    }

    /**
     * Class constructor Initialize game elements.
     */
    public Client() {
        this.player = new Player();
        this.gameEnv = new PlayerEnvironment(this, this.player);
    }

    /**
     * Initialize the connection to the server.
     *
     * @param name
     *            : Player name
     * @param ip
     *            : server ip address
     * @return true if the connection succeeds
     */
    public boolean connect(String name, String ip) {
        try {
            Socket s = new Socket(ip, PORT);
            this.player.setName(name);
            this.sp = new ServerProxy(this.gameEnv, s, this.player);
            this.gameEnv.setProxy(this.sp);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Add a player to the lobby in GUI.
     *
     * @param p
     *            : the new player to add
     */
    public void addPlayer(Player p) {
        this.mainFrame.getLobbyPanel().addPlayer(p);
    }

    /**
     * Update gameMode label in lobby.
     *
     * @param quick
     *            : gameMode
     */
    public void updateGameMode(boolean quick) {
        this.mainFrame.getLobbyPanel().setGameMode(quick);
    }

    /**
     * Function to update player name in GUI.
     *
     * @param id
     *            : player id to update
     * @param name
     *            : new name of the player
     */
    public void setName(int id, String name) {
        this.mainFrame.getLobbyPan().updatePlayerName(id, name);
    }

    /**
     * Method to remove a player from lobby in GUI.
     *
     * @param p
     *            : the player to remove
     */
    public void removePlayer(Player p) {
        this.mainFrame.getLobbyPanel().removePlayer(p.getId());
    }

    /**
     * Method to quit lobby Calls when the user click on lobby panel return
     * button.
     */
    public void leaveGame() {
        this.gameEnv.leaveGame();
        if (this.hpServer != null) {
            this.hpServer.endGame();
        }
    }

    /**
     * Method to launch the server in a new thread.
     *
     * @param quick
     *            : game mode
     */
    public void setAndLaunchServer(boolean quick) {
        this.mainFrame.getLobbyPan().setGameMode(quick);
        this.hpServer = new Server(PORT, quick);
        Thread s = new Thread(this.hpServer);
        s.start();
    }

    /**
     * Method calls by GUI to change ready statement.
     */
    public void toggleReady() {
        this.gameEnv.sendReadyStatement(!this.gameEnv.getPlayers()
                .get(this.gameEnv.getPlayerId()).getReady());
    }

    /**
     * Method to update ready statement in GUI lobby.
     *
     * @param id
     *            : player id to update
     * @param state
     *            : new ready statement
     */
    public void setReady(int id, boolean state) {
        this.mainFrame.getLobbyPan().setReady(id, state);
    }

    /**
     * Method to launch the game and display game board.
     *
     * @param players
     *            : list of players in game
     * @param aCauldron
     *            : initial value of cauldron
     * @param myId
     *            : the id of the current player
     */
    public void launchGame(Object[] players, int aCauldron, int myId) {
        // Chargement du jeu
        this.mainFrame.getBoardPan().displayGameBoard(players, aCauldron, myId);
        // Update du chaudron
        updateCauldron(aCauldron);
        // Affichage du panneau de jeu
        this.mainFrame.changePanel(this.mainFrame.getBoardPan());
        // Initialisation du premier tour
        setTurnToPlay(this.gameEnv.getCurrentPlayer());
    }

    /**
     * Method to change player turn to play.
     * @param playerId : the next player id
     */
    public void nextPlayer(int playerId) {
        if (this.turnToPlay != null
                && this.turnToPlay.getPlayerId() != playerId) {
            this.turnToPlay.setCoinVisibility(false);
            this.turnToPlay.getNext().setCoinVisibility(true);
            this.turnToPlay = this.turnToPlay.getNext();
            this.mainFrame
                    .getBoardPan()
                    .getGamePart()
                    .addAction(
                            "C'est à " + this.turnToPlay.getPlayerName()
                                    + " de jouer");
        }
    }

    /**
     * Method to update player board in GUI.
     *
     * @param playerId : player id to update
     * @param aPlayer : player
     */
    public void updatePlayerBoard(int playerId, Player aPlayer) {
        ArrayList<PlayerBoard> playerBoards = this.mainFrame.getBoardPan()
                .getGamePart().getActivePlayerBoards();
        for (int i = 0; i < playerBoards.size(); i++) {
            if (playerBoards.get(i).getPlayerId() == playerId) {
                if (playerBoards.get(i) instanceof PlayerBoardMyBoard) {
                    ((PlayerBoardMyBoard)
                            playerBoards.get(i)).updatePlayer(aPlayer);
                } else {
                    ((PlayerBoardEnemyBoard)
                            playerBoards.get(i)).updatePlayer(aPlayer);
                }
            }
        }
    }

    /**
     * Method to update cauldron number.
     *
     * @param nb
     *            : new cauldron number
     */
    public void updateCauldron(int nb) {
        this.mainFrame.getBoardPan().getGamePart().getCauldron()
                .setGemNumber(nb);
    }

    /**
     * Method to set the first player to play.
     *
     * @param playerId (int)
     */
    public void setTurnToPlay(int playerId) {
        ArrayList<PlayerBoard> playerBoards = this.mainFrame.getBoardPan()
                .getGamePart().getActivePlayerBoards();
        for (int i = 0; i < playerBoards.size(); i++) {
            if (playerBoards.get(i).getPlayerId() == playerId) {
                this.turnToPlay = playerBoards.get(i);
                this.turnToPlay.setCoinVisibility(true);
                this.mainFrame
                        .getBoardPan()
                        .getGamePart()
                        .addAction(
                                "C'est à " + this.turnToPlay.getPlayerName()
                                        + " de jouer");
            }
        }
    }

    /**
     * Method to display crystal ball popup.
     *
     * @param cards
     *            : the cards to display in the popup
     */
    public void displayCristalBallPopup(Card[] cards) {
        this.mainFrame.getCristalBallPopup().reorganizeCards(cards);
    }

    /**
     * sendFourCardStack : use with a crystal ball card.
     * @param cards : the list of four cards
     */
    public void sendFourCardStack(ArrayList<Card> cards) {
        this.gameEnv.sendFourCardStack(cards);
    }

    /**
     * isWindows.
     * @return true if os is Windows | false otherwise
     */
    public static boolean isWindows() {
        return os.indexOf("win") >= 0;
    }

    /**
     * isMac.
     * @return true if os is OS X | false otherwise
     */
    public static boolean isMac() {
        return os.indexOf("mac") >= 0;
    }

    /**
     * Method to get validation to play a card.
     *
     * @param c : the card to play
     * @return true if card is playable | false otherwise
     */
    public boolean playCard(Card c) {
        if (!this.gameEnv.playCard(c)) {
            JOptionPane.showMessageDialog(this.mainFrame,
                    "Vous ne pouvez pas jouer cette carte", null, 0);
            return false;
        }
        return true;
    }

    /**
     * A card has been played by another player.
     *
     * @param p : the player
     * @param c : its played card
     */
    public void cardPlayed(Player p, Card c) {
        // Add gameInformation
        if (c.getPower() != 0) {
            this.mainFrame
                    .getBoardPan()
                    .getGamePart()
                    .addAction(
                            p.getName() + " a joué une carte "
                                    + c.getClass().getSimpleName() + " "
                                    + c.getPower());
        } else {
            this.mainFrame
                    .getBoardPan()
                    .getGamePart()
                    .addAction(
                            p.getName() + " a joué une carte "
                                    + c.getClass().getSimpleName());
        }
        // Display card
        if (p.getId() != this.gameEnv.getPlayerId()) {
            if (c.isHocus()) {
                this.mainFrame.getBoardPan().getGamePart().getGameZone()
                        .addHocusCard(c, this.mainFrame.getWidth());
            } else {
                this.mainFrame.getBoardPan().getGamePart().getGameZone()
                        .addPocusCard(c, this.mainFrame.getWidth());
            }
        }
        // Add target
        if (c.getClass().getSuperclass().getSimpleName()
                .compareTo("CardTargeting") == 0) {
            ArrayList<Card> targetedCards;
            targetedCards = ((CardTargeting) c).getTargetedCards();
            ArrayList<PlayerBoard> playerBoards = this.mainFrame.getBoardPan()
                    .getGamePart().getActivePlayerBoards();
            // parcours des playerBoard
            for (int i = 0; i < playerBoards.size(); i++) {
                // Si c'est le joueur visé
                if (playerBoards.get(i).getPlayerId() == ((CardTargeting) c)
                        .getTargetedPlayer().getId()) {
                    // Parcours des cartes visée et check dans grimoire board
                    for (int j = 0; j < targetedCards.size(); j++) {
                        if (playerBoards.get(i).grim1.getCard().equals(
                                targetedCards.get(j))) {
                            playerBoards.get(i).grim1.setTargeted(true);
                        } else {
                            if (playerBoards.get(i).grim2.getCard().equals(
                                    targetedCards.get(j))) {
                                playerBoards.get(i).grim2.setTargeted(true);
                            } else {
                                if (playerBoards.get(i).grim3.getCard().equals(
                                        targetedCards.get(j))) {
                                    playerBoards.get(i).grim3.setTargeted(true);
                                }
                            }
                        }
                    }
                    this.mainFrame
                            .getBoardPan()
                            .getGamePart()
                            .addAction(
                                    playerBoards.get(i).getPlayerName()
                                            + " est visé par la carte Hocus.");
                    break;
                }
            }
        } else {
            if (c.getClass().getSuperclass().getSimpleName()
                    .compareTo("PlayerTargeting") == 0) {
                ArrayList<PlayerBoard> playerBoards = this.mainFrame
                        .getBoardPan().getGamePart().getActivePlayerBoards();
                // parcours des playerBoard
                for (int i = 0; i < playerBoards.size(); i++) {
                    // Si c'est le joueur visé
                    if (playerBoards.get(i).getPlayerId() == ((PlayerTargeting)
                            c).getTargetedPlayer().getId()) {
                        playerBoards.get(i).setTargeted(true);
                        // Add target information
                        if (c instanceof EnchantedMirror) {
                            this.mainFrame
                                    .getBoardPan()
                                    .getGamePart()
                                    .addAction(
                                            c.getCardOwner().getName()
                                                    + " a renvoyer l'effet de"
                                                    + "la carte Hocus.");
                            this.mainFrame
                                    .getBoardPan()
                                    .getGamePart()
                                    .addAction(
                                            playerBoards.get(i).getPlayerName()
                                                    + " est visé par la carte"
                                                    + " Hocus.");
                        } else {
                            this.mainFrame
                                    .getBoardPan()
                                    .getGamePart()
                                    .addAction(
                                            playerBoards.get(i).getPlayerName()
                                                    + " est visé par la carte"
                                                    + " Hocus.");
                        }
                    }
                }
            }
        }
        // Check if the target is redraw
        if (this.gameEnv.getHocusPlayed().getCardOwner().getId() == this.gameEnv
                .getPlayerId()
                && this.gameEnv.getHocusPlayed().getClass().getSuperclass()
                        .getSimpleName().compareTo("CardTargeting") == 0
                && c instanceof EnchantedMirror) {
            this.displayMessage("Vous devez rechoisir les cartes visées sur le"
                    + "nouveau joueur");
            this.mainFrame.getListener().mouseListener.setSecondTarget(true);
            //((CardTargeting)this.gameEnv.getHocusPlayed()).
            //setTargetedPlayer(((CardTargeting)c).getTargetedPlayer());
        }
    }

    /**
     * Method to display ResultsPanel.
     * @param players ()
     */
    public void displayEndGame(Object[] players) {
        System.out.println("DisplayEndGame > Display");
        // Init classement
        this.mainFrame.getResultsPan().initPlayers(players);
        // Display panel
        this.mainFrame.changePanel(this.mainFrame.getResultsPan());
    }

    /**
     * Method to update the discarding on GUI.
     * @param discard : the discard cards
     */
    public void setDiscard(ArrayList<Card> discard) {
        try {
            this.mainFrame.getBoardPan().getGamePart().updateDiscard(discard);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Remove elements from GameZone
        this.mainFrame.getBoardPan().getGamePart().getGameZone().removeCards();
        this.mainFrame.repaint();
    }

    /**
     * Method call by GUI classes to switch cards.
     * @param handCard : the hand card to exchange
     * @param grimoireCard : the grimoire card to exchange
     *
     * @return true if action succeed | false otherwise
     */
    public boolean exchangeHandCardsWithGrimoire(Card handCard,
            Card grimoireCard) {
        return this.gameEnv.exchangeHandCardsWithGrimoire(handCard,
                grimoireCard);
    }

    /**
     * Method to display a message to the user in a popup.
     * @param message : the message to display
     */
    public void displayMessage(String message) {
        JOptionPane.showMessageDialog(this.mainFrame,
                message, null, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Finish the turn of the user.
     *
     * @param pickGem : true => pick gems; false => draw cards from the library
     */
    public void endTurn(boolean pickGem) {
        this.gameEnv.endTurn(pickGem);
    }

    /**
     * resetTargetStates.
     */
    public void resetTargetStates() {
        ArrayList<PlayerBoard> playerBoards = this.mainFrame.getBoardPan()
                .getGamePart().getActivePlayerBoards();
        for (int i = 0; i < playerBoards.size(); i++) {
            playerBoards.get(i).resetTargets();
        }
    }

    /**
     * connectionLost.
     */
    public void connectionLost() {
        this.mainFrame.changePanel(this.mainFrame.getMainMenuPan());
        this.displayMessage("La connexion au serveur a été perdue");
    }

    /**
     * connectionLost function : called when a player lost its connection.
     * @param p : the player that lost its connection
     */
    public void connectionLost(Player p) {
        ArrayList<PlayerBoard> playerBoards = this.mainFrame.getBoardPan()
                .getGamePart().getActivePlayerBoards();
        Object[] players = new Object[playerBoards.size()];
        for (int i = 0; i < playerBoards.size(); i++) {
            players[i] = playerBoards.get(i).getPlayer();
        }
        this.displayEndGame(players);
        this.displayMessage(p.getName()
                + " a été déconnecté.\nRelancer une nouvelle partie pour"
                + "continuer\nà jouer à Hocus Pocus by CEMAJ.");
    }

    /**
     * isCardInGame.
     * @return true : inGame | false : notInGame
     */
    public boolean isCardInGame() {
        if (this.gameEnv.getPlayedCards().size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * getHocusPlayed.
     * @return the Hocus played card
     */
    public Card getHocusPlayed() {
        return this.gameEnv.getHocusPlayed();
    }

    /**
     * getGameEnv.
     * @return game environment
     */
    public PlayerEnvironment getGameEnv() {
        return this.gameEnv;
    }

    /**
     * setGameEnv.
     * @param gameEnv (PlayerEnvironment)
     */
    public void setGameEnv(PlayerEnvironment gameEnv) {
        this.gameEnv = gameEnv;
    }

    /**
     * getSp.
     * @return a ServerProxy
     */
    public ServerProxy getSp() {
        return this.sp;
    }

    /**
     * setSp.
     * @param sp (ServerProxy)
     */
    public void setSp(ServerProxy sp) {
        this.sp = sp;
    }

    /**
     * getHpServer.
     * @return a Server
     */
    public Server getHpServer() {
        return this.hpServer;
    }

    /**
     * setHpServer.
     * @param hpServer (Server)
     */
    public void setHpServer(Server hpServer) {
        this.hpServer = hpServer;
    }

    /**
     * getMainFrame.
     * @return a MainFrame
     */
    public MainFrame getMainFrame() {
        return this.mainFrame;
    }

    /**
     * setMainFrame.
     * @param mainFrame (MainFrame)
     */
    public void setMainFrame(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    /**
     * getPlayer.
     * @return a Player
     */
    public Player getPlayer() {
        return this.player;
    }

    /**
     * setPlayer.
     * @param player (Player)
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * getTurnToPlay.
     * @return a PlayerBoard
     */
    public PlayerBoard getTurnToPlay() {
        return this.turnToPlay;
    }

    /**
     * setTurnToPlay.
     * @param turnToPlay (PlayerBoard)
     */
    public void setTurnToPlay(PlayerBoard turnToPlay) {
        this.turnToPlay = turnToPlay;
    }
}
